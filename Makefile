# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

NAME=glasnost
RELEASE_TYPE=-devel
RPM_RELEASE=1
GLASNOST=$(NAME)$(RELEASE_TYPE)
PORT=9000
VERSION=20040225

GLASNOST_MODULE= $(subst -,_,$(GLASNOST))
LANGUAGES=de es fi fr sv
PYTHON_VERSION=2.3

PREFIX=/usr/local
PREFIX_QUOTED=$(subst /,\/,$(PREFIX))
VARPREFIX=/var/lib
SHAREPREFIX=$(PREFIX)/share
LOGPREFIX=/var/log

# Target directories for install
BIN_DIR=$(PREFIX)/bin
DATA_DIR=$(VARPREFIX)/$(GLASNOST)
DATA_DIR_QUOTED= $(subst /,\/,$(DATA_DIR))
ETC_DIR=/etc/$(GLASNOST)
ETC_DIR_QUOTED= $(subst /,\/,$(ETC_DIR))
EXTENSIONS_DIR=$(DATA_DIR)/extensions
GLASNOST_DIR=$(PREFIX)/share/$(GLASNOST)
LIBS_DIR=$(PREFIX)/lib/$(GLASNOST)
LIBS_DIR_QUOTED = $(subst /,\/,$(LIBS_DIR))
LOCALE_DIR=$(SHAREPREFIX)/locale
LOCALE_DIR_QUOTED= $(subst /,\/,$(LOCALE_DIR))
LOG_DIR=$(LOGPREFIX)/$(GLASNOST)
LOG_DIR_QUOTED= $(subst /,\/,$(LOG_DIR))
PYTHON_DIR=$(LIBS_DIR)/glasnost
SBIN_DIR=$(PREFIX)/sbin
SCRIPTS_DIR=$(GLASNOST_DIR)/scripts
SERVERS_DIR=$(LIBS_DIR)/servers
SERVERS_DIR_QUOTED= $(subst /,\/,$(SERVERS_DIR))
TEMPLATES_DIR=$(DATA_DIR)/templates
WEB_DIR=$(LIBS_DIR)/web
WEB_DIR_QUOTED= $(subst /,\/,$(WEB_DIR))
WEBCACHE_DIR=$(DATA_DIR)/webcache

SOURCE_DIR=`echo $(CURDIR) | awk -F/ '{print $$NF}'`
PROJECT_DIR=$(GLASNOST)-$(VERSION)
DIST_FILE=$(GLASNOST)-$(VERSION).tar.gz
ARCHIVE_FILE=$(GLASNOST)-$(VERSION).tar.bz2

FIND=find
GETTEXT=/usr/share/doc/python$(PYTHON_VERSION)/examples/Tools/i18n/pygettext.py
TAR=tar
RPM=rpm
PACKAGEINFO=redhat/packageinfo.pl

SERVER_USER=root
SERVER_GROUP=root
WEB_USER=www-data
WEB_GROUP=www-data

TEMPLATES=default april bxlug codelutin.org cuisine demo.entrouvert.org easter-eggs entrouvert.com entrouvert2 help glasnost2 lightbulb macfly plane september vecam theridion entrouvert.be labor-liber.net glasnost.entrouvert.org watercolor skelblue

RPM_PACKAGE_NAME=python$(PYTHON_VERSION)-tal python$(PYTHON_VERSION)-imaging  python$(PYTHON_VERSION)-docutils

.PHONY: help archive clean data-archive dist dist-clean install-create-dir install-glasnost install-data install-extensions install-gtk install-locale install-log install-python-libs install-scripts install-servers install-templates install-web install mo po profiles-xml glasnost.spec rpm rpm-depend uninstall

help:
	@echo
	@echo "        Rules"
	@echo "archive    - create an archive in tar format of the project"
	@echo "rpm        - create an rpm package"
	@echo "rpm-depend - create rpm package don't exist in redhat distribution"
	@echo "install    - install glasnost on local machine"
	@echo "uninstall  - uninstall glasnost on local machine"
	@echo "dist       - create package in tar format ready to install"
	@echo
	@echo "        Variables"
	@echo "GLASNOST (default=${GLASNOST})"
	@echo "PORT (DEFAULT=${PORT})"
	@echo "VERSION (DEFAULT=${VERSION})"
	@echo "PYTHON_VERSION (DEFAULT=${PYTHON_VERSION})"
	@echo "PREFIX (default=${PREFIX})"
	@echo "VARPREFIX (default=${VARPREFIX})"
	@echo "SHAREPREFIX (default=${SHAREPREFIX})"
	@echo "LOGPREFIX (default=${LOGPREFIX})"
	@echo
	@echo "example: make GLASNOST=glasnost VERSION=0.6.0 RPM_RELEASE=1 PYTHON_VERSION=2.2 PREFIX=/usr PORT=8000 rpm"

archive: dist-clean
	cd .. ; \
	mv $(SOURCE_DIR) $(PROJECT_DIR); \
	$(TAR) jcf $(ARCHIVE_FILE) $(PROJECT_DIR); \
	mv $(PROJECT_DIR) $(SOURCE_DIR)

clean:
	rm -rf tmp-system tmp-tests
	rm -rf root-tests root-system
	rm -f talTranslations.py

config: config.in
	cat $^ | sed -e "s/^glasnost = .*/glasnost = $(GLASNOST)/g" \
		-e "s/^logdir = .*/logdir = $(LOG_DIR_QUOTED)/g" \
		-e "s/^port = .*/port = $(PORT)/g" \
		-e "s/^vardir = .*/vardir = $(DATA_DIR_QUOTED)/g" \
		-e "s/^webdir = .*/webdir = $(WEB_DIR_QUOTED)/g" \
		-e "s/^LocaleDir = .*/LocaleDir = $(LOCALE_DIR_QUOTED)/g" \
		> config

config-system:
	rm -rf tmp-system && cp -R system tmp-system
	for F in `$(FIND) tmp-system -name "*.py"`; \
	do \
		cp $$F /tmp/glasnost-source; \
		cat /tmp/glasnost-source | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = \"$(LIBS_DIR_QUOTED)\"/g" > $$F; \
		rm /tmp/glasnost-source; \
	done

config-tests:
	rm -rf tmp-tests && cp -R tests tmp-tests
	for F in `$(FIND) tmp-tests -name "*.py"`; \
	do \
		cp $$F /tmp/glasnost-source; \
		cat /tmp/glasnost-source | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = \"$(LIBS_DIR_QUOTED)\"/g" > $$F; \
		rm /tmp/glasnost-source; \
	done

data-archive:
	tar cjf ../$(GLASNOST)-data-`date +%Y%m%d%H%M`.tar.bz2 \
		--directory "$(VARPREFIX)/$(GLASNOST)/" --exclude=webcache ./

dist: dist-clean
	rm -rf $(PROJECT_DIR) && mkdir $(PROJECT_DIR)
	$(TAR) c --exclude CVS --exclude .cvsignore --exclude .xvpics \
		--exclude debian --exclude $(PROJECT_DIR) \
		--exclude '.#*' --exclude tmp-system --exclude root-system \
		--exclude build-stamp --exclude stamp-po --exclude configure-stamp \
		--exclude tmp-tests --exclude root-tests . | \
		 (cd $(PROJECT_DIR) && tar x)
	$(FIND) $(PROJECT_DIR) -type l | xargs -r rm
	rm -rf $(PROJECT_DIR)/templates/*
	for F in $(TEMPLATES); \
	do \
		cp -R templates/$$F $(PROJECT_DIR)/templates/; \
		rm -rf $(PROJECT_DIR)/templates/$$F/CVS; \
	done
	tar czf ../$(DIST_FILE) $(PROJECT_DIR)
	rm -rf $(PROJECT_DIR)

dist-clean:
	rm -rf help/data
	rm -rf system/data
	$(FIND) . -name "*~" | xargs -r rm
	$(FIND) . -name ".*~" | xargs -r rm
	$(FIND) . -name ".*.swp" | xargs -r rm
	$(FIND) . -name "*.pyc" | xargs -r rm
	$(FIND) . -name "*.pyo" | xargs -r rm
	$(FIND) . -name ".#*" | xargs -r rm
	$(FIND) . -name ".nautilus-metafile.xml" | xargs -r rm
	$(FIND) . -name "nohup.out" | xargs -r rm
	$(FIND) ./templates/ -name '*.talc.*' -or -name '*.htmlc.*' | xargs -r rm
	-rm -f config

infos-xml:
	for F in `$(FIND) templates/ -type f -name '*.xml.in'`; \
	do \
		intltool-merge -x po/glasnost-web/ $$F `dirname $$F`/`basename $$F .in`; \
	done

install-create-dir:
	-mkdir -p "$(DESTDIR)/$(BIN_DIR)"
	-mkdir -p "$(DESTDIR)/$(DATA_DIR)"
	-mkdir -p "$(DESTDIR)/$(GLASNOST_DIR)"
	-mkdir -p "$(DESTDIR)/$(ETC_DIR)"
	-mkdir -p "$(DESTDIR)/$(EXTENSIONS_DIR)"
	-mkdir -p "$(DESTDIR)/$(LIBS_DIR)"
	-mkdir -p "$(DESTDIR)/$(LOCALE_DIR)"
	-mkdir -p "$(DESTDIR)/$(LOG_DIR)"
	-mkdir -p "$(DESTDIR)/$(PYTHON_DIR)"
	-mkdir -p "$(DESTDIR)/$(SBIN_DIR)"
	-mkdir -p "$(DESTDIR)/$(SCRIPTS_DIR)"
	-mkdir -p "$(DESTDIR)/$(SERVERS_DIR)"
	-mkdir -p "$(DESTDIR)/$(TEMPLATES_DIR)"
	-mkdir -p "$(DESTDIR)/$(WEB_DIR)"
	-mkdir -p "$(DESTDIR)/$(WEBCACHE_DIR)"

install-glasnost: config profiles-xml
	mkdir -p "$(DESTDIR)/$(GLASNOST_DIR)"
	-cp config "$(DESTDIR)/$(GLASNOST_DIR)"/
	if test -e "$(DESTDIR)/$(SBIN_DIR)/$(GLASNOST)"; then \
		rm -f "$(DESTDIR)/$(SBIN_DIR)/$(GLASNOST)"; \
	fi
	if test "true" = "$(DONT_USE_DESTDIR_FOR_LINK)" ; then \
		RPM_DESTDIR=""; \
	else \
		RPM_DESTDIR=="$(DESTDIR)"; \
	fi
	cat glasnost-ctl.py | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = \"$(LIBS_DIR_QUOTED)\"/g" > "$(DESTDIR)/$(SBIN_DIR)/$(GLASNOST)-ctl"
	chmod +x "$(DESTDIR)/$(SBIN_DIR)/$(GLASNOST)-ctl"

	if ! test -e "$(DESTDIR)/$(ETC_DIR)"; then \
		mkdir -p "$(DESTDIR)/$(ETC_DIR)"; \
	fi
	test ! -e "$(DESTDIR)/$(ETC_DIR)/config" || \
		cp "$(DESTDIR)/$(ETC_DIR)/config" "$(DESTDIR)/$(ETC_DIR)/config.save"
	test -e "$(DESTDIR)/$(ETC_DIR)/config" || cp config "$(DESTDIR)/$(ETC_DIR)/config"
	test -d "$(DESTDIR)/$(ETC_DIR)/profiles" || mkdir "$(DESTDIR)/$(ETC_DIR)/profiles"
	for F in `find profiles -type f -name '*.xml'`; \
	do \
		cp $$F "$(DESTDIR)/$(ETC_DIR)/profiles"; \
	done

install-data:
	if ! test -e "$(DESTDIR)/$(DATA_DIR)"; then \
		mkdir -p "$(DESTDIR)/$(DATA_DIR)"; \
	fi
	if test -e "$(DESTDIR)/$(DATA_DIR)/system"; then \
		rm -rf "$(DESTDIR)/$(DATA_DIR)/system"; \
	fi
	if test ! -e system/data; then \
		./make-system; \
	fi
	cp -pr system/data "$(DESTDIR)/$(DATA_DIR)/system"
	chown $(SERVER_USER).$(SERVER_GROUP) "$(DESTDIR)/$(DATA_DIR)" || /bin/true
	if ! test -e "$(DESTDIR)/$(DATA_DIR)/webcache"; then \
		mkdir -p "$(DESTDIR)/$(DATA_DIR)/webcache"; \
	fi
	chown $(WEB_USER).$(WEB_GROUP) "$(DESTDIR)/$(DATA_DIR)/webcache"

install-extensions:
	-mkdir -p "$(DESTDIR)/$(EXTENSIONS_DIR)"
	cp extensions/*.py "$(DESTDIR)/$(EXTENSIONS_DIR)"

install-gtk: glasnost-gtk/glasnost-gtk.py
	cat glasnost-gtk/glasnost-gtk.py | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = \"$(LIBS_DIR_QUOTED)\"/g" > "$(DESTDIR)/$(BIN_DIR)/$(GLASNOST)-gtk"
	chmod +x "$(DESTDIR)/$(BIN_DIR)/$(GLASNOST)-gtk"

install-locale: mo
	for L in $(LANGUAGES); \
	do \
		mkdir -p "$(DESTDIR)/$(LOCALE_DIR)/$$L/LC_MESSAGES"; \
		cp -R locale/$$L/LC_MESSAGES/glasnost-web.mo "$(DESTDIR)/$(LOCALE_DIR)/$$L/LC_MESSAGES/$(GLASNOST)-web.mo"; \
	done

install-log:
	if test ! -e "$(DESTDIR)/$(LOG_DIR)"; then \
		mkdir -p "$(DESTDIR)/$(LOG_DIR)"; \
	fi
	chown $(SERVER_USER).$(SERVER_GROUP) "$(DESTDIR)/$(LOG_DIR)" | /bin/true

install-python-libs:
	-mkdir -p "$(DESTDIR)/$(PYTHON_DIR)"
	-cp -R shared/* "$(DESTDIR)/$(PYTHON_DIR)"
	cat shared/__init__.py | \
		sed -e "s/applicationName = .*/applicationName = '$(GLASNOST)'/" \
		    -e "s/serverUser = .*/serverUser = '$(SERVER_USER)'/" \
		    -e "s/versionNumber = .*/versionNumber = '$(VERSION)'/" > "$(DESTDIR)/$(PYTHON_DIR)/__init__.py"
	cat shared/common/tools_new.py | \
		sed -e "s/^configDir.*/configDir = \"$(ETC_DIR_QUOTED)\"/g" > "$(DESTDIR)/$(PYTHON_DIR)/common/tools_new.py"

install-scripts:
	-cp -R scripts/* "$(DESTDIR)/$(SCRIPTS_DIR)"
	for F in `$(FIND) "$(DESTDIR)/$(SCRIPTS_DIR)" -name "*.py"`; \
	do \
		cp $$F /tmp/glasnost-source; \
		cat /tmp/glasnost-source | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = \"$(LIBS_DIR_QUOTED)\"/g" > $$F; \
		rm /tmp/glasnost-source; \
	done

install-servers:
	-mkdir -p "$(DESTDIR)/$(LIBS_DIR)"
	-mkdir -p "$(DESTDIR)/$(SERVERS_DIR)"
	-cp -R servers/* "$(DESTDIR)/$(SERVERS_DIR)"
	for F in `$(FIND) "$(DESTDIR)/$(SERVERS_DIR)" -name "*.py"`; \
	do \
		cp $$F /tmp/glasnost-source; \
		cat /tmp/glasnost-source | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = \"$(LIBS_DIR_QUOTED)\"/g" > $$F; \
		rm /tmp/glasnost-source; \
	done

install-templates: infos-xml
	-mkdir -p "$(DESTDIR)/$(TEMPLATES_DIR)"
	for F in $(TEMPLATES); \
	do \
		(cd templates && $(TAR) c --exclude CVS --exclude '*.in' --exclude '*.in.h' $$F) | (cd "$(DESTDIR)/$(TEMPLATES_DIR)" && $(TAR) x); \
	done

install-web:
	-mkdir -p "$(DESTDIR)/$(WEB_DIR)"
	-cp glasnost-web/.htaccess "$(DESTDIR)/$(WEB_DIR)"/
	-cp -R glasnost-web/* "$(DESTDIR)/$(WEB_DIR)"/
	for F in `$(FIND) "$(DESTDIR)/$(WEB_DIR)" -name "*.py"`; \
	do \
		cp $$F /tmp/glasnost-source; \
		cat /tmp/glasnost-source | sed -e "s/^glasnostPythonDir = .*/glasnostPythonDir = '$(LIBS_DIR_QUOTED)'/g" > $$F; \
		rm /tmp/glasnost-source; \
	done

install: 	install-create-dir \
		install-extensions \
		install-glasnost \
		install-gtk \
		install-python-libs \
		install-servers \
		install-templates \
		install-web \
		install-data \
		install-locale \
		install-log \
		install-scripts

install-redhat:	
		$(MAKE) PYTHON_VERSION=2.2 PREFIX=/usr WEB_USER=apache WEB_GROUP=apache GLASNOST=glasnost install


mo:		po/glasnost-web/de.po \
		po/glasnost-web/es.po \
		po/glasnost-web/fi.po \
		po/glasnost-web/fr.po \
		po/glasnost-web/sv.po
	-mkdir -p locale/de/LC_MESSAGES
	-mkdir -p locale/es/LC_MESSAGES
	-mkdir -p locale/fi/LC_MESSAGES
	-mkdir -p locale/fr/LC_MESSAGES
	-mkdir -p locale/sv/LC_MESSAGES
	msgfmt --statistics -c -v -o locale/de/LC_MESSAGES/glasnost-web.mo po/glasnost-web/de.po
	msgfmt --statistics -c -v -o locale/es/LC_MESSAGES/glasnost-web.mo po/glasnost-web/es.po
	msgfmt --statistics -c -v -o locale/fi/LC_MESSAGES/glasnost-web.mo po/glasnost-web/fi.po
	msgfmt --statistics -c -v -o locale/fr/LC_MESSAGES/glasnost-web.mo po/glasnost-web/fr.po
	msgfmt --statistics -c -v -o locale/sv/LC_MESSAGES/glasnost-web.mo po/glasnost-web/sv.po

profiles-xml:
	for F in `$(FIND) profiles/ -type f -name '*.xml.in'`; \
	do \
		intltool-merge -x po/glasnost-web/ $$F `dirname $$F`/`basename $$F .in`; \
	done


talTranslations.py: templates/
	./talGettext.py templates/ > talTranslations.py

stamp-po:	glasnost-web/ \
		shared/common/ \
		shared/proxy/ \
		shared/server/ \
		shared/web/ \
		system/ \
		talTranslations.py

	cp po/glasnost-web/de.po po/glasnost-web/de.pox
	cp po/glasnost-web/es.po po/glasnost-web/es.pox
	cp po/glasnost-web/fi.po po/glasnost-web/fi.pox
	cp po/glasnost-web/fr.po po/glasnost-web/fr.pox
	cp po/glasnost-web/sv.po po/glasnost-web/sv.pox
	for F in `$(FIND) profiles/ templates/ -type f -name '*.xml.in'`; \
	do \
		intltool-extract --type=gettext/xml $$F; \
	done
	$(GETTEXT) -d po/glasnost-web/messages -k N_ `$(FIND) $^ -type f -name "*.py"` \
			`$(FIND) profiles/ templates/ -type f -name '*.xml.in.h'`
	msgmerge -o po/glasnost-web/de.po -D po/glasnost-web de.pox messages.pot
	msgmerge -o po/glasnost-web/es.po -D po/glasnost-web es.pox messages.pot
	msgmerge -o po/glasnost-web/fi.po -D po/glasnost-web fi.pox messages.pot
	msgmerge -o po/glasnost-web/fr.po -D po/glasnost-web fr.pox messages.pot
	msgmerge -o po/glasnost-web/sv.po -D po/glasnost-web sv.pox messages.pot

po: stamp-po
	touch stamp-po

glasnost.spec: redhat/glasnost.spec.in Makefile
	cat $< | sed -e "s/^%define PYTHON_VERSION .*/%define PYTHON_VERSION ${PYTHON_VERSION}/" \
	-e "s/^%define ver .*/%define ver ${VERSION}/" \
	-e "s/^%define rel .*/%define rel ${RPM_RELEASE}/" \
	-e "s/^%define prefix .*/%define prefix ${PREFIX_QUOTED}/" \
	-e "s/^%define nam .*/%define nam ${GLASNOST}/" > $@

redhat/README.redhat: debian/README.Debian
	cat $< | sed -e "s/for Debian//" > $@

rpm: glasnost.spec redhat/README.redhat dist rpm-depend
	$(RPM) -ta ../$(DIST_FILE)
	cp /usr/src/rpm/RPMS/noarch/${NAME}-${VERSION}-${RPM_RELEASE}.noarch.rpm redhat
	cp /usr/src/rpm/RPMS/noarch/${NAME}-gtk-${VERSION}-${RPM_RELEASE}.noarch.rpm redhat

rpm-depend:
	apt-get install --download-only $(RPM_PACKAGE_NAME)
	for f in $(RPM_PACKAGE_NAME); do \
	PACKAGE_NAME=`grep-available -X -P -s Filename $$f|cut -f2 -d' '`; \
	PACKAGE_NAME=`basename $$PACKAGE_NAME`; \
	(cd redhat; alien --keep-version --to-rpm /var/cache/apt/archives/$$PACKAGE_NAME); \
	done

uninstall:
	-rm -Rf "$(DESTDIR)/$(GLASNOST_DIR)"
	-rm -Rf "$(DESTDIR)/$(LOG_DIR)"
	-rm -Rf "$(DESTDIR)/$(PYTHON_DIR)"
	-rm -Rf "$(DESTDIR)/$(LIBS_DIR)"
	-rm -Rf "$(DESTDIR)/$(SERVERS_DIR)"
	-rm -f "$(DESTDIR)/etc/init.d/$(GLASNOST)"
	-rm -f "$(DESTDIR)/$(SBIN_DIR)/$(GLASNOST)-ctl"
	-rm -f "$(DESTDIR)/$(BIN_DIR)/$(GLASNOST)-gtk"
	$(FIND) "$(LOCALE_DIR)" -name "$(GLASNOST)-web.mo" -exec rm {} \; \
	|| /bin/true

