#!/bin/bash
#
# Startup script for the Glasnost Server
#
# chkconfig: - 84 16
# description: Glasnost.
# config: /etc/glasnost/config

# Source function library.
. /etc/rc.d/init.d/functions

prog=Glasnost
glasnost=/usr/sbin/`basename $0`

start() {
        echo -n $"Starting $prog: "
	$glasnost start
}
stop() {
	echo -n $"Stopping $prog: "
	$glasnost stop
}

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
	echo not implemented
	;;
  restart)
	stop
	start
	;;
  *)
	echo $"Usage: $prog {start|stop|restart|status}"
	exit 1
esac
