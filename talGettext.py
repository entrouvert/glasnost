#! /usr/bin/env python

# TODO: support attr-translate

import os
import sgmllib
import sys

directory = sys.argv[1]

translations = {}


class ParserForTranslations(sgmllib.SGMLParser):
    def __init__(self, body):
        sgmllib.SGMLParser.__init__(self)
        self.translation = 0
        self.feed(body)

    def handle_data(self, data):
        if not self.translation:
            return
        self.translation = 0
        translations[data.strip()] = None

    def unknown_starttag(self, tag, attrs):
        ats = [x for x in attrs if x[0] == 'tal:translate' and not x[1]]
        if ats:
            self.translation = 1
            return
        ats = [x for x in attrs if x[0] == 'tal:attr-translate']
        if ats:
            ats = ats[0][1].split()
            for a in ats:
                la = [x for x in attrs if x[0] == a]
                if not la:
                    continue
                translations[la[0][1].strip()] = ''


if hasattr(os, 'walk'):
    osWalk = os.walk
else:
    # Python < 2.3.
    # Directly copied from Python2.3 os.walk function, but without yield.

    def osWalk(top, topdown=1, onerror=None):
        """Directory tree generator."""

        paths = []

        from os.path import join, isdir, islink

        try:
            names = os.listdir(top)
        except os.error, err:
            if onerror is not None:
                onerror(err)
            return

        dirs, nondirs = [], []
        for name in names:
            if isdir(join(top, name)):
                dirs.append(name)
            else:
                nondirs.append(name)

        if topdown:
            paths.append((top, dirs, nondirs))
        for name in dirs:
            path = join(top, name)
            if not islink(path):
                for x in osWalk(path, topdown, onerror):
                    paths.append(x)
        if not topdown:
            paths.append((top, dirs, nondirs))
        return paths


for root, dir, files in osWalk(directory):
    for file in files:
        if not file.endswith('.tal') and not file.endswith('.html'):
            continue
        file = os.path.join(root, file)
        ParserForTranslations(open(file).read())

for t in translations.keys():
    print '_("""%s""")' % t



