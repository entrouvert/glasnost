# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Proxy Kinds"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.kinds as commonKinds

import things

## import connectors # Do not remove!
import functions # Do not remove!
import modes # Do not remove!
import properties # Do not remove!
import uploads # Do not remove!
import values # Do not remove!
import widgets # Do not remove!


register = things.register


class KindMixin(things.ThingMixin):
    def isExportable(self):
        return self.importExport in ['public', 'to-server-only']

    def isImportable(self):
        return self.importExport in ['from-server-only', 'public']


class AbstractSequenceMixin(KindMixin):
    pass


class ChoiceMixin(KindMixin):
    pass


class IdMixin(KindMixin):
    pass


class PathMixin(KindMixin):
    pass


class SequenceMixin(AbstractSequenceMixin):
    pass


class StringMixin(KindMixin):
    pass


class ThingMixin(ChoiceMixin):
    pass


class TimeMixin(KindMixin):
    pass


class TokenMixin(KindMixin):
    pass


class UsersSetMixin(SequenceMixin):
    pass


class BaseKind(KindMixin, commonKinds.BaseKind):
    pass
register(BaseKind)


class AcceptedRoles(SequenceMixin, commonKinds.AcceptedRoles):
    pass
register(AcceptedRoles)


class Alias(StringMixin, commonKinds.Alias):
    pass
register(Alias)


class Any(KindMixin, commonKinds.Any):
    pass
register(Any)


class ApplicationToken(TokenMixin, commonKinds.ApplicationToken):
    pass
register(ApplicationToken)


## class ArgumentPins(SequenceMixin, commonKinds.ArgumentPins):
##     pass
## register(ArgumentPins)


class AuthorsSet(UsersSetMixin, commonKinds.AuthorsSet):
    pass
register(AuthorsSet)


class Boolean(KindMixin, commonKinds.Boolean):
    pass
register(Boolean)


class Choice(ChoiceMixin, commonKinds.Choice):
    pass
register(Choice)


class Command(KindMixin, commonKinds.Command):
    pass
register(Command)


class CreationTime(TimeMixin, commonKinds.CreationTime):
    pass
register(CreationTime)


class Data(KindMixin, commonKinds.Data):
    pass
register(Data)


class DispatcherId(KindMixin, commonKinds.DispatcherId):
    pass
register(DispatcherId)


class Duration(KindMixin, commonKinds.Duration):
    pass
register(Duration)


class Email(StringMixin, commonKinds.Email):
    pass
register(Email)


class Fault(ThingMixin, commonKinds.Fault):
    pass
register(Fault)


class Fields(KindMixin, commonKinds.Fields):
    pass
register(Fields)


class FilePath(PathMixin, commonKinds.FilePath):
    pass
register(FilePath)


class Fingerprint(StringMixin, commonKinds.Fingerprint):
    pass
register(Fingerprint)


class Float(KindMixin, commonKinds.Float):
    pass
register(Float)


class Id(IdMixin, commonKinds.Id):
    pass
register(Id)


## class InputConnector(ThingMixin, commonKinds.InputConnector):
##     pass
## register(InputConnector)


class Integer(KindMixin, commonKinds.Integer):
    pass
register(Integer)


class IntegerChoice(ChoiceMixin, commonKinds.IntegerChoice):
    pass
register(IntegerChoice)


class Kind(ThingMixin, commonKinds.Kind):
    pass
register(Kind)


class KindName(ChoiceMixin, commonKinds.KindName):
    pass
register(KindName)


class LanguageChoice(ChoiceMixin, commonKinds.LanguageChoice):
    pass
register(LanguageChoice)


class Link(KindMixin, commonKinds.Link):
    pass
register(Link)


class Mapping(KindMixin, commonKinds.Mapping):
    pass
register(Mapping)


class Marks(KindMixin, commonKinds.Marks):
    pass
register(Marks)


class Memory(KindMixin, commonKinds.Memory):
    pass
register(Memory)


class Mode(ThingMixin, commonKinds.Mode):
    pass
register(Mode)


class ModificationTime(TimeMixin, commonKinds.ModificationTime):
    pass
register(ModificationTime)


## class OutputConnector(ThingMixin, commonKinds.OutputConnector):
##     pass
## register(OutputConnector)


class PairwiseMatrix(KindMixin, commonKinds.PairwiseMatrix):
    pass
register(PairwiseMatrix)


class Password(StringMixin, commonKinds.Password):
    pass
register(Password)


class Path(PathMixin, commonKinds.Path):
    pass
register(Path)


class Properties(SequenceMixin, commonKinds.Properties):
    pass
register(Properties)


class PythonIdentifier(StringMixin, commonKinds.PythonIdentifier):
    pass
register(PythonIdentifier)


class Rating(KindMixin, commonKinds.Rating):
    pass
register(Rating)


class ReadersSet(UsersSetMixin, commonKinds.ReadersSet):
    pass
register(ReadersSet)


## class ResultPins(SequenceMixin, commonKinds.ResultPins):
##     pass
## register(ResultPins)


class Script(StringMixin, commonKinds.Script):
    pass
register(Script)


class ScriptSourceCode(StringMixin, commonKinds.ScriptSourceCode):
    pass
register(ScriptSourceCode)


class Sequence(SequenceMixin, commonKinds.Sequence):
    pass
register(Sequence)


class ServerId(IdMixin, commonKinds.ServerId):
    pass
register(ServerId)


class ServerRole(ChoiceMixin, commonKinds.ServerRole):
    pass
register(ServerRole)


class SlotName(ChoiceMixin, commonKinds.SlotName):
    pass
register(SlotName)


class Source(KindMixin, commonKinds.Source):
    pass
register(Source)


class String(StringMixin, commonKinds.String):
    pass
register(String)


class Structure(AbstractSequenceMixin, commonKinds.Structure):
    pass
register(Structure)


class Time(TimeMixin, commonKinds.Time):
    pass
register(Time)


class Thing(ThingMixin, commonKinds.Thing):
    pass
register(Thing)


class Token(TokenMixin, commonKinds.Token):
    pass
register(Token)


class TranslationToAdd(KindMixin, commonKinds.TranslationToAdd):
    pass
register(TranslationToAdd)


class TranslatorsSets(KindMixin, commonKinds.TranslatorsSets):
    pass
register(TranslatorsSets)


class Union(KindMixin, commonKinds.Union):
    pass
register(Union)


class Upload(ThingMixin, commonKinds.Upload):
    pass
register(Upload)


class UsersSet(UsersSetMixin, commonKinds.UsersSet):
    pass
register(UsersSet)


class UserToken(TokenMixin, commonKinds.UserToken):
    pass
register(UserToken)


class ValueHolder(ThingMixin, commonKinds.ValueHolder):
    pass
register(ValueHolder)


class Widget(ThingMixin, commonKinds.Widget):
    pass
register(Widget)


class WidgetName(ChoiceMixin, commonKinds.WidgetName):
    pass
register(WidgetName)


class WritersSet(UsersSetMixin, commonKinds.WritersSet):
    pass
register(WritersSet)


class XChoice(ChoiceMixin, commonKinds.XChoice):
    pass
register(XChoice)


