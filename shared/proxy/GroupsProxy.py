# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Groups Proxy"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.GroupsCommon as commonGroups
import glasnost.common.tools_new as commonTools
import glasnost.common.system as system

from DispatcherProxy import callServer, getApplicationToken
import ObjectsProxy as objects
from tools import *


class AdminGroups(objects.AdminMixin, commonGroups.AdminGroups):
    pass
objects.register(AdminGroups)


class GroupMixin(objects.ObjectProxyMixin):
    def getGroup(self, groupId):
        return getProxy(groupId).getObject(groupId)


class GroupAll(GroupMixin, commonGroups.GroupAll):
    pass
objects.register(GroupAll)


class GroupDelta(GroupMixin, commonGroups.GroupDelta):
    pass
objects.register(GroupDelta)


class GroupIntersection(GroupMixin, commonGroups.GroupIntersection):
    pass
objects.register(GroupIntersection)


class GroupRole(GroupMixin, commonGroups.GroupRole):
    pass
objects.register(GroupRole)


class GroupUnion(GroupMixin, commonGroups.GroupUnion):
    pass
objects.register(GroupUnion)



class GroupsProxy(commonGroups.GroupsCommonMixin, objects.ObjectsProxy):
    def addObjectMember(self, objectId, memberId):
        userToken = context.getVar('userToken', default = '')
        callServer(
                commonTools.extractServerId(objectId),
                'addObjectMember',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, memberId])

    def deleteObjectMember(self, objectId, memberId):
        userToken = context.getVar('userToken', default = '')
        callServer(
                commonTools.extractServerId(objectId),
                'deleteObjectMember',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, memberId])

    def getObjectIdsWithContent(self, objectId, indirect = 0, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getObjectIdsWithContent',
                [serverId, getApplicationToken(), userToken, objectId,
                 indirect])

    def getObjectsWithContent(self, objectId, indirect = 0, serverId = None):
        objectIds = self.getObjectIdsWithContent(
                objectId, indirect = indirect, serverId = serverId)
        multiCall = MultiCall()
        for objectId in objectIds:
            self.getObject(objectId, multiCall = multiCall)
        lazyObjects = multiCall.call()
        objects = {}
        for i in range(len(objectIds)):
            objects[objectIds[i]] = lazyObjects[i]()
        return objects

    def objectContains(self, objectId, containedId):
        userToken = context.getVar('userToken', default = '')
        return callServer(
                commonTools.extractServerId(objectId),
                'objectContains',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, containedId])


def getSetContainedIds(set, serverRoles = None, raiseWhenUncountable = 0):
    """Return the ids of the items contained in a set."""

    group = GroupUnion()
    group.membersSet = set
    return group.getContainedIds(serverRoles = serverRoles,
                                 raiseWhenUncountable = raiseWhenUncountable)


def setContains(set, objectId, serverId = None):
    if objectId in set or system.generalPublicId in set:
        return 1
    cache = context.getVar('temporaryCache')
    cacheKey = 'setContains-%s-%s' % (repr(set), objectId)
    if cache is not None and cache.has_key(cacheKey):
        return cache[cacheKey]
    userToken = context.getVar('userToken', default = '')
    serverId = GroupsProxy().getServerId(serverId = serverId)
    result = callServer(serverId, 'setContains',
                [serverId, getApplicationToken(), userToken, set, objectId])
    if cache is not None:
        cache[cacheKey] = result
    return result

