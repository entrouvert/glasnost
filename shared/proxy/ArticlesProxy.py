# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Articles Proxy"""

__version__ = '$Revision$'[11:-2]


from glasnost.common.ArticlesCommon import *
import glasnost.common.context as context
import glasnost.common.system as system
import glasnost.common.tools_new as commonTools

from DispatcherProxy import MultiCall, callServer, getApplicationToken
from ObjectsProxy import register, AdminMixin, ObjectProxyMixin, ObjectsProxy
from tools import *


class AdminArticles(AdminMixin, AdminArticlesCommon):
    pass
register(AdminArticles)


class Article(ObjectProxyMixin, ArticleCommon):
    pass
register(Article)


class ArticlesProxy(ArticlesCommonMixin, ObjectsProxy):
    def canGetObjectHistory(self, objectId):
        userToken = context.getVar('userToken', default = '')
        return callServer(
            commonTools.extractServerId(objectId),
            'canGetObjectHistory',
            [commonTools.extractServerId(objectId), getApplicationToken(),
             userToken, objectId])

    def getLastObjectIds(
            self, objectsCount, possibleAuthorsSet, possibleReadersSet,
            possibleWritersSet, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        if not possibleAuthorsSet:
            possibleAuthorsSet = [system.generalPublicId]
        if not possibleReadersSet:
            possibleReadersSet = [system.generalPublicId]
        if not possibleWritersSet:
            possibleWritersSet = [system.generalPublicId]
        return callServer(
            serverId,
            'getLastObjectIds',
            [serverId, getApplicationToken(), userToken,
             objectsCount, possibleAuthorsSet, possibleReadersSet,
             possibleWritersSet])

    def getLastObjects(
            self, objectsCount, possibleAuthorsSet, possibleReadersSet,
            possibleWritersSet, requiredSlotNames = None, serverId = None):
        objectIds = self.getLastObjectIds(
            objectsCount, possibleAuthorsSet, possibleReadersSet,
            possibleWritersSet, serverId = serverId)
        multiCall = MultiCall()
        for objectId in objectIds:
            self.getPartialObject(
                objectId, requiredSlotNames, multiCall = multiCall)
        return [lazyObject() for lazyObject in multiCall.call()]

    def getObjectDocBookChapter(self, objectId):
        userToken = context.getVar('userToken', default = '')
        return iso8859_15(callServer(
            commonTools.extractServerId(objectId),
            'getObjectDocBookChapter',
            [commonTools.extractServerId(objectId), getApplicationToken(),
             userToken, objectId]))

    def getObjectLatexChapter(self, objectId):
        userToken = context.getVar('userToken', default = '')
        return iso8859_15(callServer(
            commonTools.extractServerId(objectId),
            'getObjectLatexChapter',
            [commonTools.extractServerId(objectId), getApplicationToken(),
             userToken, objectId]))

    def search(self, searchTerms, scope, language, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        result = callServer(
            serverId,
            'search',
            [serverId, getApplicationToken(), userToken, utf8(searchTerms),
             scope, language])
        return result

