# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Cache Proxy"""

__version__ = '$Revision$'[11:-2]


import xmlrpclib

import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.tools_new as commonTools

from DispatcherProxy import MultiCall, callServer, getApplicationToken

from tools import *

cacheIsThere = 1


def getServerId(serverId = None):
    if serverId is not None:
        return commonTools.makeApplicationId(serverId, 'cache')
    return commonTools.makeApplicationId(
            context.getVar('dispatcherId'), 'cache')


def invalidateKeyStart(cacheKey):
    global cacheIsThere
    if not cacheIsThere: return
    userToken = context.getVar('userToken', default = '')
    serverId = getServerId()
    try:
        return callServer(
            serverId,
            'invalidateKeyStart',
            [serverId, getApplicationToken(), userToken, cacheKey])
    except faults.UnknownServerId:
        # It means that the cache server isn't available.
        cacheIsThere = 0


def invalidateValue(objectId):
    global cacheIsThere
    if not cacheIsThere: return
    if context.getVar('dispatcherId') is not None:
        serverId = None
    else:
        serverId = objectId
    userToken = context.getVar('userToken', default = '')
    serverId = getServerId(serverId = serverId)
    try:
        return callServer(
                serverId,
                'invalidateValue',
                [serverId, getApplicationToken(), userToken, objectId])
    except faults.UnknownServerId:
        # It means that the cache server isn't available.
        cacheIsThere = 0


def isValueStillValid(objectId, timeCached, serverId = None, multiCall = None):
    userToken = context.getVar('userToken', default = '')
    serverId = getServerId(serverId = serverId)
    return callServer(
            serverId,
            'isValueStillValid',
            [serverId, getApplicationToken(), userToken, objectId, timeCached],
            resultHandler = lambda x: x(),
            multiCall = multiCall)


# Used only by getProxyForServerRole().
class CacheProxy:
    def canGetAdmin(self, serverId = None):
        return 0

    def canViewAll(self, serverId = None):
        return 0

