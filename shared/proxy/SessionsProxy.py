# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Sessions Proxy"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context

from DispatcherProxy import callServer, getApplicationToken
from ObjectsProxy import Proxy


class SessionsProxy(Proxy):
    serverRole = 'sessions'

    def addTemporaryData(self, data, serverId = None):
        userToken = context.getVar('userToken', default = '')
        sessionToken = context.getVar('sessionToken')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'addTemporaryData',
                [serverId, getApplicationToken(), userToken,
                 sessionToken, data])

    def delTemporaryData(self, dataToken, serverId = None):
        userToken = context.getVar('userToken', default = '')
        sessionToken = context.getVar('sessionToken')
        serverId = self.getServerId(serverId = serverId)
        callServer(
                serverId,
                'delTemporaryData',
                [serverId, getApplicationToken(), userToken,
                 sessionToken, dataToken])

    def getTemporaryData(self, dataToken, serverId = None):
        userToken = context.getVar('userToken', default = '')
        sessionToken = context.getVar('sessionToken')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getTemporaryData',
                [serverId, getApplicationToken(), userToken,
                 sessionToken, dataToken])

    def deleteSession(self, objectToken, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        callServer(
                serverId,
                'deleteObject',
                [serverId, getApplicationToken(), userToken, objectToken])

    def getHistory(self, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getHistory',
                [serverId, getApplicationToken(), userToken])

    def getSession(self, objectToken, ipAddress, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        object = callServer(
                serverId,
                'getObject',
                [serverId, getApplicationToken(), userToken, objectToken,
                 ipAddress])
        return object

    def newSession(self, ipAddress, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'newObject',
                [serverId, getApplicationToken(), userToken, ipAddress])

    def setSession(self, object, serverId = None):
        # The isDirty flag is used to remove conflicts when a page embeds
        # another object (like an image).
        # Whithout isDirty flag, when editing, this can produce something like:
        # s1 = getSession <- beginning of page handling
        # s2 = getSession <- beginning of image handling
        # modification of s1
        # setSession(s1) <- end of page handling
        # setSession(s2) <- end of image handling => destroys s1 changes!
        if not object.has_key('isDirty'):
            return
        del object['isDirty']
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        object['version'] = callServer(
                serverId,
                'setObject',
                [serverId, getApplicationToken(), userToken, object])
        return None

