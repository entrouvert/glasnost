# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Password Accounts Proxy"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.PasswordAccountsCommon as commonPasswordAccounts

from DispatcherProxy import callServer, getApplicationToken
import ObjectsProxy as objects
from tools import utf8


class AdminPasswordAccounts(objects.AdminMixin,
                            commonPasswordAccounts.AdminPasswordAccounts):
    pass
objects.register(AdminPasswordAccounts)


class PasswordAccount(objects.ObjectProxyMixin,
                      commonPasswordAccounts.PasswordAccount):
    pass
objects.register(PasswordAccount)


class PasswordAccountsProxy(commonPasswordAccounts.PasswordAccountsCommonMixin,
                            objects.ObjectsProxy):

    def canChangePassword(self, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'canChangePassword',
                [serverId, getApplicationToken(), userToken])



    def changePassword(self, currentPassword, newPassword, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId, 
                'changePassword',
                [serverId, getApplicationToken(), userToken,
                 utf8(currentPassword), utf8(newPassword)])

    def checkObjectAuthentication(self, login, password, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        if login is None:
            loginExport = ''
        else:
            loginExport = utf8(login)
        if password is None:
            passwordExport = ''
        else:
            passwordExport = utf8(password)
        return callServer(
                serverId, 
                'checkObjectAuthentication',
                [serverId, getApplicationToken(), userToken, loginExport,
                 passwordExport])

    def emailPassword(self, login, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'emailPassword',
                [serverId, getApplicationToken(), userToken, utf8(login)] )

