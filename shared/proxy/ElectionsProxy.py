# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Elections Proxy"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
from glasnost.common.ElectionsCommon import *
import glasnost.common.system as system
import glasnost.common.tools_new as commonTools

from DispatcherProxy import MultiCall, callServer, getApplicationToken
from ObjectsProxy import register, AdminMixin, ObjectProxyMixin, ObjectsProxy
from tools import *
import VotesProxy # Do not remove!


class AdminElections(AdminMixin, AdminElectionsCommon):
    pass
register(AdminElections)


class ElectionMixin(ObjectProxyMixin):
    pass


class AbstractElection(ElectionMixin, AbstractElectionCommon):
    pass
register(AbstractElection)


class ElectionAverage(ElectionMixin, ElectionAverageCommon):
    pass
register(ElectionAverage)


class ElectionCondorcet(ElectionMixin, ElectionCondorcetCommon):
    pass
register(ElectionCondorcet)


class ElectionsProxy(ElectionsCommonMixin, ObjectsProxy):
    def abstainForVote(self, objectId, previousVoteToken):
        userToken = '' # The voter must be kept anonymous.
        callServer(
                commonTools.extractServerId(objectId),
                'abstainForVote',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, previousVoteToken])

    def canVote(self, objectId):
        userToken = context.getVar('userToken', default = '')
        return callServer(
                commonTools.extractServerId(objectId),
                'canVote',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId])

    def computeObject(self, objectId):
        userToken = context.getVar('userToken', default = '')
        callServer(
                commonTools.extractServerId(objectId),
                'computeObject',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId])
        cache = context.getVar('cache')
        if cache and cache.has_key(objectId):
            del cache[objectId]


    def getBallotKind(self, objectId):
        userToken = context.getVar('userToken', default = '')
        return callServer(
                commonTools.extractServerId(objectId),
                'getBallotKind',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId])

    def getLastObjectIds(
            self, objectsCount, possibleAuthorsSet, possibleReadersSet,
            possibleWritersSet, possibleStates, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        if not possibleAuthorsSet:
            possibleAuthorsSet = [system.generalPublicId]
        if not possibleReadersSet:
            possibleReadersSet = [system.generalPublicId]
        if not possibleWritersSet:
            possibleWritersSet = [system.generalPublicId]
        if possibleStates is None:
            possibleStates = []
        return callServer(
                serverId,
                'getLastObjectIds',
                [serverId, getApplicationToken(), userToken, objectsCount,
                 possibleAuthorsSet, possibleReadersSet, possibleWritersSet,
                 possibleStates])

    def getLastObjectIdsWithVoter(self, objectsCount, voterId, possibleStates,
                                  serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        if possibleStates is None:
            possibleStates = []
        return callServer(
                serverId,
                'getLastObjectIdsWithVoter',
                [serverId, getApplicationToken(), userToken, objectsCount,
                 voterId, possibleStates])

    def getLastObjects(
            self, objectsCount, possibleAuthorsSet, possibleReadersSet,
            possibleWritersSet, possibleStates, requiredSlotNames = None,
        serverId = None):
        objectIds = self.getLastObjectIds(
                objectsCount, possibleAuthorsSet, possibleReadersSet,
                possibleWritersSet, possibleStates, serverId = serverId)
        multiCall = MultiCall()
        for objectId in objectIds:
            self.getPartialObject(
                    objectId, requiredSlotNames, multiCall = multiCall)
        return [lazyObject() for lazyObject in multiCall.call()]

    def getLastObjectsWithVoter(
            self, objectsCount, voterId, possibleStates,
            requiredSlotNames = None, serverId = None):
        objectIds = self.getLastObjectIdsWithVoter(
                objectsCount, voterId, possibleStates,
                serverId = serverId)
        multiCall = MultiCall()
        for objectId in objectIds:
            self.getPartialObject(
                    objectId, requiredSlotNames, multiCall = multiCall)
        return [lazyObject() for lazyObject in multiCall.call()]

    def getVote(self, objectId, voteToken):
        userToken = context.getVar('userToken', default = '')
        voteImport = callServer(
                commonTools.extractServerId(objectId),
                'getVote',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, voteToken])
        return commonTools.importThing(voteImport)

    def getVotesProxy(self):
        from VotesProxy import votesProxy
        return votesProxy

    def pesterAbstentionnists(self, objectId):
        userToken = context.getVar('userToken', default = '')
        httpHostName = context.getVar('httpHostName')
        httpPort = context.getVar('httpPort')
        httpScriptDirectoryPath = context.getVar(
                'httpScriptDirectoryPath')
        if httpHostName is None:
            httpHostName = ''
        else:
            httpHostName = utf8(httpHostName)
        if httpPort is None:
            httpPort = ''
        else:
            httpPort = utf8(httpPort)
        if httpScriptDirectoryPath is None:
            httpScriptDirectoryPath = ''
        else:
            httpScriptDirectoryPath = utf8(httpScriptDirectoryPath)
        return callServer(
                commonTools.extractServerId(objectId),
                'pesterAbstentionnists',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, httpHostName, httpPort,
                 httpScriptDirectoryPath])

    def vote(self, objectId, voteToken, previousVoteToken):
        userToken = '' # The voter must be kept anonymous.
        return callServer(
                commonTools.extractServerId(objectId),
                'vote',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId, voteToken, previousVoteToken])

