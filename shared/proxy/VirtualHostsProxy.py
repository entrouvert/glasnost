# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Virtual Hosts Proxy"""

__version__ = '$Revision$'[11:-2]


import copy

import glasnost.common.tools_new as commonTools
from glasnost.common.VirtualHostsCommon import *

from DispatcherProxy import callServer, getApplicationToken
from ObjectsProxy import register, AdminMixin, ObjectProxyMixin, ObjectsProxy
from tools import *


class AdminVirtualHosts(AdminMixin, AdminVirtualHostsCommon):
    pass
register(AdminVirtualHosts)


class VirtualHost(ObjectProxyMixin, VirtualHostCommon):
    pass
register(VirtualHost)


class VirtualHostsProxy(VirtualHostsCommonMixin, ObjectsProxy):
    def canDeleteObject(self, objectId):
        userToken = context.getVar('userToken', default = '')
        serverId = '%s/virtualhosts' % context.getVar('dispatcherId')
        return callServer(
                serverId,
                'canDeleteObject',
                [serverId, getApplicationToken(), userToken, objectId])

    def canGetObject(self, objectId):
        userToken = context.getVar('userToken', default = '')
        serverId = '%s/virtualhosts' % context.getVar('dispatcherId')
        return callServer(
                serverId,
                'canGetObject',
                [serverId, getApplicationToken(), userToken, objectId])

    def canModifyObject(self, objectId):
        userToken = context.getVar('userToken', default = '')
        serverId = '%s/virtualhosts' % context.getVar('dispatcherId')
        return callServer(
                serverId,
                'canModifyObject',
                [serverId, getApplicationToken(), userToken, objectId])

    def getHostName(self, serverId):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return iso8859_15(callServer(
                serverId,
                'getHostName',
                [serverId, getApplicationToken(), userToken]))

    def getObject(self, objectId, multiCall = None):
        cache = context.getVar('cache')
        if cache is not None and cache.has_key(objectId):
            result = copy.deepcopy(cache[objectId])
            if multiCall:
                multiCall.addEarlyResult(result)
                return None
            else:
                return result
        userToken = context.getVar('userToken', default = '')
        serverId = '%s/virtualhosts' % context.getVar('dispatcherId')
        return callServer(
                serverId,
                'getObject',
                [serverId, getApplicationToken(), userToken, objectId],
                resultHandler = self.getObject_handleResult,
                multiCall = multiCall)

    def getObjectByHostName(self, hostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        objectImport = callServer(
                serverId,
                'getObjectByHostName',
                [serverId, getApplicationToken(), userToken, utf8(hostName)])
        return commonTools.importThing(objectImport)

    def getObjectIdByHostName(self, hostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getObjectIdByHostName',
                [serverId, getApplicationToken(), userToken, utf8(hostName)])

    def getObjectLabelAndLanguage(self, objectId, multiCall = None):
        serverId = '%s/virtualhosts' % context.getVar('dispatcherId')
        userToken = context.getVar('userToken', default = '')
        if not objectId:
            if multiCall is None:
                return '', None
            else:
                return multiCall.addEarlyResult(('', None))
        def handleResult(lazyLabelAndLanguage):
            label, language = lazyLabelAndLanguage()
            if not language:
                language = None
            return iso8859_15(label), language
        return callServer(
                serverId,
                'getObjectLabelAndLanguage',
                [serverId, getApplicationToken(), userToken, objectId],
                resultHandler = handleResult,
                multiCall = multiCall)

    def getPartialObject(self, objectId, requiredSlotNames, multiCall = None):
        return self.getObject(objectId, multiCall = multiCall)

    def hasDispatcherId(self, dispatcherId, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'hasDispatcherId',
                [serverId, getApplicationToken(), userToken,
                 utf8(dispatcherId)])

    def hasHostName(self, hostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'hasHostName',
                [serverId, getApplicationToken(), userToken, utf8(hostName)])

    def hasObject(self, objectId):
        userToken = context.getVar('userToken', default = '')
        serverId = '%s/virtualhosts' % context.getVar('dispatcherId')
        try:
            return callServer(
                serverId,
                'hasObject',
                [serverId, getApplicationToken(), userToken, objectId])
        except:
            from glasnost.proxy.DispatcherProxy import serverAccessors
            raise repr(serverAccessors)

