# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Identities Proxy"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.IdentitiesCommon as commonIdentities
import glasnost.common.tools_new as commonTools

from DispatcherProxy import callServer, getApplicationToken
import ObjectsProxy as objects
import things


class AdminIdentities(objects.AdminMixin, commonIdentities.AdminIdentities):
    pass
objects.register(AdminIdentities)


class Identification(things.ThingMixin, commonIdentities.Identification):
    pass
things.register(Identification)


class Identity(objects.ObjectProxyMixin, commonIdentities.Identity):
    pass
objects.register(Identity)


class IdentitiesProxy(commonIdentities.IdentitiesCommonMixin,
                      objects.ObjectsProxy):
    
    def abstainForVote(self, electionId):
        userToken = context.getVar('userToken', default = '')
        serverId = self.computeServerIdFromUserToken(userToken)
        callServer(
                serverId,
                'abstainForVote',
                [serverId, getApplicationToken(), userToken, electionId])

    def changeStatus(self, objectId, statusAction):
        userToken = context.getVar('userToken', default = '')
        serverId = commonTools.extractServerId(objectId)
        callServer(
                serverId,
                'changeStatus',
                [serverId, getApplicationToken(), userToken, objectId,
                 statusAction])

    def checkIdentityLocalNameIdentifierIDP(
            self, peerHostName, localNameIdentifier, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'checkIdentityLocalNameIdentifierIDP',
                [serverId, getApplicationToken(), userToken, peerHostName,
                 localNameIdentifier])

    def checkIdentityPeerNameIdentifierIDP(
            self, peerHostName, peerNameIdentifier, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'checkIdentityPeerNameIdentifierIDP',
                [serverId, getApplicationToken(), userToken, peerHostName,
                 peerNameIdentifier])

    def checkIdentityLocalNameIdentifierSP(
            self, peerHostName, localNameIdentifier, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'checkIdentityLocalNameIdentifierSP',
                [serverId, getApplicationToken(), userToken, peerHostName,
                 localNameIdentifier])

    def checkIdentityPeerNameIdentifierSP(
            self, peerHostName, peerNameIdentifier, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'checkIdentityPeerNameIdentifierSP',
                [serverId, getApplicationToken(), userToken, peerHostName,
                 peerNameIdentifier])

    def computeServerIdFromUserToken(self, userToken):
        return commonTools.makeApplicationId(userToken, self.serverRole)

    def deleteUserToken(self):
        userToken = context.getVar('userToken', default = '')
        if not userToken:
            return
        serverId = self.computeServerIdFromUserToken(userToken)
        callServer(
                serverId,
                'deleteUserToken',
                [serverId, getApplicationToken(), userToken])

    def getAdminIdentitiesRoles(self, serverId = None):
        serverId = self.getServerId(serverId = serverId)
        userToken = context.getVar('userToken', default = '')
        return callServer(
                serverId,
                'getAdminIdentitiesRoles',
                [serverId, getApplicationToken(), userToken])

    def getDefaultAssertionConsumerServiceUrl(self, electionId):
        userToken = context.getVar('userToken', default = '')
        serverId = self.computeServerIdFromUserToken(userToken)
        return callServer(
                serverId,
                'getDefaultAssertionConsumerServiceUrl',
                [serverId, getApplicationToken(), userToken, electionId])

    def getElectionVoteToken(self, electionId):
        userToken = context.getVar('userToken', default = '')
        serverId = self.computeServerIdFromUserToken(userToken)
        return callServer(
                serverId,
                'getElectionVoteToken',
                [serverId, getApplicationToken(), userToken, electionId])

    def getLocalNameIdentifierIdentityProvider(
            self, peerHostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getLocalNameIdentifierIdentityProvider',
                [serverId, getApplicationToken(), userToken, peerHostName])

    def getLocalNameIdentifierServiceProvider(
            self, peerHostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getLocalNameIdentifierServiceProvider',
                [serverId, getApplicationToken(), userToken, peerHostName])

    def getObjectEmail(self, objectId):
        userToken = context.getVar('userToken', default = '')
        return callServer(
                commonTools.extractServerId(objectId),
                'getObjectEmail',
                [commonTools.extractServerId(objectId), getApplicationToken(),
                 userToken, objectId])

    def getPeerNameIdentifierIdentityProvider(
            self, peerHostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getPeerNameIdentifierIdentityProvider',
                [serverId, getApplicationToken(), userToken, peerHostName])

    def getPeerNameIdentifierServiceProvider(
            self, peerHostName, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getPeerNameIdentifierServiceProvider',
                [serverId, getApplicationToken(), userToken, peerHostName])

    def getPersonId(self):
        userToken = context.getVar('userToken', default = '')
        if not userToken:
            return ''
        serverId = self.computeServerIdFromUserToken(userToken)
        return callServer(
                serverId,
                'getPersonId',
                [serverId, getApplicationToken(), userToken])

    def getPrivateKeySSL(self, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getPrivateKeySSL',
                [serverId, getApplicationToken(), userToken])

    def getRoles(self, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getRoles',
                [serverId, getApplicationToken(), userToken])

    def getSymetricKey(self, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'getSymetricKey',
                [serverId, getApplicationToken(), userToken])

    def getUserId(self):
        userToken = context.getVar('userToken', default = '')
        if not userToken:
            return ''
        serverId = self.computeServerIdFromUserToken(userToken)
        return callServer(
                serverId,
                'getUserId',
                [serverId, getApplicationToken(), userToken])

    def getUserToken(self, id):
        userToken = ''
        serverId = commonTools.extractServerId(id)
        return callServer(
                serverId,
                'getUserToken',
                [serverId, getApplicationToken(), userToken, id])

    def rememberId(self, id, serverId = None):
        userToken = context.getVar('userToken', default = '')
        if not userToken:
            return
        serverId = self.computeServerIdFromUserToken(userToken)
        callServer(
                serverId,
                'rememberId',
                [serverId, getApplicationToken(), userToken, id])

    def setContainsUser(self, set, serverId = None):
        cache = context.getVar('temporaryCache')
        cacheKey = 'setContainsUser-%s' % repr(set)
        if cache is not None and cache.has_key(cacheKey):
            return cache[cacheKey]
        userToken = context.getVar('userToken', default = '')
        if set is None:
            return 0
        serverId = self.getServerId(serverId = serverId)
        result = callServer(
                    serverId,
                    'setContainsUser',
                    [serverId, getApplicationToken(), userToken, set])
        if cache is not None:
            cache[cacheKey] = result
        return result

    def setLocalNameIdentifierIdentityProvider(
            self, peerHostName, newLocalNameIdentifier,
            serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'setLocalNameIdentifierIdentityProvider',
                [serverId, getApplicationToken(), userToken,
                     peerHostName, newLocalNameIdentifier])

    def setLocalNameIdentifierServiceProvider(
            self, peerHostName, newLocalNameIdentifier,
            serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'setLocalNameIdentifierServiceProvider',
                [serverId, getApplicationToken(), userToken,
                     peerHostName, newLocalNameIdentifier])

    def setPeerNameIdentifierIdentityProvider(
            self, peerHostName, newPeerNameIdentifier,
            oldPeerNameIdentifier, idpProvidedNameIdentifier, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'setPeerNameIdentifierIdentityProvider',
                [serverId, getApplicationToken(), userToken,
                 peerHostName,
                 newPeerNameIdentifier,
                 oldPeerNameIdentifier,
                 idpProvidedNameIdentifier])

    def setPeerNameIdentifierServiceProvider(
            self, peerHostName, newPeerNameIdentifier, oldPeerNameIdentifier,
            serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                serverId,
                'setPeerNameIdentifierServiceProvider',
                [serverId, getApplicationToken(), userToken, peerHostName,
                     newPeerNameIdentifier, oldPeerNameIdentifier])

    def vote(self, electionId, voteToken):
        userToken = context.getVar('userToken', default = '')
        serverId = self.computeServerIdFromUserToken(userToken)
        callServer(
                serverId,
                'vote',
                [serverId, getApplicationToken(), userToken, electionId,
                 voteToken])

