# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Appointments Proxy"""

__version__ = '$Revision$'[11:-2]


from glasnost.common.AppointmentsCommon import *

from DispatcherProxy import MultiCall, callServer, getApplicationToken
from glasnost.proxy.GroupsProxy import setContains
from ObjectsProxy import register, AdminMixin, ObjectProxyMixin, ObjectsProxy
from tools import *


class AdminAppointments(AdminMixin, AdminAppointmentsCommon):
    pass
register(AdminAppointments)


class Appointment(ObjectProxyMixin, AppointmentCommon):
    def isForUser(self, userId):
        return setContains(self.resourcesSet, userId)
register(Appointment)


class AppointmentsProxy(AppointmentsCommonMixin, ObjectsProxy):

    def getAppointments(self, start = None, end = None,
                        participantIds = None, serverId = None):
        objectIds = self.getAppointmentIds(
                 start, end, participantIds, serverId = serverId)
        multiCall = MultiCall()
        for objectId in objectIds:
            self.getObject(objectId, multiCall = multiCall)
        lazyObjects = multiCall.call()
        objects = {}
        for i in range(len(objectIds)):
            objects[objectIds[i]] = lazyObjects[i]()
        return objects

    def getAppointmentIds(self, start = None, end = None,
                          participantIds = None, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        return callServer(
                    serverId,
                    'getAppointmentIds',
                    [serverId, getApplicationToken(), userToken,
                     start, end, participantIds])

    def updateFromVCalendar(self, vCalendar, serverId = None):
        userToken = context.getVar('userToken', default = '')
        serverId = self.getServerId(serverId = serverId)
        callServer(
            serverId,
            'updateFromVCalendar',
            [serverId, getApplicationToken(), userToken, vCalendar])

