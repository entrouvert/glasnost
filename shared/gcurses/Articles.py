# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

from ObjectsCurses import CursesScreen
from glasnost.common.tools import *
from glasnost.proxy.tools import getProxyForServerRole, getProxy
from glasnost.web.WebAPI import GlasnostObject
from tools import *

import curses

class ArticlesScreen(CursesScreen):
    
    def __init__(self, stdscr):
        CursesScreen.__init__(self, stdscr)
        self.loadArticles()
        self.keys = {
            curses.KEY_UP:    [ self.handleUp,   'Haut' ],
            curses.KEY_DOWN:  [ self.handleDown, 'Bas' ],
            'q':              [ self.quit,       'Quitter' ],
            'e':              [ self.editContent,'�diter body' ],
            'v':              [ self.viewArticle, 'Vue article' ],
        }

        self.topLine = 0
        self.selectedLine = 0
    
    def loadArticles(self):
        articlesProxy = getProxyForServerRole('articles')
        self.objects = [GlasnostObject(id) for id in articlesProxy.getObjectIds()]
        self.objects.sort(lambda x,y: cmp(x.label, y.label))
        
    def view(self, height, width):
        self.height = height

        if len(self.objects) == 0:
            return
        
        currentArticle = self.objects[self.selectedLine]

        for i, a in zip(range(height-3),
                self.objects[self.topLine:self.topLine+height-3]):
            options = 0
            if a is currentArticle:
                options = curses.color_pair(2) | curses.A_BOLD
            label = '   %s' % a.label
            self.stdscr.addstr(1+i, 0, label.ljust(width), options)
        
        self.stdscr.addstr(height-1, 0, currentArticle.objectId.ljust(width-1))
        

    def handleUp(self):
        if self.selectedLine == 0:
            return
        self.selectedLine -= 1
        if self.selectedLine - self.topLine < 0:
            self.topLine -= 1
    
    def handleDown(self):
        if self.selectedLine+1 == len(self.objects):
            return
        self.selectedLine += 1
        if self.selectedLine - self.topLine >= self.height-3:
            self.topLine += 1

    def quit(self):
        context.setVar('screens', context.getVar('screens')[:-1])

    def editContent(self):
        currentArticle = self.objects[self.selectedLine]
        proxy = getProxy(currentArticle.id)
        newBody = editText(currentArticle.body)
        if newBody != currentArticle.body:
            currentArticle.object.body = newBody
            proxy.modifyObject(currentArticle.object)
            self.objects[self.selectedLine] = GlasnostObject(currentArticle.objectId)
                
    def viewArticle(self):
        pass

