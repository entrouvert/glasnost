# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Glasnost Gtk Things"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.things as commonThings

import glasnost.proxy.things as proxyThings


from tools import *

class ThingClasses(commonThings.ThingClasses):
    def loadObjectModule(self, serverRoleDotobjectName):
        serverRole, objectName = serverRoleDotobjectName.split('.', 1)
        from tools import getGtkForServerRole
        gtk = getGtkForServerRole(serverRole)
        return gtk is not None


thingClasses = ThingClasses('gtk', proxyThings.thingClasses)
context.setVar('thingClasses', thingClasses)


def register(thingClass):
    thingClasses.register(thingClass)


class ThingMixin:
    def getEditLayoutHiddenSlotNames(self, parentSlot = None):
        slotNames = self.getEditLayoutSlotNames( parentSlot = parentSlot)
        isCreateEditMode = context.getVar('isCreateEditMode', default = 0)
        hiddenSlotNames = []
        for slotName in slotNames:
            slot = self.getSlot(slotName, parentSlot = parentSlot)
            kind = slot.getKind()
            if isCreateEditMode:
                if kind.stateInCreateMode == 'hidden' \
                        or (kind.stateInCreateMode is None
                            and kind.stateInEditMode == 'hidden'):
                    hiddenSlotNames.append(slotName)
            elif kind.stateInEditMode == 'hidden':
                hiddenSlotNames.append(slotName)
        return hiddenSlotNames

    def getEditLayoutReadOnlySlotNames(self, parentSlot = None):
        slotNames = self.getEditLayoutSlotNames(
            parentSlot = parentSlot)
        isCreateEditMode = context.getVar('isCreateEditMode', default = 0)
        readOnlySlotNames = []
        for slotName in slotNames:
            slot = self.getSlot(slotName, parentSlot = parentSlot)
            kind = slot.getKind()
            if isCreateEditMode:
                if kind.stateInCreateMode == 'read-only' \
                        or (kind.stateInCreateMode is None
                            and kind.stateInEditMode == 'read-only'):
                    readOnlySlotNames.append(slotName)
            elif kind.stateInEditMode == 'read-only':
                readOnlySlotNames.append(slotName)
        return readOnlySlotNames

    def getEditLayoutSlotNames(self, parentSlot = None):
        slotNames = self.getLayoutSlotNames( parentSlot = parentSlot)
        slotNamesToRemove = ['creationTime', 'lastEditorId','modificationTime']
            
        for slotName in slotNamesToRemove:
            if slotName in slotNames:
                slotNames.remove(slotName)
        return slotNames

    def getViewLayoutHiddenSlotNames(self, parentSlot = None):
        slotNames = self.getViewLayoutSlotNames(
             parentSlot = parentSlot)
        hiddenSlotNames = []
        for slotName in slotNames:
            slot = self.getSlot(slotName, parentSlot = parentSlot)
            kind = slot.getKind()
            if kind.stateInViewMode == 'hidden':
                hiddenSlotNames.append(slotName)
        return hiddenSlotNames

    def getViewLayoutSlotNames(self, parentSlot = None):
        return self.getLayoutSlotNames(parentSlot = parentSlot)

    def getViewLayoutVisibleSlotNames(self, parentSlot = None):
        slotNames = self.getViewLayoutSlotNames(parentSlot = parentSlot)
        visibleSlotNames = []
        for slotName in slotNames:
            slot = self.getSlot(slotName, parentSlot = parentSlot)
            kind = slot.getKind()
            if not kind.stateInViewMode == 'hidden':
                visibleSlotNames.append(slotName)
        return visibleSlotNames
    
    def getLayoutSlotNames(self,  parentSlot = None):
        allSlotNames = self.getSlotNames(parentSlot = parentSlot)
        allSlotNames = [x for x in allSlotNames if x != 'serverRole']
        orderedSlotNames = self.getOrderedLayoutSlotNames(
            parentSlot = parentSlot)
        slotNames = []
        for slotName in orderedSlotNames:
            if not slotName in allSlotNames:
                continue
            slotNames.append(slotName)
        for slotName in allSlotNames:
            if slotName in slotNames:
                continue
            slotNames.append(slotName)
        return slotNames


    def getViewTable(self):
        slotNames = self.getViewLayoutSlotNames(self)
        table = gtk.Table(len(slotNames), 2, gtk.FALSE)
        for slotName, i in zip(slotNames, range(len(slotNames))):
            slot = self.getSlot(slotName)
            widget = slot.getWidget()
            table.attach(widget.getWidgetLabel(slot), 0, 1, i, i+1)
            table.attach(widget.getWidgetView(slot), 1, 2, i, i+1)
        return table


class BaseThing(ThingMixin, proxyThings.BaseThing):
    pass
