# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Widgets for the glasnost-gui sessions manager"""

import errno
import gettext
from tools import *
import GlasnostGui
import os
import cPickle as pickle
import sessions

class SessionsManager(GlasnostGui.scaffold_view):
     
    def delete_event(self, widget = None, data = None):
        self._parentWindow.show()
        self.destroy()
    
    def __init__(self, parentWindow):
        GlasnostGui.scaffold_view.__init__(self)
        self.sessionsList = sessions.getSessionsList()

        self.set_position(gtk.WIN_POS_CENTER)
        self.connect('delete_event', self.delete_event)
        self._parentWindow = parentWindow
        self.set_title('Sessions Manager')
        self.currently_selected = None
        
        pixdir = context.getVar('imagesDirectory')
        button_vbox = gtk.VBox(FALSE, 5)
        button_vbox.show()
        
        #Widgets Instances        
        self.selector = gtk.OptionMenu()
        sessionMenu = sessions.getSessionMenu(self.sessionsList, self.set_parent_fields)
        self.active_menu = sessionMenu
        self.selector.set_menu(sessionMenu)

        self.selector.show()
        button_vbox.pack_start(self.selector, gtk.FALSE, gtk.FALSE)
        
        new_button = GlasnostGui.Buttons(
                '%s/pixmaps/new_sessions.png' % pixdir,'New session')
        new_button.connect('clicked', self.create_session)
 
        default_button = GlasnostGui.Buttons(
                '%s/pixmaps/default.png' % pixdir,'set as default')
        default_button.connect('clicked', self.set_default_session)
        
        save_button = gtk.Button(stock='gtk-save')
        save_button.connect('clicked', self.save_session)
        
        delete_button = gtk.Button(stock='gtk-delete')
        delete_button.connect('clicked', self.delete_session)
        
        button_vbox.pack_start(new_button, gtk.FALSE, gtk.FALSE)
        button_vbox.pack_start(default_button,gtk.FALSE, gtk.FALSE) 
        button_vbox.pack_start(save_button, gtk.FALSE, gtk.FALSE)        
        button_vbox.pack_start(delete_button, gtk.FALSE, gtk.FALSE)        
        
        login_table = gtk.Table( 3, 2, FALSE)
        login_table.set_border_width(5)
        
        self.session_entry = gtk.Entry()
        self.session_entry.set_max_length(50)
        
        self.login_entry = gtk.Entry()
        self.login_entry.set_max_length(25)
        
        self.pass_entry = gtk.Entry()
        self.pass_entry.set_visibility(0)
        self.pass_entry.set_max_length(25)
        
        self.disp_entry = gtk.Entry()
        self.disp_entry.set_text('glasnost://')
        self.disp_entry.set_max_length(50)

        login_table.attach(gtk.Label(_('Session :')), 0, 1, 0, 1)
        login_table.attach(self.session_entry, 1, 2, 0, 1)
        
        login_table.attach(gtk.Label(_('Username :')), 0, 1, 1, 2)
        login_table.attach(self.login_entry, 1, 2, 1, 2)
        
        login_table.attach(gtk.Label(_('Password :')), 0, 1, 2, 3)
        login_table.attach(self.pass_entry, 1, 2, 2, 3)

        login_table.attach(gtk.Label(_('Dispatcher :')), 0, 1, 3, 4)
        login_table.attach(self.disp_entry, 1, 2, 3, 4)
        
        #Packing
        self.MainHbox.pack_start(button_vbox, gtk.FALSE, gtk.FALSE)
        self.h_add_widget(login_table)
        self.MainVbox.pack_start(self.MainHbox, gtk.FALSE, gtk.TRUE,0)
        self.add_exit_button()
        self.ExitButton.connect('clicked', self.delete_event)
        #self.show_all()
    
    def create_session(self, widget = None):
        self.clear_fields()
        
    def save_session(self, widget = None):
        tSession  = self.session_entry.get_text()
        tLogin = self.login_entry.get_text()
        tPass = self.pass_entry.get_text()
        tDisp = self.disp_entry.get_text()
        
        if tSession is '' or tLogin is '' or tPass is '' or tDisp is '':  
            error_msg = 'There is some empty field(s)'
            dialog = gtk.MessageDialog(self,\
                gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,\
                gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,\
                error_msg)
            dialog.run()
            dialog.destroy()
        else:
            if tSession not in \
                    [x.sessionName for x in self.sessionsList]:
                newSession = sessions.Session(tSession, tLogin, tPass, tDisp)
                self.sessionsList.append(newSession)

                menuitem = gtk.MenuItem(newSession.sessionName)
                menuitem.set_data('session', newSession)
                menuitem.connect('activate',self.set_parent_fields, newSession)
                self.active_menu.add(menuitem)
                menuitem.show()
                self.clear_fields()
    
            else:
                session = [x for x in self.sessionsList \
                           if x.sessionName == tSession]
                session.updateValues(tLogin, tPass, tDisp)
            
            sessions.saveSessionsList(self.sessionsList)

    def save_on_file(self):
        sessions.saveSessionsList(self.sessionsList)
    
    def delete_session(self, widget = None):
        self.clear_fields()
        self.sessionsList.remove(self.currently_selected)
        self.save_on_file()
        menuItemList = self.active_menu.get_children()
        for item in menuItemList:
            toRemove = item.get_data('session')
            if toRemove.getThingName() \
                   == self.currently_selected.getThingName():
                self.active_menu.remove(item)
                self.active_menu.show()
                return
        
    def set_parent_fields(self, widget, session):
        self.currently_selected = session
        self.session_entry.set_text(session.sessionName)
        self.login_entry.set_text(session.userName)
        self.pass_entry.set_text(session.password)
        self.disp_entry.set_text(session.server)
    
        self._parentWindow.login_entry.set_text(session.userName)
        self._parentWindow.pass_entry.set_text(session.password)
        self._parentWindow.disp_entry.set_text(session.server)
    
    def clear_fields(self):
        self.session_entry.set_text('')
        self.login_entry.set_text('')
        self.pass_entry.set_text('')
        self.disp_entry.set_text('glasnost://')
        
        self._parentWindow.login_entry.set_text('')
        self._parentWindow.pass_entry.set_text('')
        self._parentWindow.disp_entry.set_text('')
    
    def set_default_session(self, widget = None):
        if self.currently_selected:
            for item in self.sessionsList:
                item.defaultSession = 0
            self.currently_selected.defaultSession = 1
            self.save_on_file()

