# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Login Box for Glasnost GTK+ interface"""

__version__ = '$Revision$'[11:-2]

from glasnost.proxy.DispatcherProxy import getRegisteredRoles
import GlasnostGui
from tools import *
import time
import os
import sessions

class LoginBox(gtk.Dialog):
 
    def __init__(self):
        gtk.Dialog.__init__(self)
        self.set_title(_('Identification'))
        self.set_resizable(0)
        self.set_position(gtk.WIN_POS_CENTER)
        self.default_session = None
        self.sessionsList = sessions.getSessionsList()
        
        login_table = gtk.Table( 4, 2, FALSE)
        login_table.set_border_width(5)
        
        session_table = gtk.Table(2,2, FALSE)
        session_table.set_border_width(5)
        
        #Text entries section
        self.login_entry = gtk.Entry()
        self.login_entry.set_sensitive(gtk.FALSE)
        self.login_entry.set_max_length(25)

        self.pass_entry = gtk.Entry()
        self.pass_entry.set_sensitive(gtk.FALSE)
        self.pass_entry.set_visibility(0)
        self.pass_entry.set_max_length(25)
        
        self.disp_entry = gtk.Entry()
        self.disp_entry.set_sensitive(gtk.FALSE)
        self.disp_entry.set_max_length(50)
        
        #OptionMenu       
        self.selector = gtk.OptionMenu()
        self.active_menu = sessions.getSessionMenu(self.sessionsList,
                self.set_fields)
        self.selector.set_menu(self.active_menu)
        
        #Separator MenuItem
        menuitem = gtk.MenuItem()
        menuitem.show()
        self.active_menu.add(menuitem)
        
        #Sessions Manager MenuItem
        menuitem = gtk.MenuItem(_('Sessions Manager'))
        menuitem.connect('activate',self.show_session_manager)
        self.active_menu.add(menuitem)
        
        #Manual Entry MenuItem
        menuitem = gtk.MenuItem(_('Other...'))
        menuitem.connect('activate',self.switch_sensitive)
        self.active_menu.add(menuitem)
        
        if self.active_menu.get_data('defaultCandidate'):
            session = self.active_menu.get_data('defaultCandidate')
            self.set_fields(None, session)
        
        session_table.attach(gtk.Label(_('Session')), 0, 1, 0, 1)
        session_table.attach(self.selector, 1, 2, 0, 1)        
        
        #HSeparator
        hsep = gtk.HSeparator()
        hsep.show()
        
        login_table.attach(gtk.Label(_('Username')), 0, 1, 0, 1)
        login_table.attach(self.login_entry, 1, 2, 0, 1)
        
        login_table.attach(gtk.Label(_('Password')), 0, 1, 1, 2)
        login_table.attach(self.pass_entry, 1, 2, 1, 2)

        login_table.attach(gtk.Label(_('Dispatcher')), 0, 1, 2, 3)
        login_table.attach(self.disp_entry, 1, 2, 2, 3)
        
        image = gtk.Image()
        image.set_from_file('%s/pixmaps/identity.png' % \
                                context.getVar('imagesDirectory'))
        self.vbox.pack_start(image, TRUE, TRUE, 5)

        self.vbox.pack_start(session_table, TRUE, TRUE, 5)
        self.vbox.pack_start(hsep, TRUE, TRUE, 5)
        self.vbox.pack_start(login_table, TRUE, TRUE, 5)
        
        ok_button = gtk.Button(stock='gtk-ok')
        ok_button.connect('clicked', self.delete_event, None)
        
        cancel_button = gtk.Button(stock='gtk-cancel')
        cancel_button.connect('clicked', self.abort, None)
        
        self.action_area.pack_start(cancel_button, FALSE, FALSE, 0)
        self.action_area.pack_start(ok_button, FALSE, FALSE, 0)
        self.show_all()
    
    def set_fields(self, widget, session):
        self.login_entry.set_sensitive(gtk.FALSE)
        self.pass_entry.set_sensitive(gtk.FALSE)
        self.disp_entry.set_sensitive(gtk.FALSE)

        self.login_entry.set_text(session.userName)
        self.pass_entry.set_text(session.password)
        self.disp_entry.set_text(session.server)
    
    def switch_sensitive(self, widget = None):
        self.login_entry.set_sensitive(gtk.TRUE)
        self.pass_entry.set_sensitive(gtk.TRUE)
        self.disp_entry.set_sensitive(gtk.TRUE)

    def show_session_manager(self, widget = None):
        self.hide()
        from SessionsManager import SessionsManager
        self.s_manager = SessionsManager(self)
    
    def delete_event(self, widget, data):
        tLogin = self.login_entry.get_text()
        tPass = self.pass_entry.get_text()
        tDisp = self.disp_entry.get_text()
        pixDir = context.getVar('imagesDirectory')
    
        if not tLogin or not tPass or not tDisp:
            dialog = gtk.MessageDialog(self,
                        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                        gtk.MESSAGE_WARNING, gtk.BUTTONS_OK,
                        'Some informations are missing.')
            dialog.run()
            dialog.destroy()
            return TRUE
        
        context.setVar('dispatcherId', tDisp)
        knownRoles = getRegisteredRoles(tDisp)
        context.setVar('knownRoles', knownRoles)
    
        passwordAccountsProxy = getProxyForServerRole('passwordaccounts')

        try :
            userToken = passwordAccountsProxy.checkObjectAuthentication(
                            tLogin, tPass) [0]
        except glasnost.common.faults.WrongLogin:
            dialog = gtk.MessageDialog(self,
                        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                        gtk.MESSAGE_WARNING, gtk.BUTTONS_OK,
                        'Your login is not valid.')
            dialog.run()
            dialog.destroy()
            return TRUE
        except glasnost.common.faults.WrongPassword:
            dialog = gtk.MessageDialog(self,
                        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                        gtk.MESSAGE_WARNING, gtk.BUTTONS_OK,
                        'Your login is not valid.')
            dialog.run()
            dialog.destroy()
            return TRUE

        context.setVar('userToken', userToken)
        #userId = authenticationProxy.getUserId()
        #context.setVar('userId', userId)
        self.hide()
        SSplash = glasnost.gtk.GlasnostGui.SplashScreen()
        while gtk.events_pending():
                gtk.mainiteration()        
        DaWindow = glasnost.gtk.MainWindow.MainWindow()
        DaWindow.show_all()
        self.destroy()
        SSplash.destroy()
        return FALSE
        
    def abort(self, widget, data):
        gtk.main_quit()
