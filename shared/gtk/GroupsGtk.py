# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


import glasnost.proxy.GroupsProxy as proxyGroups

import GlasnostGui
import ObjectsGtk as objects
from tools import *


COLUMN_ID = 0
COLUMN_NAME = 1


## class AdminGroups(objects.AdminMixin, groupsProxy.AdminGroups):
##     pass
## objects.register(AdminGroups)


class GroupAll(objects.ObjectGtkMixin, proxyGroups.GroupAll):
    pass
objects.register(GroupAll)


class GroupDelta(objects.ObjectGtkMixin, proxyGroups.GroupDelta):
    pass
objects.register(GroupDelta)


class GroupIntersection(objects.ObjectGtkMixin, proxyGroups.GroupIntersection):
    pass
objects.register(GroupIntersection)


class GroupRole(objects.ObjectGtkMixin, proxyGroups.GroupRole):
    pass
objects.register(GroupRole)


class GroupUnion(objects.ObjectGtkMixin, proxyGroups.GroupUnion):
    pass
objects.register(GroupUnion)


class GroupsGtk(objects.ObjectsGtkMixin, proxyGroups.GroupsProxy):
    data = None
    listColumns = (gobject.TYPE_STRING,
                   gobject.TYPE_STRING,
                   gobject.TYPE_STRING)
    
    def get_menu_param(self):
        pixDir = context.getVar('imagesDirectory')
        settings = [
                ['%s/pixmaps/group.png' % pixDir,'New',1],
                ['gtk-find', 'Search', 2],
                ]
        return settings
    
    def add_columns(self,treeview):
        model = treeview.get_model()

        # column for Object Ids
        column = gtk.TreeViewColumn(
                'Id', gtk.CellRendererText(), text = COLUMN_ID)
        treeview.append_column(column)
    
        # columns for names
        column = gtk.TreeViewColumn(
                'title', gtk.CellRendererText(), text = COLUMN_NAME)
        treeview.append_column(column)

