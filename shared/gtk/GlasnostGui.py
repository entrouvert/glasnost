# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Widgets for the glasnost-gui prototype"""

import gettext
from tools import *

import LoginBox
import SearchWindow

# --- DIRECT OBJECTS MENU --- #

class DirectMenu(gtk.VBox):
        
    def __init__(self, obj_type):
            
#        rcstring = """
#        style "grey" {
#           bg[NORMAL] = { 0.5, 0.5, 0.5 }
#        }
#        widget "*utton" style "grey"
#        """
#        gtk.rc_parse_string(rcstring)

        gtk.VBox.__init__(self, 5)
        self.set_border_width(5)
        
        obj_target = getGtkForServerRole(obj_type)
        GlobalEvent = context.getVar('gtkGlobalEvent_instance')
        
        if obj_target:
            data = obj_target.get_menu_param()
            
            if data:
                for item in data:
                    color_button = Buttons(item[0], item[1])
                    
                    #Connection for new object
                    if item[2] == 1:
                            color_button.connect('clicked',
                                    GlobalEvent.new_object, obj_target)
                    
                    #Connection for search object
                    if item[2] == 2:
                        color_button.connect('clicked', 
                                GlobalEvent.search_object, obj_target)
                    color_button.show()
                    self.pack_start(color_button, TRUE, FALSE, 0)
                    self.show_all()
            else:
                pass

# --- BUTTONS WITH ICONS --- #

def xpm_label_box(parent, xpm_filename, label_text):
    box1 = gtk.HBox(FALSE, 0)
    box1.set_border_width(2)

    style = parent.get_style()

    image = gtk.Image()
    if xpm_filename[0] == '/':
        image.set_from_file(xpm_filename)
    else:
        image.set_from_stock(xpm_filename, gtk.ICON_SIZE_LARGE_TOOLBAR)
    label = gtk.Label(label_text)

    box1.pack_start(image, FALSE, FALSE, 3)
    box1.pack_start(label, FALSE, FALSE, 3)

    image.show()
    label.show()
    return box1

class Buttons(gtk.Button):
    
    def __init__(self, xpm_path, someLabel):
        gtk.Button.__init__(self, None, None)
        pix_box = xpm_label_box(self, xpm_path, someLabel)
        pix_box.show()
        self.add(pix_box)
        self.show()

# --- STATUS BAR --- #
class StatusBar(gtk.Statusbar):

    def __init__(self):
    
        #gettext section
        #__builtins__.__dict__['_'] = gettext.gettext
        #gettext.bindtextdomain('glasnost-gui', '/usr/share/locale')
        #lang1 = gettext.translation('glasnost-gui','/usr/share/locale',['fr'])
        #lang1.install()
        
        gtk.Statusbar.__init__(self)
        #self.rubric = _('You are here -> ')
        self.rubric = 'You are here -> '
        #message = self.rubric + _('nowhere')
        message = self.rubric + 'nowhere'
        self.MsgBarContext= self.get_context_id("glasnost_Path")
        #self.write(message)
        self.set_has_resize_grip(0)
        self.show()
        
    def write(self,content):
        message = self.rubric + content
        self.pop(self.MsgBarContext)
        self.push(self.MsgBarContext,message)
         
# --- PROGRESS WINDOW --- #
class ProgressWindow(gtk.Window):
    def __init__(self, the_text, size):
        gtk.Window.__init__(self,gtk.WINDOW_POPUP)
        self.theBar = gtk.ProgressBar()
        self.theBar.set_orientation(gtk.PROGRESS_LEFT_TO_RIGHT)
        self.theBar.set_fraction(0.0)
        self._size = size
        self.text = the_text
        
        if self._size:
            self.step = 1.0/size

        if the_text:
            self.theBar.set_text('Processing objects ' + self.text)
        
        vbox = gtk.VBox(FALSE,0)
        vbox.set_border_width(5)
        vbox.show() 
       
        vbox.pack_start(self.theBar, gtk.FALSE, gtk.FALSE, 5)
        
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_resizable(FALSE)
        self.set_decorated(FALSE)
        self.add(vbox)
        self.set_resizable(0)
        self.set_border_width(1)
        #self.set_modal(TRUE)
        self.show_all()
        
    def pulse(self):
        if self._size!=0:
            while gtk.events_pending():
                gtk.mainiteration()
            self.theBar.set_fraction( self.theBar.get_fraction() + self.step)
        else:
            self.theBar.set_text(self.text)
            while gtk.events_pending():
                gtk.mainiteration()


class ServerRolesOptionMenu(gtk.OptionMenu):
    """Option Menu filled with server roles"""

    menu = None

    def __init__(self, statusBar = None, mainWindow = None, boundMethod = None):
        gtk.OptionMenu.__init__(self)
        self.sbar = statusBar
        self.mainWindow = mainWindow
        if not boundMethod:
            boundMethod = self.select_obj
        self.boundMethod = boundMethod
        self.set_menu(self.getMenu())
        
    def getMenu(self):
        if self.menu:
            return self.menu

        menu = gtk.Menu()
        menu.show()
        
        roles = []
        for role in context.getVar('knownRoles'):
            #try:
            w = getGtkForServerRole(role)
            #except:
                #continue
            if not hasattr(w, 'canGetObjects') or \
                        not hasattr(w, 'objectsNameCapitalized'):
                continue
            if not w.canGetObjects():
                continue
        
            roles.append(w)

        roles.sort(lambda x, y: locale.strcoll(_(x.objectsNameCapitalized),
                                               _(y.objectsNameCapitalized)) )
        for w in roles:
            menuitem = gtk.MenuItem(_(w.objectsNameCapitalized))
            menuitem.set_data('role', w.serverRole)
            menuitem.connect('activate', self.boundMethod, menuitem.get_data('role'))
            menu.add(menuitem)
            menuitem.show()       

        self.menu = menu
        return menu
        
    def select_obj(self,event, chosen):
        self.sbar.write(chosen)
        self.mainWindow.browse_obj(chosen)
        return FALSE
        
# --- IMAGE --- #
class Image(gtk.Image):

    def __init__(self, image_path):
        gtk.Image.__init__(self)
        self.set_from_file(image_path)
        self.show()

# --- PANED WINDOWS --- #
class HPaned(gtk.HPaned):

    def __init__(self,position,content1,content2):
        gtk.HPaned.__init__(self)
        self.set_position(position)
        self.content_load(content1,content2)
        self.show()
        
    def content_load(self,content1,content2):
        self.add1(content1)
        self.add2(content2)
        
class V_Paned(gtk.VPaned):

    def __init__(self,position,content1,content2):
        gtk.VPaned.__init__(self)
        self.set_position(position)
        self.content_load(content1,content2)
        self.show()
        
    def content_load(self,content1,content2):
        self.add1(content1)
        self.add2(content2)
        
# --- MENU BAR --- #
class MenuBar(gtk.MenuBar):
    
    def delete_event(self, widget, event, data=None):
        gtk.main_quit()
        return FALSE
    
    def __init__(self):
        gtk.MenuBar.__init__(self)
        
        pixDir = context.getVar('imagesDirectory')

        ref_image = Image('%s/pixmaps/users2.png' % pixDir)
        item_1 = gtk.ImageMenuItem(_('Change user'))
        item_1.set_image(ref_image)
        item_1.connect('activate', self.change_user, None)
        
        ref_image = gtk.Image()
        ref_image.set_from_stock('gtk-preferences', gtk.ICON_SIZE_MENU)
        item_2 = gtk.ImageMenuItem(_('Settings'))
        item_2.set_image(ref_image)
        
        ref_image = gtk.Image()
        ref_image.set_from_stock('gtk-quit', gtk.ICON_SIZE_MENU)
        item_3 = gtk.ImageMenuItem(_('Exit'))
        item_3.set_image(ref_image)
        item_3.connect('activate', self.delete_event, None)
        
        menu = gtk.Menu()
        menu.append(item_1)
        menu.append(item_2)
        menu.append(item_3)
        
        gla_menu = gtk.MenuItem("Glasnost")
        gla_menu.set_submenu(menu)
        
        item = gtk.ImageMenuItem(_('About'))
        item.connect_object('activate', self.showAboutBox, None)
        image = gtk.Image()
        image.set_from_stock('gnome-stock-about', gtk.ICON_SIZE_MENU)
        item.set_image(image)

        menu = gtk.Menu()
        menu.append(item)
        help_menu = gtk.MenuItem(_('Help'))
        help_menu.set_submenu(menu)

        self.append(gla_menu)
        self.append(help_menu)
        self.show_all()
        
    def delete_event(self, widget, event, data=None):
        gtk.main_quit()
        return FALSE
        
    def change_user(self, widget, event, data=None):
        refM = context.getVar('gtkMainWindow_instance')
        #refM.re_init()
        refM.hide()
        refL = LoginBox.LoginBox()
        refM.destroy()
        
        return TRUE
        
    def showAboutBox(self,event):
        AboutBox()


if gnome:
    class AboutBox(gnome.ui.About):
        def __init__(self):
            gnome.ui.About.__init__(self, 'Glasnost', '0.1',
                    'Copyright (c) 2001-2003 Glasnost Development Team',
                    '',
                    [ utf8('Odile B�nassy <obenassy@entrouvert.com>'),
                    utf8('Romain Chantereau <rchantereau@entrouvert.com>'),
                    utf8('Nicolas Clapi�s <nclapies@easter-eggs.org>'),
                    utf8('Pierre-Antoine Dejace <padejace@entrouvert.be>'),
                    utf8('Thierry Dulieu <tdulieu@easter-eggs.com>'),
                    utf8('Florent Monnier <monnier@codelutin.com>'),
                    utf8('C�dric Musso <cmusso@easter-eggs.org>'),
                    utf8('Fr�d�ric P�ters <fpeters@entrouvert.be>'),
                    utf8('Benjamin Poussin <poussin@codelutin.com>'),
                    utf8('Emmanuel Raviart <eraviart@entrouvert.com>'),
                    utf8('S�bastien R�gnier <regnier@codelutin.com>'),
                    utf8('Emmanuel Saracco <esaracco@easter-eggs.com>') ])
            self.show()
else:
    class AboutBox(gtk.Window):
        
        def __init__(self):
            gtk.Window.__init__(self,gtk.WINDOW_TOPLEVEL)
                    
            self.set_resizable(0)
            self.set_title('Da Slack Propaganda... shall be removed')
            self.connect('delete_event', self.delete_event)
            self.set_border_width(1)

            pixDir = context.getVar('imagesDirectory')

            separator = gtk.HSeparator()
            image = Image('%s/pixmaps/glasnost.png' % pixDir)
            label = gtk.Label("\nSlackware Ultra Powered \n      Glasnost GUI :)\n\n")
                
            vbox = gtk.VBox(FALSE, 0)
            vbox.set_border_width(5)
            vbox.show()            
                
            vbox.pack_start(image, FALSE, FALSE, 0)
            vbox.pack_start(separator, FALSE, FALSE, 0)
            vbox.pack_start(label, FALSE, FALSE, 0)
                
            self.add(vbox)
            self.show()
            
        def delete_event(self, widget, event, data=None):
            self.destroy()
            return FALSE


# --- SCAFFOLD VIEW --- #
class scaffold_view(gtk.Window):

    def delete_event(self, widget, data=None):
        self.destroy()
        return gtk.TRUE

    def __init__(self):
        gtk.Window.__init__(self,gtk.WINDOW_TOPLEVEL)
        #self.set_default_size(400,200)
        self.connect("delete_event", self.delete_event)
        self.set_resizable(1)
        self.showed = 0
        
        self.MainVbox = gtk.VBox(gtk.FALSE,0)
        self.MainVbox.set_border_width(5)
        self.MainVbox.show()
        
        self.MainHbox = gtk.HBox(gtk.TRUE,0)
        self.MainHbox.set_border_width(2)
        self.MainHbox.show()
        
        self.add(self.MainVbox)
        self.show_all()
        
    def h_add_widget(self, the_widget):
        self.MainHbox.pack_start(the_widget, gtk.TRUE, gtk.TRUE,0)
        the_widget.show()
        
    def v_add_widget(self, the_widget):
        self.MainVbox.pack_start(the_widget, gtk.FALSE, gtk.TRUE,0)
        the_widget.show()
        
    def add_exit_button(self):
        self.v_add_widget(gtk.HSeparator())
        hbox = gtk.HBox(gtk.FALSE, 0)
        self.ExitButton = gtk.Button(stock='gtk-close')
        self.ExitButton.connect('clicked', self.delete_event)
        hbox.pack_end(self.ExitButton, gtk.FALSE, gtk.FALSE)
        self.v_add_widget(hbox)
        self.show_all()

# ---  BASIC MENU --- #
class basic_menu(gtk.HandleBox):

    def __init__(self, parent_win):
        gtk.HandleBox.__init__(self)
        self.button_hbox = gtk.HBox(gtk.FALSE,2)
        self.button_hbox.set_border_width(5)
        self.button_hbox.show()
        
        #Instances
        pixDir = context.getVar('imagesDirectory')
        self.button1 = Buttons('%s/pixmaps/small_edit.png' % pixDir, 'Edit')
        self.button2 = Buttons('gtk-dialog-info', _('Infos'))
        self.button3 = Buttons('gtk-find', _('Search'))
        
        #Packing
        self.button_hbox.pack_start(self.button1, gtk.FALSE, gtk.FALSE, 0)
        self.button_hbox.pack_start(self.button2, gtk.FALSE, gtk.FALSE, 0)
        self.button_hbox.pack_start(self.button3, gtk.FALSE, gtk.FALSE, 0)
        
        self.add(self.button_hbox)
        self.show_all()
        
    def button_connect(self, number, event, method, data):
        getattr(self, 'button%d' % number).connect(event,method,data)

class edit_menu(gtk.HBox):

    def __init__(self, parent_win):
        gtk.HBox.__init__(self, gtk.FALSE,2)
        self.set_border_width(5)
        
        #Instances
        self.button1 = gtk.Button(stock='gtk-save')
        #self.button2 = gtk.Button(stock='gtk-find')
        self.button4 = gtk.Button(stock='gtk-close')
        self.button4.connect('clicked', parent_win.delete_event)
        
        #Packing
        self.pack_end(self.button4, gtk.FALSE, gtk.FALSE, 0)        
        self.pack_end(self.button1, gtk.FALSE, gtk.FALSE, 0)
        #self.pack_end(self.button2, gtk.FALSE, gtk.FALSE, 0)
        self.show_all()
        
    def button_connect(self, number, event, method, data):
        getattr(self, 'button%d' % number).connect(event,method,data)

# --- CONTENT VIEW --- #
class content_view(gtk.ScrolledWindow):

    def __init__(self, theText):
        gtk.ScrolledWindow.__init__(self)
        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_shadow_type(gtk.SHADOW_IN)
        
        self.viewer = gtk.TextView()
        self.buffer = gtk.TextBuffer(None)
        self.viewer.set_buffer(self.buffer)
        self.viewer.set_editable(FALSE)
        self.viewer.set_cursor_visible(FALSE)
        self.viewer.set_wrap_mode(TRUE)
        
        iter = self.buffer.get_iter_at_offset(0)
        tag = self.buffer.create_tag('source')
        tag.set_property('font', 'Courier 10')
        tag.set_property('pixels_above_lines', 0)
        tag.set_property('pixels_below_lines', 0)
        if theText is None:
            theText = 'nothing to publish ...'
        self.buffer.insert(iter, theText)
        
        self.add(self.viewer)
        self.show_all()
        
    def add_text(self, the_text):
        iter = self.buffer.get_iter_at_offset(0)
        self.buffer.insert(iter, the_text)
    
    def get_text_content(self):
        return iso8859_15(self.buffer.get_text(
                self.buffer.get_start_iter(),
                self.buffer.get_end_iter(), gtk.FALSE))
    
    def toggle_edit_mode(self):
        self.viewer.set_editable(TRUE)
        self.viewer.set_cursor_visible(TRUE)
    
# --- INFOFRAME --- #
class InfoFrame(gtk.Frame):
 
    def __init__(self, table_size):
        gtk.Frame.__init__(self)
        self.X = 0
        self.Y = 0
        self.side = 0
        self.infosTable = gtk.Table( table_size, 2, TRUE)
        self.infosTable.set_row_spacings(5)
        
        self.add(self.infosTable)
        self.show_all()
        
    def append(self, theWidget):
        self.infosTable.attach(theWidget,self.X, self.X+1, self.Y, self.Y+1)
        theWidget.show()

        if self.side == 0:
            self.side = 1
            self.X+=1
        else:
            self.side = 0
            self.X = 0
            self.Y+=1
    
    def set_frame_title(self,theTitle):
        self.set_label(theTitle)
    
# --- OBJECT VIEW --- #
class object_view(scaffold_view):
    
    def __init__(self,title, obj_target_id):
        scaffold_view.__init__(self)
        #self.set_size_request(600,120)
        self.set_title('Viewing : %s' % title)
        self.obj_id = obj_target_id
        self.add_basic_menu()
        self.infoframe = None
        self.hidden = 1
    
    def add_basic_menu(self):
        self.basicMenu = basic_menu(self)
        self.MainVbox.pack_start(self.basicMenu, gtk.FALSE, gtk.TRUE,0)
        
        #Buttons Connect
        self.basicMenu.button_connect(1, 'clicked', self.toggle_edit_mode, self.obj_id)
        self.basicMenu.button_connect(2, 'clicked', self.toggle_infoframe, None)
        
    def add_content_view(self, content):
        self.contentView = content_view(content)
        self.set_size_request(600,400)
        self.h_add_widget(self.contentView)
        self.MainVbox.pack_start(self.MainHbox, gtk.TRUE, gtk.TRUE, 0)

    def add_infoframe(self, frame):
        self.infoframe = frame
        self.infoframe.show_all()
        
    def toggle_infoframe(self, widget, data=None):
        if self.infoframe:
            if self.hidden:
                self.h_add_widget(self.infoframe)
                self.hidden = 0
            else:
                self.MainHbox.remove(self.infoframe)
                self.hidden = 1
    
    def toggle_edit_mode(self, widget, data=None):
        GlobalEventRef = context.getVar('gtkGlobalEvent_instance')
        self.hide()
        GlobalEventRef.edit_by_id(None,self.obj_id)
        self.destroy()
    
# --- OBJECT EDIT --- #
class object_edit(scaffold_view):
    
    def __init__(self,title, obj_target_id, obj_gtk_ref, new_object):
        scaffold_view.__init__(self)
        #self.set_size_request(600,400)
        self.set_title('Editing : %s' % title)
        
        #For edition
        self.obj_id = obj_target_id
        #For new Object
        self.obj_gtk = obj_gtk_ref
        self.new_gtk = new_object
        
        self.add_edit_menu()
        self.infoBox = gtk.VBox(gtk.FALSE,2)
        self.infoBox.show()
        self.infoPacked = 0
        self.MainVbox.pack_start(self.MainHbox, gtk.TRUE, gtk.TRUE, 0)
        self.show_all()
        
    def stock_slot_list(self, TheDico):
        self.SlotDico = TheDico
    
    def add_edit_menu(self):
        self.editMenu = edit_menu(self)
        self.MainVbox.pack_end(self.editMenu, gtk.FALSE, gtk.TRUE,0)
        
        #Buttons Connect
        #GlobalEventRef = context.getVar('gtkGlobalEvent_instance')
        self.editMenu.button_connect(1, 'clicked', self.save_object, self.obj_id)
     
    def add_content_edit(self, content):
        self.contentView = content_view(content)
        self.contentView.toggle_edit_mode()
        self.h_add_widget(self.contentView)
 
    def add_infoframe(self, frame):
        
        if self.infoPacked ==0:
            self.MainHbox.pack_start(self.infoBox, gtk.FALSE, gtk.FALSE)
            self.infoPacked=1
        self.infoBox.pack_start(frame)
        frame.show()

    def save_object(self, widget, obj_id):
        
        #Object Instances
        if obj_id :
            objectGtk = getGtk(obj_id)
            object = getObject(obj_id)
        else:
            #object = self.obj_gtk.addObject(None)
            object = self.new_gtk
 
        for item in self.SlotDico.keys():
            if item == 'body':
               setattr(object,'body',self.contentView.get_text_content())
            else:
                widget = self.SlotDico[item].getWidget()
                setattr(object,item,widget.grabWidgetValue(self.SlotDico[item]))
        try:
            if obj_id :
                objectGtk.modifyObject(object)
            else:
                self.obj_gtk.addObject(self.new_gtk)
                
            self.hide()
            self.refresh_minibrowser()
            self.destroy()
            
        except faults.Fault, fault:
            dialog = gtk.MessageDialog(self,\
                    gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,\
                    gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,\
                    fault.faultString)
            
            dialog.run()
            dialog.destroy()

    def refresh_minibrowser(self):
        refMW = context.getVar('gtkMainWindow_instance')
        if self.obj_id :
            objectGtk = getGtk(self.obj_id)
            type_ref = objectGtk.serverRole
        else:
            type_ref = self.obj_gtk.serverRole
        refMW.browse_obj(type_ref)

# --- ID SELECTOR --- #
class id_selector(scaffold_view):

    def __init__(self, slot, widget_ref,title = 'Choose an id'):
        scaffold_view.__init__(self)
        self.set_size_request(600,400)
        self.set_title(title)
        self.widget_ref = widget_ref
        
        #Objects Names
        self.roles=[]
        for role in context.getVar('knownRoles'):
            w = getGtkForServerRole(role)
            if not hasattr(w, 'canGetObjects') and \
               not hasattr(w, 'objectsNameCapitalized'):
                continue
            if not w.canGetObjects():
                continue
            self.roles.append(role)
        
        self.thelist = getGtkForServerRole(self.roles[0]).get_all_ids()
        self.roles.sort(lambda x, y: locale.strcoll(x, y))
        
        #Instances
        content_switch = gtk.Image()
        content_switch.set_from_stock('gtk-execute',gtk.ICON_SIZE_BUTTON)
        
        self.SelectorMenu = ServerRolesOptionMenu(None, None, self.switch_ids_display)
        
        self.MainFrame = gtk.Frame()
        self.ids_treeview = self.generate_display(self.thelist)
        self.MainFrame.add(self.ids_treeview)
        
        self.hbox = gtk.HBox(gtk.FALSE, 0)
        self.hbox.pack_start(self.SelectorMenu, gtk.FALSE, gtk.FALSE, 0)
        
        self.MainVbox.pack_start(self.hbox, gtk.FALSE, gtk.FALSE)
        self.MainVbox.pack_start(self.MainFrame)
        self.add_exit_button()
        self.show_all()
        
    def switch_ids_display(self, widget , data):
        child = self.MainFrame.get_children()
        self.MainFrame.remove(child[0])
        
        thelist = getGtkForServerRole(data).get_all_ids()
        scrollwin = gtk.ScrolledWindow()
        scrollwin.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        scrollwin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
                
        treeview = self.generate_display(thelist)
        
        scrollwin.add(treeview)
        self.MainFrame.add(scrollwin)
        self.MainFrame.show_all()
    
    def generate_display(self, thelist):
        COLUMN_ID = 0
        COLUMN_LABEL = 1
        
        store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        
        for item in thelist:       
            iter = store.append()
            theLabel = utf8(getObject(item).getLabel())
            store.set(iter, COLUMN_ID, item, COLUMN_LABEL, theLabel)
        
        treeview = gtk.TreeView(store)
        treeview.set_rules_hint(TRUE)
        selector = treeview.get_selection()
        
        selection = treeview.get_selection()
        selection.set_mode('single')
        
        column = gtk.TreeViewColumn('Id', gtk.CellRendererText(),text=COLUMN_ID)
        treeview.append_column(column)
        
        column = gtk.TreeViewColumn('Infos', gtk.CellRendererText(),text=COLUMN_LABEL)
        treeview.append_column(column)
        treeview.connect('row-activated',self.return_selected)
        return treeview
        
    def return_selected(self, treeview, row, column):
        model = treeview.get_model()
        iter = model.get_iter(row)
        obj_id = model.get_value(iter, 0)
        label = model.get_value(iter, 1)
        self.widget_ref.update_selected(obj_id, label)
        self.destroy()
    
# --- MULTI EDIT --- #
class multi_edit(scaffold_view):

    def __init__(self, title, slot,theMulti):
        scaffold_view.__init__(self)
        self.set_size_request(800,400)
        self.set_title(title)
        
        #Instances
        self.LocalHBox = gtk.HBox(gtk.FALSE,0)
        self.MainVbox.pack_start(self.LocalHBox)
        self.MicroVbox = gtk.VBox(gtk.FALSE,0)
        self.frameLeft = gtk.Frame()
        self.frameRight = gtk.Frame()        
        
        self._id_list = slot.getValue()
        self.rolesList = slot.getKind().getItemKind(0).serverRoles
        if not self.rolesList:
            self.rolesList = context.getVar('knownRoles')
            print self.rolesList
        self.TargetRole = 0
        self.i = self.TargetRole
        
        #self.BWidget = BWidget
        self.multiRef = theMulti
        self.right_list_content = self.multiRef.get_content()
        
        #Treeviews inserted 
        self.treeview_left = None
        self.treeview_right = None

        #Packing
        self.generate_content_view('left')
        self.generate_arrows_commands()
        self.generate_content_view('right')
        
        self.add_exit_button()        
        self.show_all()
        
    def return_list(self):
        return self.right_list_content
    
    def re_generate_right_list(self):
        widgets_list = self.treeview_right.get_selection().get_selected() 
        list_store = widgets_list[0]
        
        self.right_list_content = []
        for item in list_store:
            self.right_list_content.append(item[0])
        
    def generate_content_view(self,side):
        
        if side == 'left':
            for role in self.rolesList:
                gtkRole = getGtkForServerRole(role)
                if not gtkRole:
                    continue
                thelist = gtkRole.get_all_ids()
        else :
            if self._id_list is None :
                thelist = []
            else:
                if len(self.right_list_content)!= 0:
                    thelist = self.right_list_content
                else:
                    thelist = self._id_list
                    self.right_list_content = thelist
            
        scrollwin = gtk.ScrolledWindow()
        scrollwin.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        scrollwin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
                
        treeview = self.generate_list(thelist, side)
        scrollwin.add(treeview)
        
        if side == 'left':
            self.frameLeft.add(scrollwin)
            self.LocalHBox.pack_start(self.frameLeft, gtk.TRUE, gtk.TRUE)
        else : 
            self.frameRight.add(scrollwin)
            self.LocalHBox.pack_start(self.frameRight, gtk.TRUE, gtk.TRUE)
        
    def generate_arrows_commands(self):
        
        self.DaTip = gtk.Tooltips()
        content_left = gtk.Image()
        content_left.set_from_stock('gtk-go-back',gtk.ICON_SIZE_BUTTON)
        content_right = gtk.Image()
        content_right.set_from_stock('gtk-go-forward',gtk.ICON_SIZE_BUTTON)
        content_switch = gtk.Image()
        content_switch.set_from_stock('gtk-execute',gtk.ICON_SIZE_BUTTON)
        content_store = gtk.Image()
        content_store.set_from_stock('gtk-save', gtk.ICON_SIZE_BUTTON)
        
        button_left = gtk.Button()
        button_left.add(content_left)
        button_left.connect('clicked', self.remove_object)
        self.DaTip.set_tip(button_left, "remove id")
        
        button_right = gtk.Button()
        button_right.add(content_right)
        button_right.connect('clicked', self.add_object)
        self.DaTip.set_tip(button_right, "add id")
        
        button_switch = gtk.Button()
        button_switch.add(content_switch)
        button_switch.connect('clicked',self.switch_role_display)
        self.DaTip.set_tip(button_switch, "next object type")
                
        button_store = gtk.Button()
        button_store.add(content_store)
        button_store.connect('clicked',self.store_changes)
        self.DaTip.set_tip(button_store, "Store changes")
                        
        self.MicroVbox.pack_start(button_left, gtk.TRUE, gtk.FALSE)
        self.MicroVbox.pack_start(button_switch, gtk.TRUE, gtk.FALSE)
        self.MicroVbox.pack_start(button_store, gtk.TRUE, gtk.FALSE)
        self.MicroVbox.pack_start(button_right, gtk.TRUE, gtk.FALSE)
        self.LocalHBox.pack_start(self.MicroVbox, gtk.FALSE, gtk.FALSE)
        self.show_all()
        
    def generate_list(self, thelist, side):
        COLUMN_ID = 0
        COLUMN_LABEL = 1
        
        store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        
        if self.right_list_content:
            for item in thelist:
                if side == 'left' and item not in self.right_list_content:
                    iter = store.append()
                    theLabel = utf8(getObject(item).getLabel())
                    store.set(iter, COLUMN_ID, item, COLUMN_LABEL, theLabel)
            
                if self.right_list_content and side == 'right':
                    iter = store.append()
                    theLabel = utf8(getObject(item).getLabel())
                    store.set(iter, COLUMN_ID, item, COLUMN_LABEL, theLabel)
        else:
            for item in thelist:
                #if side == 'left' and item not in self.right_list_content:
                iter = store.append()
                theLabel = utf8(getObject(item).getLabel())
                store.set(iter, COLUMN_ID, item, COLUMN_LABEL, theLabel)
        
        
        treeview = gtk.TreeView(store)
        treeview.set_rules_hint(TRUE)
        selector = treeview.get_selection()
        
        selection = treeview.get_selection()
        selection.set_mode('single')
        
        column = gtk.TreeViewColumn('Id', gtk.CellRendererText(),text=COLUMN_ID)
        treeview.append_column(column)
        
        column = gtk.TreeViewColumn('Infos', gtk.CellRendererText(),text=COLUMN_LABEL)
        treeview.append_column(column)
        
        if side == 'left':
            self.treeview_left = treeview
        else:
            self.treeview_right = treeview
        
        return treeview
        
    def switch_role_display(self, widget=None, side='left'):
        child = self.frameLeft.get_children()
        self.frameLeft.remove(child[0])
        
        if self.i == len(self.rolesList)-1:
            self.i = 0
        else:
            self.i = self.i + 1
            
        thelist = getGtkForServerRole(self.rolesList[(self.i)]).get_all_ids()
        scrollwin = gtk.ScrolledWindow()
        scrollwin.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        scrollwin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
                
        treeview = self.generate_list(thelist, side)
        
        scrollwin.add(treeview)
        self.frameLeft.add(scrollwin)
        self.frameLeft.show_all()
     
    def store_changes(self, widget=None):
        self.multiRef.update_content(self.right_list_content)
          
    def add_object(self, widget=None):
        
        if self.treeview_left is None:
            return
        widgets_list = self.treeview_left.get_selection().get_selected()   
       
        #Grabing row values   
        iter = widgets_list[1]
        if iter is None:
            return        
        selected_obj = self.treeview_left.get_model().get_value(iter, 0)   
  
        #Removing from left 
        list_store = widgets_list[0]
        if iter is None:
            return
        list_store.remove(iter)
        
        #Adding to right
        widgets_list = self.treeview_right.get_selection().get_selected() 
        list_store = widgets_list[0]
        iter = list_store.insert(0)
        list_store.set_value(iter,0,selected_obj)
        object = getObject(selected_obj)
        list_store.set_value(iter,1,utf8(object.getLabel()))
        self.re_generate_right_list()
        
    def remove_object(self, widget=None):
        
        if self.treeview_right is None:
            return
        widgets_list = self.treeview_right.get_selection().get_selected()          

        #Grabing row values   
        iter = widgets_list[1]
        if iter is None:
            return        
        selected_obj = self.treeview_right.get_model().get_value(iter, 0)   
        
        #Removing from right    
        list_store = widgets_list[0]
        if iter is None:
            return
        list_store.remove(iter)

        #Adding to left
        widgets_list = self.treeview_left.get_selection().get_selected() 
        list_store = widgets_list[0]
        iter = list_store.insert(0)
        list_store.set_value(iter,0,selected_obj)
        object = getObject(selected_obj)
        list_store.set_value(iter,1,utf8(object.getLabel()))
        self.re_generate_right_list()
        
# --- GLOBAL EVENT --- #
class GlobalEvent:
    
    def __init__(self):
        context.setVar('gtkGlobalEvent_instance', self)
        
    def new_view_by_id(self, widget, data):
        getGtk(data).viewObject(data)
    
    def new_object(self, widget, data):
        data.EditObject(None)
        
    def edit_by_id(self, widget, data):
        getGtk(data).EditObject(data)
        #refGtk = getGtk(data)
        #refGtk.EditObject(data)
        
    def search_object(self, widget, data):
        SearchWindow.SearchWindow(data)


# --- SPLASH SCREEN --- #
class SplashScreen(gtk.Window):
    
    def __init__(self):
        gtk.Window.__init__(self,gtk.WINDOW_TOPLEVEL)
        self.set_default_size(400,400)
        pixDir = context.getVar('imagesDirectory')
        ref_image = Image('%s/pixmaps/splash.png' % pixDir)
        theBar = gtk.ProgressBar()
        
        vbox = gtk.VBox(gtk.FALSE,0)
        vbox.set_border_width(5)
        vbox.show()
        self.add(vbox)
        
        vbox.pack_start(ref_image)
        #vbox.pack_start(theBar)
        #theBar.pulse()
        
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_resizable(FALSE)
        self.set_decorated(FALSE)
        self.set_modal(TRUE)
        self.show_all()

