# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost-GTK Sessions Functions"""

__version__ = '$Revision$'[11:-2]

import cPickle as pickle
import errno
import gtk
import os

def getSessionsList():
    """Return sessions informations from ~/.glasnost_sessions.pickle"""

    try:
        return pickle.load(
                open('%s/.glasnost_sessions.pickle' % os.environ['HOME']))
    except IOError, exc:
        dialog = None
        if exc.errno == errno.ENOENT:
            dialog = gtk.MessageDialog(None,
                        gtk.DIALOG_MODAL,
                        gtk.MESSAGE_INFO, gtk.BUTTONS_OK,
                        _('No previous sessions were found') )
        elif exc.errno == errno.EACCES:
            dialog = gtk.MessageDialog(None,
                        gtk.DIALOG_MODAL,
                        gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,
                        _('Error while opening %s\n%s') % (
                                cause.filename,cause.strerror) )
        else:
            raise

    if dialog:
        dialog.run()
        dialog.destroy()                

    return []

def getSessionMenu(sessionsList, activateFunction):
    """Return a GtkMenu filled with sessions information from sessionsList"""

    menu = gtk.Menu()
    menu.show()

    defaultCandidate = None
    
    for session in sessionsList:
        if session.defaultSession:
            defaultCandidate = session
        menuitem = gtk.MenuItem(session.sessionName)
        menuitem.set_data('session', session)
        menuitem.connect('activate', activateFunction, session)
        menu.add(menuitem)
        menuitem.show()

    menu.set_data('defaultCandidate', defaultCandidate)

    return menu
        

class Session:
    def __init__(self, sessionName, userName, password, server):
        self.sessionName = sessionName
        self.userName = userName
        self.password = password
        self.server = server
        self.defaultSession = 0

    def updateValues(self, userName, password, server):
        self.userName = userName
        self.password = password
        self.server = server

def saveSessionsList(sessionsList):
    pickle.dump(sessionsList,
            open('%s/.glasnost_sessions.pickle' % os.environ['HOME'], 'w'))

