# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Gtk Widgets"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.faults as faults
import glasnost.common.context as context
import glasnost.common.xhtmlgenerator as X
import glasnost.proxy.widgets as proxyWidgets
import glasnost.gtk.GlasnostGui as GlasnostGui
import glasnost.gtk.MiniBrowser as MiniBrowser

import properties
import things
from tools import *
import time

register = things.register


class WidgetMixin(things.ThingMixin):
    def getWidgetLabel(self, slot):
        label = self.getModelLabel(slot)
        return gtk.Label(label)

    def getWidgetView(self, slot):
        value = slot.getValue()
        if type(value) is not type(''):
            value = repr(value)
        value = utf8(value)
        return gtk.Label(value)

    def getWidgetEdit(self, slot):
        return self.getWidgetView(slot)

    def grabWidgetValue(self, slot):
        return


class InputTextMixin(WidgetMixin):    
    def getWidgetLabel(self, slot):
        label = self.getModelLabel(slot)
        return gtk.Label(label)

    def getWidgetView(self, slot):
        value = slot.getValue()
        if type(value) is not type(''):
            value = repr(value)
        return gtk.Label(utf8(value))

    def getWidgetEdit(self, slot):
        self.entry = gtk.Entry()
        entry_content = slot.getValue()
        if entry_content:
            if type(entry_content) is not type(''):
                entry_content=repr(entry_content)
            self.entry.set_text(utf8(entry_content))
        return self.entry

    def grabWidgetValue(self, slot):
        return iso8859_15(self.entry.get_text())

class TimeMixin(InputTextMixin):
    def getWidgetView(self, slot):
        value = slot.getValue()
        if type(value) is type(''):
            value = repr(value)
        if value is None:
            value = ''
        else:
            value = time.gmtime(float(value))
            value = time.asctime(value)
        return gtk.Label(value)

    def getWidgetEdit(self, slot):
        self.value = slot.getValue()
        if type(self.value) is type(''):
            self.value = repr(self.value)
        if self.value is None:
            self.value = ''
        else:
            self.value = time.gmtime(float(self.value))
            self.value = time.asctime(self.value)
        return gtk.Label(self.value)

    def grabWidgetValue(self, slot):
        return slot.getValue()


class ThingMixin(WidgetMixin):
    pass


class BaseWidget(WidgetMixin, proxyWidgets.BaseWidget):
    pass
register(BaseWidget)


class Amount(WidgetMixin, proxyWidgets.Amount):
    pass
register(Amount)


class Date(TimeMixin, proxyWidgets.Date):
    pass
register(Date)


class Duration(WidgetMixin, proxyWidgets.Duration):
    pass
register(Duration)


class Email(WidgetMixin, proxyWidgets.Email):
    def getWidgetLabel(self, slot):
        label = self.getModelLabel(slot)
        return gtk.Label(utf8(label))

    def getWidgetView(self, slot):
        value = slot.getValue()
        if type(value) is not type(''):
            value = repr(value)
        return gtk.Label(utf8(value))

    def getWidgetEdit(self, slot):
        self.entry = gtk.Entry()
        entry_content = slot.getValue()
        if entry_content:
            self.entry.set_text(utf8(entry_content))
        return self.entry

    def grabWidgetValue(self, slot):
        return self.entry.get_text()
register(Email)


class InputCheckBox(WidgetMixin, proxyWidgets.InputCheckBox):
    def getWidgetView(self, slot):
        value = slot.getValue()
        cb = gtk.CheckButton()
        cb.set_active(value)
        cb.set_sensitive(0)
        return cb

    def getWidgetEdit(self, slot):
        value = slot.getValue()
        cb = gtk.CheckButton()
        cb.set_active(value)
        self.cb = cb
        return cb

    def grabWidgetValue(self, slot):
        return self.cb.get_active()
register(InputCheckBox)


class InputPassword(WidgetMixin, proxyWidgets.InputPassword):
    def getWidgetLabel(self, slot):
        label = self.getModelLabel(slot)
        return gtk.Label(utf8(label))

    def getWidgetView(self, slot):
        value = slot.getValue()
        if type(value) is not type(''):
            value = repr(value)
        return gtk.Label(utf8(value))

    def getWidgetEdit(self, slot):
        self.entry = gtk.Entry()
        self.entry.set_visibility(0)
        entry_content = slot.getValue()
        if entry_content:
            self.entry.set_text(utf8(entry_content))
        return self.entry

    def grabWidgetValue(self, slot):
        return self.entry.get_text()
register(InputPassword)


class InputText(InputTextMixin, proxyWidgets.InputText):    
    pass
register(InputText)


class Link(WidgetMixin, proxyWidgets.Link):
    pass
register(Link)


class Mapping(WidgetMixin, proxyWidgets.Mapping):
     pass
register(Mapping)


class Multi(WidgetMixin, proxyWidgets.Multi):
    def getWidgetLabel(self, slot):
        label = self.getModelLabel(slot)
        return gtk.Label(utf8(label))

    def getWidgetView(self, slot):
        value = slot.getValue()
        button = gtk.Button('Browse content')
        button.connect('clicked', self.show_multi_content,value)
        return button

    def getWidgetEdit(self, slot):
        button = gtk.Button('Add / Remove')
        self.actual_content = slot.getValue()
        #button.set_data('content_list',self.actual_content)
        button.connect('clicked', self.add_remove_content,slot)
        return button

    def grabWidgetValue(self, slot):
        return self.actual_content
        
    def show_multi_content(self,widget,value):
        if value:
            objViewer = GlasnostGui.scaffold_view()
            objViewer.set_size_request(350,100)            
            refB = MiniBrowser.MiniBrowser(value,'multi')
            objViewer.v_add_widget(refB)
            objViewer.set_title('Browse content')
            objViewer.add_exit_button()
            objViewer.show_all()
        else:
            dialog = gtk.MessageDialog(None,
                        gtk.DIALOG_MODAL,
                        gtk.MESSAGE_WARNING, gtk.BUTTONS_OK,
                        _('There is nothing to browse'))
            dialog.run()
            dialog.destroy()
            
    def add_remove_content(self,widget,value):
        self.switcher = GlasnostGui.multi_edit(
                _('Add or remove from list'), value, self)

    def update_content(self,value):
        self.actual_content = value
        
    def get_content(self):
        return self.actual_content
register(Multi)

class MultiCheck(WidgetMixin, proxyWidgets.MultiCheck):
    pass
register(MultiCheck)


class Path(WidgetMixin, proxyWidgets.Path):
    pass
register(Path)


class PushButton(WidgetMixin, proxyWidgets.PushButton):
    pass
register(PushButton)


class RadioButtons(WidgetMixin, proxyWidgets.RadioButtons):
    pass
register(RadioButtons)


class Select(WidgetMixin, proxyWidgets.Select):
    def getWidgetEdit(self, slot):
        self.content_list = slot.getKind().values
        self.theMenu = gtk.OptionMenu()
        menu = gtk.Menu()
        
        for w in self.content_list:
            menuitem = gtk.MenuItem(_(w))
            #menuitem.set_data('role', w.serverRole)
            #menuitem.connect('activate',self.select_obj, menuitem.get_data('role'))
            menu.add(menuitem)
            menuitem.show()
        
        self.theMenu.set_menu(menu)
      
        content = slot.getValue()
        if content:
            self.theMenu.set_history(self.content_list.index(content))
        return self.theMenu
    
    def grabWidgetValue(self, slot):
        return self.content_list[self.theMenu.get_history()]
register(Select)


class SelectId(WidgetMixin, proxyWidgets.SelectId):
    def getWidgetLabel(self, slot):
        label = self.getModelLabel(slot)
        return gtk.Label(label)

    def getWidgetView(self, slot):
        value = slot.getValue()
        if type(value) is not type(''):
            value = repr(value)
        ref = gtk.Button(value)    
        globalEventRef = context.getVar('gtkGlobalEvent_instance')
        if globalEventRef:
            ref.connect("clicked", globalEventRef.new_view_by_id, value)
        return ref

    def getWidgetEdit(self, slot):
        self.button = gtk.Button('Choose an ID')
        self.button.connect('clicked', self.catch_id,slot)
        return self.button

    def grabWidgetValue(self, slot):
        #return self.selected
        return 'glasnost://www.antipub.be/people1'
        
    def catch_id(self,widget,slot):
        selector = GlasnostGui.id_selector(slot, self)
        self.selected = None
        return selector
    
    def update_selected(self, id, label):
        self.selected = id
        self.button.set_label(label)
register(SelectId)


class TextArea(WidgetMixin, proxyWidgets.TextArea):
    pass
register(TextArea)


class Thing(ThingMixin, proxyWidgets.Thing):
    pass
register(Thing)


class Time(TimeMixin, proxyWidgets.Time):
    pass
register(Time)


class Token(WidgetMixin, proxyWidgets.Token):
    pass
register(Token)


class UploadFile(WidgetMixin, proxyWidgets.UploadFile):
    pass
register(UploadFile)

class Url(WidgetMixin, proxyWidgets.Url):
    pass
register(Url)


class XSelect(Select, proxyWidgets.XSelect):
    pass
register(XSelect)

