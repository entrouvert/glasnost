# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Gtk Uploads"""

__version__ = '$Revision$'[11:-2]


import cStringIO

# From Python Imaging Library.
try:
    import Image as PILImage
except ImportError:
    PILImage = None

import glasnost.common.context as context

import glasnost.proxy.uploads as proxyUploads

import things

class Upload(things.ThingMixin, proxyUploads.Upload):
    data_kindName = 'UploadFile'
    data_kind_widget_fieldLabel = N_('File')
    data_kind_widgetName = 'UploadFile'

    dataFileName_kind_stateInEditMode = 'read-only'
    #dataFileName_kind_textMaxLength = 40
    dataFileName_kind_widget_fieldLabel = N_('File Name')
    dataFileName_kind_widget_size = 40
    dataFileName_kind_widgetName = 'InputText'

    dataType_kind_stateInEditMode = 'read-only'
    dataType_kind_widget_fieldLabel = N_('Mime Type')
    dataType_kind_widgetName = 'InputText'

    height_kind_hasToRepairField = 0
    height_kind_hasToSubmitField = 0
    height_kind_min = 0
    height_kind_stateInEditMode = 'read-only'
    height_kind_textMaxLength = 4
    height_kind_widget_fieldLabel = N_('Height')
    height_kind_widget_size = 4
    height_kind_widgetName = 'InputText'

    size_kind_hasToSubmitField = 0
    size_kind_stateInEditMode = 'read-only'
    size_kind_min = 0
    size_kind_textMaxLength = 6
    size_kind_widget_fieldLabel = N_('Size')
    size_kind_widget_size = 6
    size_kind_widgetName = 'InputText'

    width_kind_hasToRepairField = 0
    width_kind_hasToSubmitField = 0
    width_kind_stateInEditMode = 'read-only'
    width_kind_min = 0
    width_kind_textMaxLength = 4
    width_kind_widget_fieldLabel = N_('Width')
    width_kind_widget_size = 4
    width_kind_widgetName = 'InputText'
things.register(Upload)

