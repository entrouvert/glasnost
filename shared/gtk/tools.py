# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

import pygtk
pygtk.require('2.0')
import gtk
from gtk import TRUE, FALSE
import gobject

try:
    import gnome
    import gnome.ui
except ImportError:
    gnome = None
    
try:
    from gtk import glade
except ImportError:
    glade = None

#import time
import cPickle as pickle
import errno
import locale
import os

import imp


import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.tools_new as commonTools

from glasnost.proxy.tools import *
from glasnost.gtk.things import *

_cachedGtksByServerRole = {}

def getGtk(id):
    return getGtkForServerRole(commonTools.extractRole(id))

def getGtkForServerRole(serverRole):
    serverRole = serverRole.replace('-', '')
    if not _cachedGtksByServerRole.has_key(serverRole):
        # Code inspired from the module knee.
        import glasnost.gtk
        gtkFileNames = os.listdir(glasnost.gtk.__path__[0])
        for gtkFileName in gtkFileNames:
            if gtkFileName.endswith('Gtk.py') \
                   and gtkFileName[:-6].lower() == serverRole:
                gtkName = gtkFileName[:-3]
                if hasattr(glasnost.gtk, gtkName):
                    module = getattr(glasnost.gtk, gtkName)
                else:
                    moduleTriplet = imp.find_module(
                            gtkName, glasnost.gtk.__path__)
                    try:
                        module = imp.load_module(
                                'glasnost.gtk.%s' % gtkName,  moduleTriplet[0],
                                moduleTriplet[1], moduleTriplet[2])
                    finally:
                        if moduleTriplet[0]:
                            moduleTriplet[0].close()
                    setattr(glasnost.gtk, gtkName, module)
                gtk = module.__dict__[gtkName]()
                break
        else:
            # TODO: ncards support
            return None
        _cachedGtksByServerRole[serverRole] = gtk
    return _cachedGtksByServerRole[serverRole]

def get_widget_child(parent_widget, insertion_level, insertion_range):
      target_child = parent_widget.get_children()
      i = 0
      while i < (insertion_level-1):
          target_child = target_child[0].get_children()
          i+=1
      
      return target_child[insertion_range]

