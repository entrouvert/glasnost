# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Glasnost Objects Gtk"""

__version__ = '$Revision$'[11:-2]

import glasnost.common.faults as faults
import glasnost.common.slots as slots
import glasnost.common.context as context
import glasnost.common.translation as translation

import kinds
import things
from tools import *

import GlasnostGui

register = things.register

class ObjectGtkMixin(things.ThingMixin):
    id_kind_stateInEditMode = 'hidden'
    id_kind_stateInViewMode = 'hidden'
    id_kind_widget_fieldLabel = N_('ID')
    id_kind_widgetName = 'InputText'

    version_kind_defaultValue = 0
    version_kind_stateInEditMode = 'hidden'
    version_kind_stateInViewMode = 'hidden'
    version_kind_widget_fieldLabel = N_('Version Number')
    version_kind_widgetName = 'InputText'

    def getListValues(self):
        return [self.id, self.getLabel()]

class GtkMixin(things.ThingMixin):
    thingCategory = 'gtk'

class ObjectsGtkMixin(GtkMixin):
    data = None
    listColumns = (gobject.TYPE_STRING, gobject.TYPE_STRING)

    def create_model(self):
        store = apply(gtk.ListStore, self.listColumns)

        for item in self.data:
            iter = store.append()
            args = [iter] + list(reduce(lambda x,y:x+y, 
                    zip(range(len(self.listColumns)), item)))
            apply(store.set, args)
        return store

    def get_menu_param(self):
        pixDir = context.getVar('imagesDirectory')
        settings = [ ['gtk-new', 'New', 1],
                     ['gtk-find', 'Search', 2],
                    ]
        return settings
    
    def get_model(self):
        #if self.data is None:
        self.data = self.generate_obj_vector()
            
        if len(self.data) == 0:
            return None
        else:
            model = self.create_model()
            return model

    def generate_obj_vector(self):
        obj_list = self.getObjectIds()
        
        vect = []
        PBar = GlasnostGui.ProgressWindow(_(self.objectsName), len(obj_list))
        
        for item in obj_list:          
            object = self.getObject(item)
            vect.append( [ ((x and utf8(x)) or '') for x in object.getListValues()] )
            PBar.pulse()
        PBar.destroy()
        return vect
    
    def get_all_ids(self):
        obj_list = self.getObjectIds()
        return obj_list
    
    def add_columns(self, treeview):
        model = treeview.get_model()

        # column for Object Ids
        column = gtk.TreeViewColumn(_('Id'), gtk.CellRendererText(), text=0)
        treeview.append_column(column)
    
        # columns for First Authors
        column = gtk.TreeViewColumn(_('Label'), gtk.CellRendererText(), text=1)
        treeview.append_column(column)

    def viewObject(self, obj_target_id):
        object = self.getObject(obj_target_id)
        list = object.getViewLayoutVisibleSlotNames()
        WinTitle = utf8(object.getLabel())

        objViewer = GlasnostGui.object_view(WinTitle, obj_target_id)
        objInfos = GlasnostGui.InfoFrame(len(list))
        
        for item in list:
            
            slot = object.getSlot(item)
            widget = slot.getWidget()
            
            objInfos.append(widget.getWidgetLabel(slot))
            objInfos.append(widget.getWidgetView(slot))
        
        objViewer.v_add_widget(objInfos)
        objInfos.infosTable.show_all()
        objViewer.add_exit_button()
    
    def EditObject(self, obj_target_id):
        # Check for new object or not
        if obj_target_id is not None:
            object = self.getObject(obj_target_id)
            WinTitle = utf8(object.getLabel())
        else:
            object  = self.newObject(None)
            WinTitle = self.objectNameCapitalized
        objViewer = GlasnostGui.object_edit(WinTitle, obj_target_id, self, object)
        
        slotList = object.getSlotNames()
        if 'body' in slotList:
            if hasattr(object,'getBody'):
                bodyContent = object.getBody()
                if bodyContent:
                    objViewer.add_content_edit(utf8(object.getBody()))
                else:objViewer.add_content_edit(utf8(_('...your content here')))
                    
            else:
                if object.body:
                    objViewer.add_content_edit(utf8(object.body))
                else:
                    objViewer.add_content_edit(utf8(_('...your content here')))
        
        #Infoframe Hidden Section
        hidden_list = object.getEditLayoutHiddenSlotNames()
            
        #Infoframe ReadOnly section
        readonly_list = object.getEditLayoutReadOnlySlotNames()
                 
        #Infoframe Editable section
        list = object.getEditLayoutSlotNames()
        self.SlotDico = {}
        if len(list)!=0:
            objInfos = GlasnostGui.InfoFrame(len(list))
            objInfos.set_frame_title('Editable Section')
            
            for item in list:
                if item in hidden_list or item in readonly_list:
                    pass
                else:
                    slot = object.getSlot(item)
                    #getSlot re-instanciate every time
                    self.SlotDico[item] = slot
                    widget = slot.getWidget()
                    sLabel = widget.getWidgetLabel(slot)                
                    if sLabel.get_text() != 'body':
                        objInfos.append(sLabel)
                        objInfos.append(widget.getWidgetEdit(slot))
                    
            objViewer.add_infoframe(objInfos)
            #Not very very clean ...
            objViewer.stock_slot_list(self.SlotDico)

