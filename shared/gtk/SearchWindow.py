# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#CONTENT : SearchWindow for the glasnost-GUI
#Dejace Pierre-Antoine 

from tools import *

import GlasnostGui

class SearchWindow(gtk.Window):

    def delete_event(self, widget, event, data=None):
        self.destroy()
        return TRUE
    
    def __init__(self, obj_type):
        gtk.Window.__init__(self,gtk.WINDOW_TOPLEVEL)
        self.set_resizable(1)
        self.set_title('Search')
        self.connect("delete_event", self.delete_event)
        
        #vbox section1
        self.vbox_sec1 = gtk.VBox(FALSE,2)
        self.vbox_sec1.set_border_width(5)
        self.vbox_sec1.show()
        
        #Main Box section
        self.MainBox = gtk.HBox(FALSE,2)
        self.MainBox.set_border_width(5)
        self.MainBox.show()
        
        self.MainBox.pack_start(self.vbox_sec1, TRUE, FALSE, 0)
        
        obj_target = getGtkForServerRole(obj_type)
        self.add(self.MainBox)
        self.generate_layout(None)
        self.show_all()
            
    def generate_layout(self, layout):
        
        pixDir = context.getVar('imagesDirectory')

        button1 = GlasnostGui.Buttons('%s/pixmaps/search.png' % pixDir, 'Search')
        button2 = GlasnostGui.Buttons('%s/pixmaps/cancel.png' % pixDir, 'Close')
        button2.connect('clicked',self.delete_event, None)
        
        #General Id search
        if layout == None:
            results = MiniBrowser.MiniBrowser(None,'browse')
            
            hbox1 = gtk.HBox(FALSE,2)
            hbox1.set_border_width(5)
            hbox1.show()
            
            hbox2 = gtk.HBox(FALSE,2)
            hbox2.set_border_width(5)
            hbox2.show()
            
            hbox3 = gtk.HBox(FALSE,2)
            hbox3.set_border_width(5)
            hbox3.show()
            
            hint_image = GlasnostGui.Image(
                    '%s/pixmaps/dialog-warning4.png' % pixDir)
            nope_label = gtk.Label(
                    _('Unknown object; switching to id search.'))
            hbox1.pack_start(hint_image, FALSE, TRUE, 0)
            hbox1.pack_start(nope_label, FALSE, TRUE, 0)
            
            separator1 = gtk.HSeparator()
            separator1.show()
            
            separator2 = gtk.HSeparator()
            separator2.show()
            
            id_label = gtk.Label("Object Id : ")
            id_entry = gtk.Entry()
            id_entry.set_max_length(25)
            
            hbox2.pack_start(id_label, FALSE, TRUE, 0)
            hbox2.pack_start(id_entry, FALSE, TRUE, 0)
            
            hbox3.pack_start(button1, FALSE, TRUE, 0)
            hbox3.pack_start(button2, FALSE, TRUE, 0)
            
            self.vbox_sec1.pack_start(hbox1, FALSE, TRUE, 0)
            self.vbox_sec1.pack_start(separator1, FALSE, TRUE, 0)
            self.vbox_sec1.pack_start(hbox2, FALSE, TRUE, 0)
            self.vbox_sec1.pack_start(separator2, FALSE, TRUE, 0)
            self.vbox_sec1.pack_start(hbox3, FALSE, FALSE, 0)

            self.MainBox.pack_start(results, FALSE, FALSE, 0)

