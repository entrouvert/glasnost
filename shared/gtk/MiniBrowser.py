# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#CONTENT : The glasnost-gui Objects Selector
#Dejace Pierre-Antoine 

from tools import *

import GlasnostGui

class MiniBrowser(gtk.Frame):
    
    #Minibrowser constructor
    def __init__(self, datas, purpose='browse'):
        gtk.Frame.__init__(self)
        self.scrollwin = gtk.ScrolledWindow()
        self.scrollwin.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        self.scrollwin.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        
        #TARGET OBJECT
        self.obj_target = None
        
        if purpose == 'browse':
            if datas:
                self.obj_target = getGtkForServerRole(datas)
                self.show_list(datas)
        if purpose == 'favorite':
            self.initial_view()
        if purpose == 'multi':
            self.multi_list(datas)
        
    def show_list(self, datas):
        
        if self.obj_target:          
            obj_model = self.obj_target.get_model()
            if obj_model == None:
                self.nothing_to_browse()
            else:
                treeview = gtk.TreeView(obj_model)
                treeview.set_rules_hint(TRUE)
                treeview.connect('row-activated', self.show_content)
            
                selection = treeview.get_selection()
                selection.set_mode('single')

                self.scrollwin.add(treeview)
                self.obj_target.add_columns(treeview)
                self.add(self.scrollwin)
                self.show_all()
        else:
            self.nothing_to_browse()
    
    def show_content(self, treeview, row, column):
        model = treeview.get_model()
        iter = model.get_iter(row)
        obj_id = model.get_value(iter, 0)
        if self.obj_target:
            obj_view = self.obj_target.viewObject(obj_id)
        else:
            self.obj_target = getGtk(obj_id)
            obj_view = self.obj_target.viewObject(obj_id)
            
    def nothing_to_browse(self):
        hbox = gtk.HBox(spacing=5)
        hbox.set_border_width(5)
        hbox.show()
        pixDir = context.getVar('imagesDirectory')
        
        oops_image = gtk.Image()
        oops_image.set_from_stock('gtk-dialog-info', gtk.ICON_SIZE_DIALOG)
        oops_label = gtk.Label('Nothing to browse for this category of documents')
        
        hbox.pack_start(oops_image, FALSE, FALSE, 0)
        hbox.pack_start(oops_label, FALSE, FALSE, 0)
        self.add(hbox)
        self.show_all()
    
    def multi_list(self, thelist):
        COLUMN_ID = 0
        COLUMN_LABEL = 1
        
        store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        for item in thelist:
            iter = store.append()
            theLabel = utf8(getObject(item).getLabel())
            store.set(iter, COLUMN_ID, item, COLUMN_LABEL, theLabel)

        treeview = gtk.TreeView(store)
        treeview.set_rules_hint(TRUE)
        treeview.connect('row-activated', self.show_content) 
        
        selection = treeview.get_selection()
        selection.set_mode('single')
        
        column = gtk.TreeViewColumn('Id', gtk.CellRendererText(),text=COLUMN_ID)
        treeview.append_column(column)
        
        column = gtk.TreeViewColumn('Infos', gtk.CellRendererText(),text=COLUMN_LABEL)
        treeview.append_column(column)
        
        self.scrollwin.add(treeview)
        self.add(self.scrollwin)
        self.show_all()
    
    def initial_view(self):
        vbox = gtk.VBox(spacing=5)
        vbox.set_border_width(5)
        vbox.show()
        
        pixDir = context.getVar('imagesDirectory')

        # 10 Last Documents
        folder_image = GlasnostGui.Image('%s/pixmaps/folder.png' % pixDir)
        info_label = gtk.Label("\n    The ten last consulted documents are:    \n\n")
        vbox.pack_start(folder_image, FALSE,TRUE, 0)
        vbox.pack_start(info_label, FALSE,TRUE, 0)
        
        # Current Votes
        folder_image = GlasnostGui.Image('%s/pixmaps/vote.png' % pixDir)
        info_label = gtk.Label("\n Active vote sessions are:    \n\n")
        vbox.pack_start(folder_image, FALSE,TRUE, 0)
        vbox.pack_start(info_label, FALSE,TRUE, 0)
        
        # System Infos
        folder_image = GlasnostGui.Image('%s/pixmaps/laptop.png' % pixDir)
        info_label = gtk.Label("\n Glasnost servers status:    \n\n")
        vbox.pack_start(folder_image, FALSE,TRUE, 0)
        vbox.pack_start(info_label, FALSE,TRUE, 0)
        
        self.add(vbox)
        self.show_all()

