# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

from glasnost.proxy.CommentsProxy import *
from ObjectsGtk import register, ObjectGtkMixin, ObjectsGtkMixin

from tools import *
import GlasnostGui

COLUMN_ID = 0
COLUMN_AUTHOR = 1
COLUMN_TITLE = 2

class Comment(ObjectGtkMixin, Comment):
    def getListValues(self):
        if self.authorId:
            author = getObject(self.authorId).getLabel()
        else:
            author = _('Anonymous')
        return [self.id, author, self.title]
        
register(Comment)

class CommentsGtk(ObjectsGtkMixin, CommentsProxy):
    data = None
    listColumns = (gobject.TYPE_STRING,gobject.TYPE_STRING,gobject.TYPE_STRING)
    
    def get_menu_param(self):
        pixDir = context.getVar('imagesDirectory')
        settings = [ ['%s/pixmaps/comment.png' % pixDir, 'New', 1],
                     ['gtk-find', 'Search', 2],
                    ]
        return settings
    
    def add_columns(self,treeview):
        model = treeview.get_model()

        # column for Object Ids
        column = gtk.TreeViewColumn('Id', gtk.CellRendererText(),text=COLUMN_ID)
        treeview.append_column(column)
    
        # columns for First Authors
        column = gtk.TreeViewColumn('Author', gtk.CellRendererText(),text=COLUMN_AUTHOR)
        treeview.append_column(column)

        # column for Title
        column = gtk.TreeViewColumn('Title', gtk.CellRendererText(),text=COLUMN_TITLE)
        treeview.append_column(column)

    def viewObject(self, obj_target_id):
        object = self.getObject(obj_target_id)
        list = object.getViewLayoutVisibleSlotNames()
        WinTitle = utf8(object.getLabel())

        objInfos = GlasnostGui.InfoFrame(len(list))
        
        for item in list:
            
            slot = object.getSlot(item)
            widget = slot.getWidget()
                        
            sLabel = widget.getWidgetLabel(slot)              
            if sLabel.get_text() != 'body':
                objInfos.append(sLabel)
                objInfos.append(widget.getWidgetView(slot))
        
        objViewer = GlasnostGui.object_view(WinTitle, obj_target_id)
        objViewer.add_content_view(utf8(object.body))
        objViewer.add_infoframe(objInfos)
        objInfos.infosTable.show_all()
        objViewer.add_exit_button()

