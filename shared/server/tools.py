# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Server Tools"""

__version__ = '$Revision$'[11:-2]


import os
import pwd
import sys

from glasnost.common.tools import *

from glasnost.proxy.tools import getObject, getProxyForServerRole


def goDaemon(nochdir = 0, noclose = 0):
    """Run server as INIT daemon"""

    if glasnost.serverUser.isdigit():
        uid = int(glasnost.serverUser)
    else:
        try:
            uid = pwd.getpwnam(glasnost.serverUser)[2]
        except KeyError:
            uid = os.getuid()

    if uid != os.getuid():
        try:
            os.setuid(uid)
        except OSError:
            print 'WARNING: Failed to setuid() to %s' % uid
    
    if os.fork():
        os._exit(0)
    os.setsid()
    if nochdir == 0:
        os.chdir('/')
    if noclose == 0:
        fp = open('/dev/null', 'rw')
        sys.stdin = sys.__stdin__ = fp
        sys.stdout = sys.__stdout__ = fp
        sys.stderr = sys.__stderr__ = fp
        del fp
    if os.fork():
        os._exit(0)

