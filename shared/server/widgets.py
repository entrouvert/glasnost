# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Server Widgets"""

__version__ = '$Revision$'[11:-2]


import types

import glasnost.common.widgets as commonWidgets

import properties
import things


register = things.register


class WidgetMixin(things.ThingMixin):
    pass


class InputTextMixin(WidgetMixin):
    pass


class ThingMixin(WidgetMixin):
    pass


class TimeMixin(InputTextMixin):
    pass


class BaseWidget(WidgetMixin, commonWidgets.BaseWidget):
    pass
register(BaseWidget)


class Duration(WidgetMixin, commonWidgets.Duration):
    pass
register(Duration)


class Email(WidgetMixin, commonWidgets.Email):
    pass
register(Email)


class InputCheckBox(WidgetMixin, commonWidgets.InputCheckBox):
    pass
register(InputCheckBox)


class InputPassword(WidgetMixin, commonWidgets.InputPassword):
    pass
register(InputPassword)


class InputText(InputTextMixin, commonWidgets.InputText):
    pass
register(InputText)


class Link(WidgetMixin, commonWidgets.Link):
    pass
register(Link)


class Mapping(WidgetMixin, commonWidgets.Mapping):
    pass
register(Mapping)


class Multi(WidgetMixin, commonWidgets.Multi):
    pass
register(Multi)


class Path(WidgetMixin, commonWidgets.Path):
    pass
register(Path)


class PushButton(WidgetMixin, commonWidgets.PushButton):
    pass
register(PushButton)


class RadioButtons(WidgetMixin, commonWidgets.RadioButtons):
    pass
register(RadioButtons)


class Select(WidgetMixin, commonWidgets.Select):
    pass
register(Select)


class SelectId(WidgetMixin, commonWidgets.SelectId):
    pass
register(SelectId)


class TextArea(WidgetMixin, commonWidgets.TextArea):
    pass
register(TextArea)


class Thing(ThingMixin, commonWidgets.Thing):
    pass
register(Thing)


class Time(TimeMixin, commonWidgets.Time):
    pass
register(Time)


class Token(WidgetMixin, commonWidgets.Token):
    pass
register(Token)

        
class UploadFile(WidgetMixin, commonWidgets.UploadFile):
    pass
register(UploadFile)

class Url(WidgetMixin, commonWidgets.Url):
    pass
register(Url)


class XSelect(WidgetMixin, commonWidgets.XSelect):
    pass
register(XSelect)
