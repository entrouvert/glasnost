# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Web Properties"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.xhtmlgenerator as X

import glasnost.proxy.properties as proxyProperties

import things


class Property(things.ThingMixin, proxyProperties.Property):
    def getCompactEditLayout(self, fields, parentSlot = None):
        if parentSlot is None:
            level  = 'getCompactEditLayout'
        else:
            fieldName = parentSlot.getFieldName()
            level  = 'getCompactEditLayout %s' % fieldName
        context.push(_level = level,
                     layoutMode = 'edit',
                     useCompactLayout = 1)
        try:
            layout = X.array()
            slotNames = self.getEditLayoutSlotNames(
                fields, parentSlot = parentSlot)
            hiddenSlotNames = self.getEditLayoutHiddenSlotNames(
                fields, parentSlot = parentSlot)
            readOnlySlotNames = self.getEditLayoutReadOnlySlotNames(
                fields, parentSlot = parentSlot)
            for slotName in slotNames:
                slot = self.getSlot(slotName, parentSlot = parentSlot)
                kind = slot.getKind()
                widget = kind.getModelWidget(slot)
                if slotName in hiddenSlotNames:
                    if kind.isRequiredInEditMode or kind.isExportable():
                        hidden = widget.getModelHiddenLayout(slot, fields)
                        layout += hidden
                else:
                    context.push(inForm = 0,
                                 readOnly = 0)
                    try:
                        if kind.isRequiredInEditMode or kind.isExportable():
                            context.setVar('inForm', 1)
                            context.setVar('readOnly',
                                           slotName in readOnlySlotNames)
                        layout += widget.getHtmlCompactValue(slot, fields)
                    finally:
                        context.pull()
            return layout
        finally:
            context.pull(_level = level)

    def getCompactViewLayout(self, fields, parentSlot = None):
        if parentSlot is None:
            level  = 'getCompactViewLayout'
        else:
            fieldName = parentSlot.getFieldName()
            level  = 'getCompactViewLayout %s' % fieldName
        context.push(_level = level,
                     layoutMode = 'view',
                     useCompactLayout = 1)
        try:
            layout = X.array()
            slotNames = self.getViewLayoutSlotNames(
                    fields, parentSlot = parentSlot)
            hiddenSlotNames = self.getViewLayoutHiddenSlotNames(
                fields, parentSlot = parentSlot)
            context.push(inForm = 0,
                         readOnly = 0)
            try:
                for slotName in slotNames:
                    slot = self.getSlot(slotName, parentSlot = parentSlot)
                    kind = slot.getKind()
                    if slotName in hiddenSlotNames:
                        continue
                    widget = kind.getModelWidget(slot)
                    layout += widget.getHtmlCompactValue(slot, fields)
            finally:
                context.pull()
            return layout
        finally:
            context.pull(_level = level)
things.register(Property)

