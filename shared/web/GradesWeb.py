# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Grades Web"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.faults as faults

from glasnost.proxy.GradesProxy import *
from glasnost.proxy.GroupsProxy import getSetContainedIds

from ObjectsWeb import register, AdminMixin, ObjectWebMixin, ObjectsWebMixin
from tools import *
import widgets


class AdminGrades(AdminMixin, AdminGrades):
    pass
register(AdminGrades)


class MarksWidget(widgets.BaseWidget):
    def getHtmlFieldValue(self, slot, fields, **keywords):
        container = slot.getContainer()
        idsGetterName = slot.getKind().idsGetterName
        idsGetter = getattr(container, idsGetterName)
        objectIds = idsGetter(fields)
        tdAttributes = {}
        if self.colSpan:
            tdAttributes['colspan'] = self.colSpan
        layout = X.td(_class = 'field-value', **tdAttributes)
        if self.isReadOnly(slot):
            layout += self.getModelHiddenLayout(slot, fields)
        if objectIds:
            table = X.table()
            layout += table
            for objectId in objectIds:
                fieldName = slot.getFieldOptionName(objectId)
                mark = slot.getFieldOption(fields, objectId, default = '')
                tr = X.tr()
                table += tr
                tr += X.td(_class = 'field-label')(
                    X.objectHypertextLabel(objectId),
                    X.asIs(_(':')),
                    )
                if not self.isInForm() or self.isReadOnly(slot):
                    tr += X.td(_class = 'field-value')(
                        mark,
                        X.nbsp,
                        '%',
                        )
                else:
                    td = X.td(_class = 'field-value')
                    tr += td
                    error = slot.getObject().getError(slot.getPath())
                    if error and not context.getVar('hideErrors'):
                        td += X.span(_class = 'error-message')(
                                _(error.uiFaultString))
                    td += X.input(maxlength = 6, name = fieldName, size = 6,
                                  type = 'text', value = mark)
                    td += X.nbsp
                    td += '%'
        return X.div(_class = 'cell')(layout)

    def getModelHiddenLayout(self, slot, fields):
        container = slot.getContainer()
        idsGetterName = slot.getKind().idsGetterName
        idsGetter = getattr(container, idsGetterName)
        objectIds = idsGetter(fields)
        layout = X.array()
        if objectIds:
            for objectId in objectIds:
                fieldName = slot.getFieldOptionName(objectId)
                mark = slot.getFieldOption(fields, objectId, default = '')
                layout += X.input(name = fieldName, type = 'hidden',
                                  value = mark),
        return layout
widgets.register(MarksWidget)


class Grade(ObjectWebMixin, Grade):
    marks_kind_idsGetterName = 'getMarksIds'
    marks_kindName = 'DistributionMarks'
    marks_kind_widget_fieldLabel = N_('Choice')
    marks_kind_widgetName = 'MarksWidget'

    membersSet_kind_itemKind_value_widgetName = 'SelectId'
    membersSet_kind_widget_apply = 1
    membersSet_kind_widget_fieldLabel = N_('Members')
    membersSet_kind_widgetName = 'Multi'

    name_kind_widget_fieldLabel = N_('Name')
    name_kind_widget_size = 40
    name_kind_widgetName = 'InputText'

    def getMarksIds(self, fields):
        slotName = 'membersSet'
        slot = self.getSlot(slotName)
        kind = slot.getKind()
        count = 0
        try:
            count = int(slot.getFieldOption(fields, 'count', default = '0'))
        except ValueError:
            pass
        value = []
        for i in range(count):
            itemSlot = kind.getItemSlot(slot, i)
            itemValue = itemSlot.getValue() or ''
            if itemValue:
                value.append(itemValue)
        if value == []:
            value = None
        return value
register(Grade)


class GradesWeb(ObjectsWebMixin, GradesProxy):
    pass
