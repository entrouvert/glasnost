# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Token Accounts Web"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.ObjectsCommon as commonObjects
import glasnost.common.tools_new as commonTools
import glasnost.common.xhtmlgenerator as X

import glasnost.proxy.TokenAccountsProxy as proxyTokenAccounts

import ObjectsWeb as objects
from tools import accessForbidden, getWebForServerRole, writePageLayout


class AdminTokenAccounts(objects.AdminMixin,
                            proxyTokenAccounts.AdminTokenAccounts):
    pass
objects.register(AdminTokenAccounts)


class Login(objects.ObjectWebMixin, commonObjects.ObjectCommon):
    id_kindName = None

    language_kindName = None

    token = None
    class token_kindClass:
        _kindName = 'Token'
        balloonHelp = N_('Enter your secret token.')
        isRequired = 1
        label = N_('Token')
        widget_size = 20


class TokenAccount(objects.ObjectWebMixin,
                      proxyTokenAccounts.TokenAccount):
    def getEditLayoutSlotNames(self, fields, parentSlot = None):
        slotNames = objects.ObjectWebMixin.getEditLayoutSlotNames(self,
                fields, parentSlot = parentSlot)
        if 'token' in slotNames:
            slotNames.remove('token')
        return slotNames
objects.register(TokenAccount)


class TokenAccountsWeb(objects.ObjectsWebMixin,
                          proxyTokenAccounts.TokenAccountsProxy):
    def login(self):
        object = Login()
        return self.loginObject(object)
    login.isPublicForWeb = 1

    def loginObject(self, object):
        req = context.getVar('req')
        req.headers_out['Cache-Control'] = 'no-cache, must-revalidate'

        context.push(_level = 'loginObject', layoutMode = 'edit')
        try:
            layout = X.array()

            if context.getVar('error'):
                layout += object.getErrorLayout()

            # The instruction submitUrl = X.actionUrl('loginSubmit')
            # doesn't work because the login method can be called from
            # IdentitiesWeb.
            submitUrl = X.roleUrl(self.serverRole, action = 'loginSubmit')
            if context.getVar('virtualHost').useHTTPS:
                hostNameAndPort = commonTools.makeHttpHostNameAndPort(
                        context.getVar('httpHostName'),
                        context.getVar('httpPort'))
                submitUrl = 'https://%s%s' % (hostNameAndPort, submitUrl)

            form = X.form(action = submitUrl, enctype = 'multipart/form-data',
                          method = 'post')
            layout += form

            if context.getVar('nextUri'):
                form += X.div(X.input(name = 'nextUri', type = 'hidden',
                                      value = context.getVar('nextUri')))

            form += object.getEditLayout(fields = None)

            buttonsBar = X.div(_class = 'buttons-bar')
            form += buttonsBar
            buttonsBar += X.buttonInForm('login', 'loginButton')
            return writePageLayout(layout, _('Login'))
        finally:
            context.pull(_level = 'loginObject')

    def loginSubmit(self, **keywords):
        if keywords is None:
            keywords = {}

        object = Login()
        object.submitFields(keywords)

        if context.getVar('again'):
            return self.loginObject(object)

        try:
            userToken, authenticationMethod = self.checkObjectAuthentication(
                    object.token)
        except faults.WrongToken, fault:
            context.setVar('error', 1)
            object.setError('self.token', fault)
            return self.loginObject(object)
        except:
            if context.getVar('debug'):
                raise
            return accessForbidden()
        identitiesWeb = getWebForServerRole('identities')
        return identitiesWeb.loginSucceeded(userToken, authenticationMethod)
    loginSubmit.isPublicForWeb = 1

