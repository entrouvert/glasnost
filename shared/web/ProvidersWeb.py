# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Providers Web"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.tools_new as commonTools

import glasnost.proxy.ProvidersProxy as proxyProviders

import ObjectsWeb as objects


class AdminProviders(objects.AdminMixin, proxyProviders.AdminProviders):
    pass
objects.register(AdminProviders)


class ProviderMixin(objects.ObjectWebMixin):
    className = None
    class className_kindClass:
        _kindName = 'Choice'
        defaultValue = 'IdentityProvider'
        importExport = 'private'
        isRequired = 1
        isRequiredInEditMode = 1
        label = N_('Type')
        labels = {
                'IdentityProvider': N_('Identity Provider'),
                'ServiceProvider': N_('Service Provider'),
                }
        values = [
                'IdentityProvider',
                'ServiceProvider',
                ]
        widget_apply = 1


class IdentityProvider(ProviderMixin, proxyProviders.IdentityProvider):
    pass
objects.register(IdentityProvider)


class ServiceProvider(ProviderMixin, proxyProviders.ServiceProvider):
    pass
objects.register(ServiceProvider)


class ProvidersWeb(objects.ObjectsWebMixin, proxyProviders.ProvidersProxy):
    def getObject_handleResult(self, lazyObject):
        object = proxyProviders.ProvidersProxy.getObject_handleResult(
                self, lazyObject)
        object.className = object.__class__.__name__
        return object

    def newObject(self, fields = None):
        provider = IdentityProvider() # Fake provider to get className.
        if not fields:
            provider.className = 'IdentityProvider'
            return provider
        classNameSlot = provider.getSlot('className')
        className = classNameSlot.getWidget().submit(classNameSlot, fields)
        if not className:
            className = 'IdentityProvider'
        provider = commonTools.newThing('object', 'providers.%s' % className)
        provider.className = className
        return provider

