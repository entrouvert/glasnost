# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost System Files Web"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.faults as faults

from glasnost.proxy.SystemFilesProxy import *

from ObjectsWeb import register, AdminMixin, ObjectWebMixin, ObjectsWebMixin
from tools import *


class AdminSystemFiles(AdminMixin, AdminSystemFiles):
    pass
register(AdminSystemFiles)
        
          
class SystemFile(ObjectWebMixin, SystemFile):
    data_kind_hasToSubmitField = 0 
    data_kind_textFormat = 'text'
    data_kind_widget_fieldLabel = N_('Content')
    data_kind_widget_cols = 75
    data_kind_widget_colSpan = 2
    data_kind_widget_rows = 40
    data_kind_widgetName = 'TextArea'

    filePath_kind_widget_fieldLabel = N_('Path')
    filePath_kind_widget_size = 40
    filePath_kind_widgetName = 'InputText'

    size_kind_hasToSubmitField = 0
    size_kind_min = 0
    size_kind_textMaxLength = 6
    size_kind_widget_fieldLabel = N_('Size')
    size_kind_widget_size = 6
    size_kind_widgetName = 'InputText'

    title_kind_widget_fieldLabel = N_('Title')
    title_kind_widget_size = 40
    title_kind_widgetName = 'InputText'

    def getEditLayoutSlotNames(self, fields, parentSlot = None):
        slotNames = ObjectWebMixin.getEditLayoutSlotNames(
            self, fields, parentSlot = parentSlot)
        slotNames = slotNames[:]
        for slotName in ['data', 'size']:
            if slotName in slotNames:
                slotNames.remove(slotName)
        return slotNames

    def getViewLayoutSlotNames(self, fields, parentSlot = None):
        slotNames = ObjectWebMixin.getViewLayoutSlotNames(
            self, fields, parentSlot = parentSlot)
        slotNames = slotNames[:]
        userToken = context.getVar('userToken', default = '')
        if not userToken or context.getVar('useCompactLayout', default = 0):
            for slotName in ('creationTime', 'modificationTime', 'size',
                             'readersSet', 'writersSet'):
                if slotName in slotNames:
                    slotNames.remove(slotName)
        return slotNames
register(SystemFile)


class SystemFilesWeb(ObjectsWebMixin, SystemFilesProxy):
    def viewAll(self):
        context.push(_level = 'viewAll',
                     defaultDispatcherId = context.getVar('dispatcherId'))
        try:
            if not self.canGetObjects():
                return accessForbidden()
            isAdmin = self.isAdmin()
            userId = context.getVar('userId', default = '')
            if userId:
                userSet = [userId]
            else:
                userSet = None

            layout = X.array()
            requiredSlotNames = ['filePath', 'title']
            displayedSlotNames = ['filePath']

            if userSet:
                lastSystemFiles = self.getLastObjects(
                    10, None, userSet, requiredSlotNames)
                layout += self.getObjectsSectionLayout(
                    lastSystemFiles,
                    _("""Your last system files"""),
                    displayedSlotNames)

            lastSystemFiles = self.getLastObjects(
                10, userSet, None, requiredSlotNames)
            layout += self.getObjectsSectionLayout(
                lastSystemFiles,
                _("""The last system files"""),
                displayedSlotNames)

            if isAdmin:
                lastSystemFiles = self.getLastObjects(
                    50, None, None, requiredSlotNames)
                layout += self.getObjectsSectionLayout(
                    lastSystemFiles,
                    _("""The last system files to administer"""),
                    displayedSlotNames)

            layout += self.getViewAllButtonsBarLayout()
        finally:
            context.pull(_level = 'viewAll')
        return writePageLayout(layout, _('System Files'))
    viewAll.isPublicForWeb = 1
