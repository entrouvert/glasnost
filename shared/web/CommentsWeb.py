# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Comments Web"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.tools_new as commonTools

from glasnost.proxy.CommentsProxy import *

from ObjectsWeb import register, AdminMixin, ObjectWebMixin, ObjectsWebMixin
from tools import *


class AdminComments(AdminMixin, AdminComments):
    pass
register(AdminComments)


class Comment(ObjectWebMixin, Comment):
    body_kind_widget_fieldLabel = N_('Text')
    body_kind_widget_cols = 75
    body_kind_widget_colSpan = 2
    body_kind_widget_preview = 1
    body_kind_widget_rows = 20
    body_kind_widgetName = 'TextArea'

    def getEditLayoutSlotNames(self, fields, parentSlot = None):
        slotNames = ObjectWebMixin.getEditLayoutSlotNames(self, fields,
                parentSlot = parentSlot) [:]
        if 'authorId' in slotNames:
            slotNames.remove('authorId')
        if 'parentId' in slotNames:
            slotNames.remove('parentId')
        if 'creationTime' in slotNames:
            slotNames.remove('creationTime')
        if 'isEditorial' in slotNames:
            if not self.getWeb().canPostEditorialComment(self.parentId):
                slotNames.remove('isEditorial')
            pass

        return slotNames

    def getViewLayoutSlotNames(self, fields, parentSlot = None):
        return ['authorId', 'creationTime', 'body']

    def getViewLayout(self, fields, parentSlot = None):
        sectionLevel = context.getVar('sectionLevel')
        context.push(sectionLevel = sectionLevel+1)

        layout = X.array()
        #titleSlot = self.getSlot('title')
        #titleHtml = titleSlot.getWidget().getHtmlValue(titleSlot, fields)
        #titleTag = getattr(X, 'h%s' % (sectionLevel+1))
        #if len(titleHtml.children) == 2:
        #    # remove translation bar
        #    del titleHtml.children[1]
        #layout += titleTag(titleHtml)

        authorIdSlot = self.getSlot('authorId')
        if authorIdSlot.getValue():
            layout += X.span(_class = 'authorId')(
                authorIdSlot.getWidget().getHtmlValue(authorIdSlot, fields))
        else:
            layout += X.span(_class = 'authorId')(_('Anonymous'))
        creationTimeSlot = self.getSlot('creationTime')
        layout += X.span(_class = 'creationTime')(
                creationTimeSlot.getWidget().getHtmlValue(creationTimeSlot, fields))
        bodySlot = self.getSlot('body')
        layout += X.div(bodySlot.getWidget().getHtmlValue(bodySlot, fields))
        context.pull()

        return layout
register(Comment)


class CommentsWeb(ObjectsWebMixin, CommentsProxy):
    def clone(self): pass
    def confirmDelete(self): pass
    def delete(self): pass
    def download(self): pass
    def edit(self): pass
    def id(self): pass
    def image(self): pass
    def imageEdit(self): pass
    def rss(self): pass
    def search(self): pass
    def submit(self): pass
    def thumbnail(self): pass
    def use(self): pass
    def view(self): pass

    def getCommentsLayout(self, parentId, isEditorial = 0):
        comments = self.getObjectsWithParent(parentId, isEditorial).values()
        comments.sort(lambda x,y: cmp(x.creationTime, y.creationTime))
        layout = X.array()
        if len(comments) == 0:
            #layout += X.p(_('No comment.'))
            return None
        for i, comment in zip(range(len(comments)), comments):
            layout += X.div(_class = 'comment', id = 'comment-%d' % (i+i))(
                    X.span(_class = 'comment-no')(str(i+1)),
                    comment.getViewLayout(self, None))
        return layout

    def getBothCommentsLayout(self, parentId):
        layout = X.array()

        if self.canPostEditorialComment(parentId):
            context.push(sectionLevel = context.getVar('sectionLevel') + 1)
            comments = self.getCommentsLayout(parentId, isEditorial = 1)
            context.pull()
            if comments is not None:
                layout += X.hContext()(
                        id = 'editorial-comments')(_('Editorial Comments'))
                layout += comments

        context.push(sectionLevel = context.getVar('sectionLevel') + 1)
        comments = self.getCommentsLayout(parentId)
        context.pull()
        if comments is not None:
            layout += X.hContext()(id = 'comments')(_('Comments'))
            layout += comments

        if self.canAddObject():
            layout += X.div(_class = 'buttons-bar')(
                    X.buttonStandalone(_('Add Comment'),
                            X.idUrl(parentId, 'addComment')))

        return layout


    def viewAll(self):
        layout = X.array()
        userToken = context.getVar('userToken')
        if self.canModifyAdmin() and userToken:
            layout += X.div(_class = 'buttons-bar')(
                    X.buttonStandalone('settings', X.actionUrl('admin')))
        return writePageLayout(layout,
                self.getTranslatedObjectsNameCapitalized())
    viewAll.isPublicForWeb = 1

