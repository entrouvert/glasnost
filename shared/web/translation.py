# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Web Translation Utilities Functions"""

__version__ = '$Revision$'[11:-2]

import md5

import glasnost.common.context as context
import glasnost.common.faults as faults
from glasnost.common.translation import *
import glasnost.common.xhtmlgenerator as X

import kinds

from tools import *

def getFieldValueAndTranslationBar(slot, noBar = 0):
    value = slot.getValue()

    if type(value) is not types.StringType:
        value = slot.getKind().convertValueToOtherType(value, types.StringType)

    if not value:
        return value, None

    if not slot.getKind().isTranslatable:
        return value, None

    translationsProxy = getProxyForServerRole('translations')
    if not translationsProxy:
        return value, None

    object = slot.getObject()
    sourceId = object.getId()
    if not sourceId:
        return value, None
    
    sourceLanguage = object.getLanguage()
    if not sourceLanguage:
        return value, None

    sourceStringDigest = md5.new(value.replace('\r\n', '\n')).hexdigest()

    fieldName = slot.getFieldName()
    try:
        value, destinationLanguage, state = \
                    translationsProxy.getTranslationInfos(
                                value, sourceId, slot.getPath(),
                                sourceLanguage,
                                context.getVar('readLanguages'))
    except faults.NonExistentSlotPath:
        destinationLanguage = sourceLanguage
        state = 'ignored'
    except faults.UserAccessDenied:
        # This exception happens for example, when viewing in english a comment
        # of a vote written if french. If the translation doesn't exist, the
        # TranslationsServer calls the method getObjectStringFromDigest of the
        # VotesServer. This raises the exception, because only the ballots
        # server can call the VotesServer.
        destinationLanguage = sourceLanguage
        state = 'ignored'
    language = context.getVar('readLanguages')[0]

    translationBar = None
    if noBar:
        return value, translationBar

    if state in ['fuzzy', 'untranslated']:
        buttonKey = 'translate'
    elif state in ['translated', 'untranslatable']:
        buttonKey = 'change-translation'
    elif state == 'obsolete':
        buttonKey = 'update-translation'
    else:
        buttonKey = None

    if buttonKey and translationsProxy.canModifyLocalization(
                            sourceLanguage + language, sourceStringDigest):
        translationBar = X.div(_class = 'buttonBar')(X.buttonStandalone(
                buttonKey,
                X.roleUrl('translations', 'edit').add(
                        'localizationKey', sourceLanguage + language).add(
                        'sourceStringDigest', sourceStringDigest).add(
                        'nextUri', cleanUpUnparsedUri([]))))
    elif state == 'original':
        possibleLanguages = translationsProxy.getPossibleLanguages()
        buttons = []
        for language in possibleLanguages:
            if language == sourceLanguage:
                continue
            if not translationsProxy.canModifyLocalization(
                    sourceLanguage + language, sourceStringDigest):
                continue
        
            state = translationsProxy.getTranslationInfos(
                                value, sourceId, slot.getPath(),
                                sourceLanguage,
                                [language]) [2]
            buttonLabel = _('Translation to %s') % _(languageLabels[language])
            emoticons = { 'fuzzy': '(?)',
                          'untranslated': '(-)',
                          'translated': '(V)',
                          'untranslatable': '(v)',
                          'obsolete': '(~)' }
            if emoticons.has_key(state):
                buttonLabel += ' %s' % emoticons[state]

            buttons.append( X.buttonStandalone(
                    buttonLabel,
                    X.roleUrl('translations',
                        'edit/%s/%s' % ( sourceLanguage + language,
                                sourceStringDigest)).add(
                            'nextUri', cleanUpUnparsedUri([]))))
        if buttons:
            translationBar = X.div(_class = 'buttonBar')
            for b in buttons:
                translationBar += b
        
    return value, translationBar

