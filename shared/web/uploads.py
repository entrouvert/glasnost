# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Web Uploads"""

__version__ = '$Revision$'[11:-2]


import cStringIO

# From Python Imaging Library.
try:
    import Image as PILImage
except ImportError:
    PILImage = None

import glasnost.common.context as context

import glasnost.proxy.uploads as proxyUploads

import things
from tools import isTypeOfMimeType


class Upload(things.ThingMixin, proxyUploads.Upload):
    data_kind_tagInEditMode = 'div'
    data_kind_tagInViewMode = 'div'
    data_kind_widget_fieldLabel = N_('File')
    data_kind_widgetName = 'UploadFile'
    data_kindName = 'UploadFile'

    dataFileName_kind_stateInEditMode = 'read-only'
    dataFileName_kind_widget_fieldLabel = N_('File Name')
    dataFileName_kind_widget_size = 40
    dataFileName_kind_widgetName = 'InputText'

    dataType_kind_stateInEditMode = 'read-only'
    dataType_kind_widget_fieldLabel = N_('Mime Type')
    dataType_kind_widgetName = 'InputText'

    height_kind_hasToRepairField = 0
    height_kind_hasToSubmitField = 0
    height_kind_stateInEditMode = 'read-only'
    height_kind_min = 0
    height_kind_textMaxLength = 4
    height_kind_widget_fieldLabel = N_('Height')
    height_kind_widget_size = 4
    height_kind_widgetName = 'InputText'

    size_kind_hasToSubmitField = 0
    size_kind_stateInEditMode = 'read-only'
    size_kind_min = 0
    size_kind_textMaxLength = 6
    size_kind_widget_fieldLabel = N_('Size')
    size_kind_widget_size = 6
    size_kind_widgetName = 'InputText'

    width_kind_hasToRepairField = 0
    width_kind_hasToSubmitField = 0
    width_kind_stateInEditMode = 'read-only'
    width_kind_min = 0
    width_kind_textMaxLength = 4
    width_kind_widget_fieldLabel = N_('Width')
    width_kind_widget_size = 4
    width_kind_widgetName = 'InputText'

    def getEditLayout(self, fields, parentSlot = None):
        dataSlot = self.getSlot('data', parentSlot = parentSlot)
        data = dataSlot.getValue()
        sizeSlot = self.getSlot('size', parentSlot = parentSlot)
        if data and not sizeSlot.getValue():
            sizeSlot.setField(fields, str(len(data)))
            dataTypeSlot = self.getSlot('dataType', parentSlot = parentSlot)
            if isTypeOfMimeType(dataTypeSlot.getValue() or '',
                                'image') and PILImage:
                imageFile = cStringIO.StringIO(data)
                try:
                    imageObject = PILImage.open(imageFile)
                except IOError:
                    # Cannot identify image file.
                    pass
                else:
                    width, height = imageObject.size
                    widthSlot = self.getSlot('width', parentSlot = parentSlot)
                    widthSlot.setField(fields, str(width))
                    heightSlot = self.getSlot(
                            'height', parentSlot = parentSlot)
                    heightSlot.setField(fields, str(height))
        return things.ThingMixin.getEditLayout(
            self, fields, parentSlot = parentSlot)

    def getEditLayoutSlotNames(self, fields, parentSlot = None):
        slotNames = things.ThingMixin.getViewLayoutSlotNames(
            self, fields, parentSlot = parentSlot)
        slotNames = slotNames[:]
        for slotName in ('height', 'size', 'width'):
            if slotName in slotNames:
                slotNames.remove(slotName)
        return slotNames

    def getViewLayoutSlotNames(self, fields, parentSlot = None):
        slotNames = things.ThingMixin.getViewLayoutSlotNames(
            self, fields, parentSlot = parentSlot)
        slotNames = slotNames[:]
        if not self.dataType or \
                not isTypeOfMimeType(self.dataType, 'image'):
            if 'height' in slotNames:
                slotNames.remove('height')
            if 'width' in slotNames:
                slotNames.remove('width')
        userToken = context.getVar('userToken', default = '')
        if not userToken or context.getVar('useCompactLayout', default = 0):
            for slotName in ['dataFileName', 'dataType', 'height', 'size',
                             'width']:
                if slotName in slotNames:
                    slotNames.remove(slotName)
        return slotNames
things.register(Upload)
