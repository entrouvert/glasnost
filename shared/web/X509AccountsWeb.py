# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@entrouvert.com>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost X509 Accounts Web"""

__version__ = '$Revision$'[11:-2]


import os
from mod_python import apache

import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.ObjectsCommon as commonObjects
import glasnost.common.tools_new as commonTools
import glasnost.common.xhtmlgenerator as X

import glasnost.proxy.X509AccountsProxy as proxyX509Accounts

import ObjectsWeb as objects
from tools import accessForbidden, getWebForServerRole, writePageLayout


class AdminX509Accounts(objects.AdminMixin,
                        proxyX509Accounts.AdminX509Accounts):
    pass
objects.register(AdminX509Accounts)


class X509Account(objects.ObjectWebMixin,
                      proxyX509Accounts.X509Account):
    pass
objects.register(X509Account)


class X509AccountsWeb(objects.ObjectsWebMixin,
                      proxyX509Accounts.X509AccountsProxy):
    def returnToRetryPage(self, stringError):
        req = context.getVar('req')
        req.headers_out['Cache-Control'] = 'no-cache, must-revalidate'

        context.push(_level = 'returnToRetryPage', layoutMode = 'edit')
        try:
            context.push(_level = 'login', layoutMode = 'edit')

            layout = X.array()                
            submitUrl = X.roleUrl(self.serverRole, action = 'login')
            form = X.form(action = submitUrl,
                          enctype = 'multipart/form-data',
                          method = 'post')
            layout += form
            buttonsBar = X.div(_class = 'buttons-bar')
            form += buttonsBar
            buttonsBar += X.buttonInForm(_('Retry'), 'retryButton')
            return writePageLayout(layout, _(stringError))        
        finally:
            context.pull(_level = 'returnToRetryPage')

    def login(self):
        serial = None

        req = context.getVar('req')
        env = apache.build_cgi_env(req)

        try:
            sslClientVerify = env['SSL_CLIENT_VERIFY']
        except KeyError:
            return self.returnToRetryPage(_('SSL not activated'))

        if sslClientVerify != 'SUCCESS':
            return self.returnToRetryPage(_(
                    'Client certificate verification error'))
        else:
            serial = env['SSL_CLIENT_M_SERIAL']
            try:
                userToken, authenticationMethod \
                        = self.checkObjectAuthentication(serial)
            except faults.WrongX509Serial:
                return self.returnToRetryPage(
                        _('Unknown certificate serial number = %s') % serial)
            except:
                if context.getVar('debug'):
                    raise
                return accessForbidden()
        identitiesWeb = getWebForServerRole('identities')
        return identitiesWeb.loginSucceeded(userToken, authenticationMethod)
    login.isPublicForWeb = 1

