# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Glasnost TAL Interpreter"""

__version__ = '$Revision$'[11:-2]


from cgi import escape

import glasnost.common.context as context
import glasnost.web.tools_new as webTools

from TAL.TALInterpreter import TALInterpreter
from TAL.HTMLParser import HTMLParseError


class GlasnostTALInterpreter(TALInterpreter):
    def insertHTMLStructure(self, text, repldict):
        try:
            return TALInterpreter.insertHTMLStructure(self, text, repldict)
        except HTMLParseError, e:
            webTools.sendTalkBack(context.getVar('req'),
                    '%s\n\n%s\n' % (str(e), text))
            if not context.getVar('debug'):
                self.stream_write('\n<!-- errors detected in HTML -->\n')
                self.stream_write(text)
                return
            self.stream_write('<h1>%s</h1>' % _('Error in generated HTML'))
            self.stream_write('<p>%s</p>' % escape(str(e)))
            self.stream_write('<pre>')
            lines = text.split('\n')
            for i, line in zip(range(len(lines)), lines):
                if i+1 == e.lineno:
                    self.stream.write('<strong>')
                self.stream.write('% 5d: %s\n' % (i+1, escape(line)))
                if i+1 == e.lineno:
                    self.stream.write('</strong>')
            self.stream_write('</pre>')

