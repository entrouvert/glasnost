# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Providers Common Models"""

__version__ = '$Revision$'[11:-2]


import ObjectsCommon as objects


class AdminProviders(objects.AdminCommon):
    remoteIdentityProviderId = None
    class remoteIdentityProviderId_kindClass:
        _kindName = 'Id'
        label = N_('Remote Identity Provider')
        serverRoles = ['providers']
        
        # FIXME: To do: Restrict to identity providers ids. Don't accept
        # service providers ids. (add a getValues() method)

    serverRole = 'providers'


class ProviderAbstract(objects.ObjectCommon):
    # certificate of the remote provider :
    certificateSignature = None
    class certificateSignature_kindClass:
        _kindName = 'String'
        label = N_('Certificate for Signature')
        isRequired = 0
        widget_colSpan = 2
        widgetName = 'TextArea'

    # public key of the remote provider :
    publicKeySignature = None
    class publicKeySignature_kindClass:
        _kindName = 'String'
        label = N_('Public Key for Signature')
        isRequired = 0
        widget_colSpan = 2
        widgetName = 'TextArea'

    # certificate file for SSL :
    caCertificateSSL = None
    class caCertificateSSL_kindClass:
        _kindName = 'String'
        label = N_('CA Certificate for SSL')
        isRequired = 0
        widget_colSpan = 2
        widgetName = 'TextArea'

    # peer CA Certificate for SSL :
    certificateSSL = None
    class certificateSSL_kindClass:
        _kindName = 'String'
        label = N_('Certificate for SSL')
        isRequired = 0
        widget_colSpan = 2
        widgetName = 'TextArea'
    

    # The URL to which the provider redirects at the end of user-agent-based
    # Federation Termination Notification Protocol profiles.
    federationTerminationServiceReturnUrl = None
    class federationTerminationServiceReturnUrl_kindClass:
        _kindName = 'String' # FIXME: url
        widget_size = 80

    # The URL Used for user-agent-based Federation Termination Notification
    # Protocol profiles.
    federationTerminationServiceUrl = None
    class federationTerminationServiceUrl_kindClass:
        _kindName = 'String' # FIXME: url
        widget_size = 80

    # The providerID of the entity.
    # Example http://serviceprovider.com
    providerId = None
    class providerId_kindClass:
        _kindName = 'String' # FIXME: uri
        isRequired = 1

    # The provider s redirecting URL for use after HTTP name registration has
    # taken place.
    registerNameIdentifierServiceReturnUrl = None
    class registerNameIdentifierServiceReturnUrl_kindClass:
        _kindName = 'String' # FIXME: uri
        widget_size = 80

    # The URL used for user-agent-based Register Name Identifier Protocol
    # profiles.
    registerNameIdentifierServiceUrl = None
    class registerNameIdentifierServiceUrl_kindClass:
        _kindName = 'String' # FIXME: uri
        widget_size = 80

    federationTerminationProtocolProfiles = None
    class federationTerminationProtocolProfiles_kindClass:
        _kindName = 'Sequence'
        class itemKind_valueClass:
            _kindName = 'String' # FIXME: uri
            widget_size = 80

    # The provider's preferred Register Name Identifier Protocol profile,
    # which should be used by other providers when registering a new
    # identifier. Each element MUST contain a valid Register Name Identifier
    # Protocol profile identification URI. The absence of this element SHALL
    # mean that the provider does not support any profile of the Register Name
    #Identifier Protocol.
    registerNameIdentifierProtocolProfiles = None
    class registerNameIdentifierProtocolProfiles_kindClass:
        _kindName = 'Sequence'
        class itemKind_valueClass:
            _kindName = 'String' # FIXME: uri
            widget_size = 80
    
    serverRole = 'providers'

    # The Single Logout Protocol profiles supported by the provider. Each
    # element MUST contain a valid Single Logout Protocol profile
    # identification URI. The absence of this element SHALL mean that the
    # provider does not support any profile of the Single Logout Protocol.
    singleLogoutProtocolProfiles = None
    class singleLogoutProtocolProfiles_kindClass:
        _kindName = 'Sequence'
        class itemKind_valueClass:
            _kindName = 'String' # FIXME: uri
            widget_size = 80

    # The URL to which the provider redirects at the end of user-agent-based
    # Single Logout Protocol profiles.
    singleLogoutServiceReturnUrl = None
    class singleLogoutServiceReturnUrl_kindClass:
        _kindName = 'String' # FIXME: url
        widget_size = 80

    # The URL Used for user-agent-based Single Logout Protocol profiles.
    singleLogoutServiceUrl = None
    class singleLogoutServiceUrl_kindClass:
        _kindName = 'String' # FIXME: url
        widget_size = 80

    # The provider SOAP endpoint URI.
    soapEndpoint = None
    class soapEndpoint_kindClass:
        _kindName = 'String' # FIXME: uri
        isRequired = 1
        widget_size = 80

    def getLabel(self):
        label = self.providerId
        if label is None:
            return ''
        return label


class IdentityProvider(ProviderAbstract):
    # The Single Sign-On Protocol profiles supported by the provider. Each
    # element MUST contain a valid Single Sign-On Protocol profile
    # identification URI.
    singleSignOnProtocolProfiles = None
    class singleSignOnProtocolProfiles_kindClass:
        _kindName = 'Sequence'
        class itemKind_valueClass:
            _kindName = 'String' # FIXME: uri
            widget_size = 80

    # The identity provider's URL for accepting authentication requests for the
    # Single Sign-On and Federation Protocol.
    singleSignOnServiceUrl = None
    class singleSignOnServiceUrl_kindClass:
        _kindName = 'String' # FIXME: uri
        isRequired = 1
        widget_size = 80


class ServiceProvider(ProviderAbstract):
    # URI of the SP for receiving Authentication Assertions from an
    # authenticating party.
    # FIXME: Liberty Alliance allows several AssertionConsumerServiceURLs.
    assertionConsumerServiceUrl = None
    class assertionConsumerServiceUrl_kindClass:
        _kindName = 'String' # FIXME: uri
        isRequired = 1
        widget_size = 80

    # if true, the service provider have to sign the Authentication Request
    # elements
    authnRequestSigned = None
    class authnRequestSigned_kindClass:
        _kindName = 'Boolean'
        isRequired = 1


class ProvidersCommonMixin(objects.ObjectsCommonMixin):
    adminClassName = 'AdminProviders'
    newObjectNameCapitalized = N_('New Provider')
    objectClassName = 'IdentityProvider'
    objectName = N_('provider')
    objectNameCapitalized = N_('Provider')
    objectsName = N_('providers')
    objectsNameCapitalized = N_('Providers')
    serverRole = 'providers'

