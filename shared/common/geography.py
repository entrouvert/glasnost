# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Geography Values"""

__version__ = '$Revision$'[11:-2]




countryCodes = [
    'af',
    'al',
    'dz',
    'as',
    'ad',
    'ao',
    'ai',
    'aq',
    'ag',
    'ar',
    'am',
    'aw',
    'au',
    'at',
    'az',
    'bs',
    'bh',
    'bd',
    'bb',
    'by',
    'be',
    'bz',
    'bj',
    'bm',
    'bt',
    'bo',
    'ba',
    'bw',
    'bv',
    'br',
    'io',
    'bn',
    'bg',
    'bf',
    'bi',
    'kh',
    'cm',
    'ca',
    'cv',
    'ky',
    'cf',
    'td',
    'cl',
    'cn',
    'cx',
    'cc',
    'co',
    'km',
    'cg',
    'cd',
    'ck',
    'cr',
    'ci',
    'hr',
    'cu',
    'cy',
    'cz',
    'dk',
    'dj',
    'dm',
    'do',
    'ec',
    'eg',
    'sv',
    'gq',
    'er',
    'ee',
    'et',
    'fk',
    'fo',
    'fj',
    'fi',
    'fr',
    'gf',
    'pf',
    'tf',
    'ga',
    'gm',
    'ge',
    'de',
    'gh',
    'gi',
    'gr',
    'gl',
    'gd',
    'gp',
    'gu',
    'gt',
    'gn',
    'gw',
    'gy',
    'ht',
    'hm',
    'va',
    'hn',
    'hk',
    'hu',
    'is',
    'in',
    'id',
    'ir',
    'iq',
    'ie',
    'il',
    'it',
    'jm',
    'jp',
    'jo',
    'kz',
    'ke',
    'ki',
    'kp',
    'kr',
    'kw',
    'kg',
    'la',
    'lv',
    'lb',
    'ls',
    'lr',
    'ly',
    'li',
    'lt',
    'lu',
    'mo',
    'mk',
    'mg',
    'mw',
    'my',
    'mv',
    'ml',
    'mt',
    'mh',
    'mq',
    'mr',
    'mu',
    'yt',
    'mx',
    'fm',
    'md',
    'mc',
    'mn',
    'ms',
    'ma',
    'mz',
    'mm',
    'na',
    'nr',
    'np',
    'nl',
    'an',
    'nc',
    'nz',
    'ni',
    'ne',
    'ng',
    'nu',
    'nf',
    'mp',
    'no',
    'om',
    'pk',
    'pw',
    'ps',
    'pa',
    'pg',
    'py',
    'pe',
    'ph',
    'pn',
    'pl',
    'pt',
    'pr',
    'qa',
    're',
    'ro',
    'ru',
    'rw',
    'sh',
    'kn',
    'lc',
    'pm',
    'vc',
    'ws',
    'sm',
    'st',
    'sa',
    'sn',
    'cs',
    'sc',
    'sl',
    'sg',
    'sk',
    'si',
    'sb',
    'so',
    'za',
    'gs',
    'es',
    'lk',
    'sd',
    'sr',
    'sj',
    'sz',
    'se',
    'ch',
    'sy',
    'tw',
    'tj',
    'tz',
    'th',
    'tl',
    'tg',
    'tk',
    'to',
    'tt',
    'tn',
    'tr',
    'tm',
    'tc',
    'tv',
    'ug',
    'ua',
    'ae',
    'gb',
    'us',
    'um',
    'uy',
    'uz',
    'vu',
    've',
    'vn',
    'vg',
    'vi',
    'wf',
    'eh',
    'ye',
    'zm',
    'zw'
    ]

africaanCountries = [
    'dz',
    'ao',
    'bj',
    'bw',
    'bf',
    'bi',
    'cm',
    'cf',
    'td',
    'cg',
    'cd',
    'ci',
    'dj',
    'eg',
    'gq',
    'er',
    'et',
    'ga',
    'gm',
    'gh',
    'gn',
    'gw',
    'ke',
    'ls',
    'lr',
    'ly',
    'mg',
    'mw',
    'ml',
    'mr',
    'ma',
    'mz',
    'na',
    'ne',
    'ng',
    'rw',
    'st',
    'sn',
    'sl',
    'so',
    'za',
    'sd',
    'sz',
    'tz',
    'tg',
    'tn',
    'ug',
    'eh',
    'zm',
    'zw',
]

asianCountries = [
    'af',
    'am',
    'az',
    'bh',
    'bd',
    'bt',
    'bn',
    'kh',
    'cn',
    'cy',
    'ge',
    'in',
    'id',
    'ir',
    'iq',
    'il',
    'jp',
    'jo',
    'kz',
    'kr',
    'kp',
    'kw',
    'kg',
    'la',
    'lb',
    'my',
    'mn',
    'np',
    'om',
    'pk',
    'pg',
    'ph',
    'qa',
    'sa',
    'sg',
    'lk',
    'sy',
    'tj',
    'th',
    'tr',
    'tm',
    'ae',
    'uz',
    'vn',
    'ye',
    'ps',
]

atlanticOceanCountries = [
    'bm',
    'bv',
    'cv',
    'fo',
    'fk',
    'sh',
    'gs'
    ]

australaisaCountries = [
    'au',
    'nz',
    'nf'
    ]

centralAmericaCountries = [
    'bz',
    'cr',
    'sv',
    'gt',
    'hn',
    'ni',
    'pa',
    'ag',
    'bs',
    'cu',
    'gd',
    'ht',
    'jm',
    'do',
    'lc',
    'vc',
    'tt',
    ]

europeanCountries = [
    'al',
    'ad',
    'at',
    'by',
    'be',
    'ba',
    'bg',
    'hr',
    'cz',
    'dk',
    'ee',
    'es',
    'fi',
    'fr',
    'de',
    'gi',
    'gr',
    'hu',
    'is',
    'ie',
    'it',
    'lv',
    'li',
    'lt',
    'lu',
    'mk',
    'mt',
    'md',
    'mc',
    'nl',
    'no',
    'pl',
    'pt',
    'ro',
    'ru',
    'sm',
    'sk',
    'si',
    'se',
    'ch',
    'ua',
    'gb',
    'va'
    ]


northAmericaCountries = [
    'ca',
    'gl',
    'mx',
    'us',
    'pm'
    ]
    

southAmericaCountries = [
    'ar',
    'bo',
    'br',
    'cl',
    'co',
    'ec',
    'gf',
    'gy',
    'py',
    'pe',
    'sr',
    'uy',
    've'
    ]


frenchRegions = [
    'alsace',
    'aquitaine',
    'auvergne',
    'bourgogne',
    'bretagne',
    'centre',
    'champagneardenne',
    'corse',
    'domtom',
    'franchecomte',
    'iledefrance',
    'languedocroussillon',
    'limousin',
    'lorraine',
    'midipyrenees',
    'nordpasdecalais',
    'paysdelaloire',
    'picardie',
    'poitoucharentes',
    'paca',
    'rhonealpes',
    'haute-normandie',
    'basse-normandie',
    ]

