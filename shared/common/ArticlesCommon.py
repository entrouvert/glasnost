# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Articles Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminArticlesCommon(AdminCommon):
    serverRole = 'articles'


class ArticleCommon(ObjectCommon):
    authorsSet = None
    authorsSet_kind_balloonHelp = N_(
        'Select an author (or a group of authors) for this article.')
    authorsSet_kindName = 'AuthorsSet'

    body = None
    class body_kindClass:
        _kindName = 'String'
        balloonHelp = N_('The content of this article in the above format. '\
                'Click on "Preview" to update the formatted preview below.')
        label = N_('Content')
        isRequired = 1
        isTranslatable = 1
        widget_colSpan = 2
        widget_preview = 1
        widgetName = 'TextArea'

        def getTextFormat(self, slot):
            return slot.getObject().format

    creationTime = None
    creationTime_kindName = 'CreationTime'

    editionTime = None
    editionTime_kind_formatString = '%Y-%m-%d %H:%M'
    editionTime_kind_importExport = 'from-server-only'
    editionTime_kindName = 'Time'

    format = 'spip'
    format_kind_balloonHelp = N_(
            "The text format of the article's content. "\
            "[SPIP->syntax-spip] or [reStructredText->syntax-rst] syntax reference.")
    
    format_kind_isRequired = 1
    format_kind_values = [
        'html',
        'spip',
        'rst',
        ]
    format_kindName = 'Choice'

    lastEditorId = None
    lastEditorId_kind_importExport = 'from-server-only'
    lastEditorId_kind_serverRoles = ['identities']
    lastEditorId_kindName = 'Id'

    modificationTime = None
    modificationTime_kindName = 'ModificationTime'

    readersSet = None
    readersSet_kindName = 'ReadersSet'

    serverRole = 'articles'

    title = None
    title_kind_balloonHelp = N_('A stand alone title should be meaningful.')
    title_kind_isRequired = 1
    title_kindName = 'String'

    writersSet = None
    writersSet_kindName = 'WritersSet'

    def getBody(self):
        return self.body

    def getLabel(self):
        label = self.getTitle()
        if label is None:
            return ''
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += [
            'title', 'format', 'body', 'creationTime', 'lastEditorId',
            'editionTime', 'modificationTime', 'authorsSet', 'writersSet',
            'readersSet']
        return slotNames

    def getTitle(self):
        return self.title


class ArticlesCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminArticles'
    newObjectNameCapitalized = N_('New Article')
    objectClassName = 'Article'
    objectName = N_('article')
    objectNameCapitalized = N_('Article')
    objectsName = N_('articles')
    objectsNameCapitalized = N_('Articles')
    serverRole = 'articles'

