# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost People Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminPeopleCommon(AdminCommon):
    defaultGroupId = None
    defaultGroupId_kind_balloonHelp = N_(
            'Select the group new users will be automatically added to.')
    defaultGroupId_kind_serverRoles = ['groups']
    defaultGroupId_kindName = 'Id'

    serverRole = 'people'

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = AdminCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += ['defaultGroupId']
        return slotNames


class PersonCommon(ObjectCommon):
    """Person super class used to be inherited with a Mixin Class.

    This is the definition of the attributes of a person object.
    This object is used by the PeopleProxy.
    """
    
    creationTime = None
    creationTime_kindName = 'CreationTime'

    cryptEmails = 0
    class cryptEmails_kindClass:
        _kindName = 'Boolean'
        balloonHelp = N_('Should Glasnost encrypt your emails?')
        isRequired = 1
        label = N_('Crypt Emails')
        labels = {
                '0': N_('Never'),        
                '1': N_('When Possible'),
                }

    email = None
    email_kind_balloonHelp = N_('Required field, as passwords are sent by email.')
    email_kindName = 'Email'

    fingerprint = None
    fingerprint_kind_balloonHelp = N_(
            'GnuPG public key fingerprint. '\
            'Necessary to receive encrypted e-mails.')
    fingerprint_kind_stateInViewMode = 'read-only/hidden-if-empty'
    fingerprint_kindName = 'Fingerprint'

    firstName = None
    firstName_kind_balloonHelp = N_("Glasnost's default behaviour is to display "\
            "people's names as follows: First Name \"Nickname\" Last Name. "\
            "Only the First Name field is required.")
    firstName_kind_isTranslatable = 0
    firstName_kind_isRequired = 1
    firstName_kindName = 'String'

    language = None
    class language_kindClass:
        _kindName = 'LanguageChoice'
        balloonHelp = None


    lastName = None
    lastName_kind_balloonHelp = N_('Optional but recommended.')
    lastName_kind_isTranslatable = 0
    lastName_kindName = 'String'

    modificationTime = None
    modificationTime_kindName = 'ModificationTime'

    nickname = None
    nickname_kind_balloonHelp = N_('Optional.')
    nickname_kind_isTranslatable = 0
    nickname_kind_stateInViewMode = 'read-only/hidden-if-empty'
    nickname_kindName = 'String'

    serverRole = 'people'

    def canCache(self):
        return 0

    def containsText(self, text):
        """Lookup up text in some instance attributes.

        The search is performed in the following attributes:
            + firstName
            + lastName
            + nickname
        
        Keyword argument
        ================

        *text*:
            The wanted text.

        Return
        ======

        *1*:
            The text was found.

        *0*:
            The text was not found.

        
        """
        text = text.lower()
        for attributeName in [
            'firstName',
            'lastName',
            'nickname',
            ]:
            attribute = getattr(self, attributeName)
            if type(attribute) is type('') \
               and attribute.lower().find(text) >= 0:
                return 1
        return 0

    def getEmail(self):
        return self.email

    def getFingerprint(self):
        return self.fingerprint

    def getFullName(self):
        """Return the full person name string.
        
        Return concatenation of firstName, lastName and nickname.
        
        """
        
        return getFullName(self.firstName, self.lastName, self.nickname)

    def getLabel(self):
        """Return the full person name string.

        Return concatenation of firstName, lastName and nickname.
        If this concatenation is impossible, and produce None, an empty string
        is returned.
        
        """
        
        label = self.getFullName()
        if not label:
            return self.email
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        """Return the layout slots names sequences.
        
        The layout slots names of the parent are taken, and to them, the
        class slot names are added.

        Keyword argument
        ================

        *parentSlot*:
            The slot where the object is instanciated.

        Return the slot names sequence, minimum slot names are the current
        object slots.
        
        """
        
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        if 'language' in slotNames:
            slotNames.remove('language')
        slotNames += [
            'firstName', 'lastName', 'nickname', 'email',
            'fingerprint', 'language', 'creationTime', 'modificationTime']
        return slotNames

    def getSortString(self):
        """Return the object string used for sorting.

        Return the concatenation of the lastName and firstName string.
        
        """
        
        if self.lastName:
            sortString = self.lastName
        else:
            sortString = ''
        if self.firstName:
            sortString += ' ' + self.firstName
        return sortString

    def mustCryptEmails(self):
        return self.cryptEmails


class PeopleCommonMixin(ObjectsCommonMixin):
    
    """Abstract class designed to be inherited with another class.

    The product of a multiple inheritance is a fonctionnal People server/proxy.
    All methods can be overriden or extended, in fact, this class define their
    default behavior.

    Attributes:
    ===========

    *adminClassName*:
        The class name string used for administrative purposes.
        
    *newObjectNameCapitalized*:
        The illustrating string explaining what do a new object creation.
        The objects are the objects handled by the subclass (not the class
        itself).
        
    *objectClassName*:
        The handled object class name string.
    
    *objectName*:
        The 'gettextized' handled object class name string.
    
    *objectNameCapitalized*:
        The capitalized 'gettextized' handled object class name string.
    
    *objectsName*:
        The 'gettextized' functionnal class name string (Usually the class name
        without the type (proxy or server, etc...)).
        
    *objectsNameCapitalized*:
        The capitalized 'gettextized'  class name string (Usually the class name
        without the type (proxy or server, etc...)).
    
    *serverRole*:
        The class server role string.
    
    """
    
    adminClassName = 'AdminPeople'
    newObjectNameCapitalized = N_('New Person')
    objectClassName = 'Person'
    objectName = N_('person')
    objectNameCapitalized = N_('Person')
    objectsName = N_('people')
    objectsNameCapitalized = N_('People')
    serverRole = 'people'


def getFullName(firstName, lastName, nickname = None):
    """Return the full person name string.
    
    Return concatenation of firstName, lastName and nickname.

    Keyword arguments
    =================

    *firstName*:
        The first name string

    *lastName*:
        The last name string

    *nickname*:
        The nickname string (default: None).

    """
    
    fullName = ''
    if firstName:
        if not fullName:
            fullName = firstName
        else:
            fullName = fullName + ' ' + firstName
    if nickname:
        if not fullName:
            fullName = '"' + nickname + '"'
        else:
            fullName = fullName + ' ' + '"' + nickname + '"'
    if lastName:
        if not fullName:
            fullName = lastName
        else:
            fullName = fullName + ' ' + lastName
    return fullName
