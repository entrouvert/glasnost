# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Identities Common Models"""

__version__ = '$Revision$'[11:-2]


import faults
import ObjectsCommon as objects
import things
import tools_new as commonTools


class AdminIdentities(objects.AdminCommon):
    identitiesRoles = ['people']
    class identitiesRoles_kindClass:
        _kindName = 'Sequence'
        class itemKind_valueClass:
            _kindName = 'ServerRole'
            defaultValue = 'people'

    privateKeySignature = None
    class privateKeySignature_kindClass:
        _kindName = 'String'
        label = N_('Private Key for Signature')
        isRequired = 0
        isTranslatable = 0
        widget_colSpan = 2
        widgetName = 'TextArea'

    privateKeySSL = None
    class privateKeySSL_kindClass:
        _kindName = 'String'
        label = N_('Private Key for SSL')
        isRequired = 0
        isTranslatable = 0
        widget_colSpan = 2
        widgetName = 'TextArea'

    symetricKey = None
    class symetricKey_kindClass:
        _kindName = 'String'
        label = N_('Symetric key to crypt/decrypt the RelayState')
        isRequired = 0
        isTranslatable = 0
        widget_colSpan = 2

    serverRole = 'identities'


class Identification(things.BaseThing):
    localNameIdentifier = None
    class localNameIdentifier_kindClass:
        _kindName = 'String' # FIXME: NameIdentifier or Token or...
        # isRequired = 1 FIXME removed for CVQ demo only. Uncomment ASAP.

    peerHostName = None
    class peerHostName_kindClass:
        _kindName = 'String' # FIXME.
        # isRequired = 1 FIXME removed for CVQ demo only. Uncomment ASAP.

    peerNameIdentifier = None
    class peerNameIdentifier_kindClass:
        _kindName = 'String' # FIXME: NameIdentifier or Token or...
        # isRequired = 1 FIXME removed for CVQ demo only. Uncomment ASAP.
things.register(Identification)
    

class Identity(objects.ObjectCommon):
    
    identityIdentifications = None
    class identityIdentifications_kindClass:
        _kindName = 'Sequence'
        label = N_('Identity Providers Identifications')
        class itemKind_valueClass:
            _kindName = 'Thing'
            valueThingCategory = 'other'
            valueThingName = 'Identification'
        stateInEditMode = 'hidden'
        stateInViewMode = 'hidden'

    language_kindName = None

    objectsMemory = None
    class objectsMemory_kindClass:
        _kindName = 'Memory'
        importExport = 'from-server-only'
        stateInEditMode = 'hidden'
        stateInViewMode = 'hidden'

    personId = None
    class personId_kindClass:
        _kindName = 'Id'
        # isRequired = 1 FIXME: uncomment later
        label = N_('Person')
        def getServerRoles(self, slot):
            from glasnost.proxy.IdentitiesProxy import IdentitiesProxy
            return IdentitiesProxy().getAdminIdentitiesRoles()

    serverRole = 'identities'

    serviceIdentifications = None
    class serviceIdentifications_kindClass:
        _kindName = 'Sequence'
        label = N_('Service Providers Identifications')
        class itemKind_valueClass:
            _kindName = 'Thing'
            valueThingCategory = 'other'
            valueThingName = 'Identification'
        stateInEditMode = 'hidden'
        stateInViewMode = 'hidden'

    spellcheckEntries = 1
    class spellcheckEntries_kindClass:
        _kindName = 'Boolean'
        balloonHelp = N_('Should Glasnost spellcheck your texts?')
        isRequired = 1
        label = N_('Spellcheck Entries')

    voteTokens = None
    class voteTokens_kindClass:
        _kindName = 'Mapping'
        importExport = 'from-server-only'
        class keyKind_valueClass:
            _kindName = 'Id'
            serverRoles = ['elections']
        stateInEditMode = 'hidden'
        stateInViewMode = 'hidden'
        class valueKind_valueClass:
            _kindName = 'Token'

    def getEmail(self):
        person = self.getPerson()
        if person is None:
            return None
        return person.getEmail()

    def getFingerprint(self):
        person = self.getPerson()
        if person is None:
            return None
        return person.getFingerprint()

    def getLabel(self):
        person = self.getPerson()
        if person is None:
            return '#%s' % commonTools.extractLocalId(self.id)
        return person.getLabel()

    def getLanguage(self):
        person = self.getPerson()
        if person is None:
            return None
        return person.getLanguage()

    def getPerson(self):
        if self.personId is not None:
            from glasnost.proxy.tools import getProxy
            personProxy = getProxy(self.personId)
            if personProxy is None:
                return None
            try:
                return personProxy.getObject(self.personId)
            except faults.MissingItem:
                pass
        return None

    def mustCryptEmails(self):
        person = self.getPerson()
        if person is None:
            return 0
        return person.mustCryptEmails()


class IdentitiesCommonMixin(objects.ObjectsCommonMixin):
    adminClassName = 'AdminIdentities'
    newObjectNameCapitalized = N_('New Identity')
    objectClassName = 'Identity'
    objectName = N_('identity')
    objectNameCapitalized = N_('Identity')
    objectsName = N_('identities')
    objectsNameCapitalized = N_('Identities')
    serverRole = 'identities'

