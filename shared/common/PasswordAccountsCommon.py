# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Password Accounts Common Models"""

__version__ = '$Revision$'[11:-2]


import ObjectsCommon as objects


class AdminPasswordAccounts(objects.AdminCommon):
    serverRole = 'passwordaccounts'

    storePasswordsInClearText = 1
    class storePasswordsInClearText_kindClass:
        _kindName = 'Boolean'
        isRequired = 1
        label = N_('Password Type')
        labels = {
                '0': N_('Crypted'),
                '1': N_('Clear Text'),        
                }
        widgetName = 'RadioButtons'

    userCanChoosePassword = 0
    class userCanChoosePassword_kindClass:
        _kindName = 'Boolean'
        balloonHelp = N_(
                'Specifies whether users are allowed to choose '
                'their passwords (instead of autogenerating one for them).')
        isRequired = 1
        label = N_('Password')
        labels = {
                '0': N_('Automatically Generated'),        
                '1': N_('User Choice'),
                }

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = objects.AdminCommon.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['userCanChoosePassword', 'storePasswordsInClearText']
        return slotNames


class PasswordAccount(objects.ObjectCommon):
    language_kindName = None
    
    login = None
    class login_kindClass:
        _kindName = 'String'
        balloonHelp = N_('Enter the username you use on this site.')
        isRequired = 1
        isTranslatable = 0
        label = N_('Username')
        textMaxLength = 40
        widget_size = 15

    password = None
    class password_kindClass:
        _kindName = 'Password'
        balloonHelp = N_('Enter your secret password.')
        isRequired = 0
        label = N_('Password')
        textMaxLength = 15
        widget_size = 15

    identityId = None
    class identityId_kindClass:
        _kindName = 'Id'
        isRequired = 1
        label = N_('Identity')
        serverRoles = ['identities']
    serverRole = 'passwordaccounts'

    def getLabel(self):
        label = self.login
        if label is None:
            return ''
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = objects.ObjectCommon.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['login', 'password', 'identityId']
        return slotNames


class PasswordAccountsCommonMixin(objects.ObjectsCommonMixin):
    adminClassName = 'AdminPasswordAccounts'
    newObjectNameCapitalized = N_('New Password Account')
    objectClassName = 'PasswordAccount'
    objectName = N_('password account')
    objectNameCapitalized = N_('Password Account')
    objectsName = N_('password accounts')
    objectsNameCapitalized = N_('Password Accounts')
    serverRole = 'passwordaccounts'

