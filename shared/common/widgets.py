# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Common Widgets"""

__version__ = '$Revision$'[11:-2]


import context
import properties
import things
from tools import *


register = things.register


class BaseWidget(things.BaseThing):
    apply = 0
    apply_kind_label = N_('Apply Button')
    apply_kindName = 'Boolean'

    colSpan = None
    colSpan_kind_importExport = 'private'
    colSpan_kind_stateInEditMode = 'hidden'
    colSpan_kind_stateInViewMode = 'hidden'
    colSpan_kindName = 'Integer'

    fieldLabel = None
    fieldLabel_kind_importExport = 'private'
    fieldLabel_kind_stateInEditMode = 'hidden'
    fieldLabel_kind_stateInViewMode = 'hidden'
    fieldLabel_kindName = 'String'

##     helpAlias = None
##     helpAlias_kind_label = N_('Help Alias')
##     helpAlias_kindName = 'Alias'

    thingCategory = 'widget'

    thingPublicCategory = N_('Widget')
    thingPublicCategory_kind_stateInEditMode = 'read-only/hidden-if-empty'
    thingPublicCategory_kind_stateInViewMode = 'read-only/hidden-if-empty'

    thingPublicName_kind_label = N_('Widget')
    thingPublicName_kind_stateInEditMode = 'read-only/hidden-if-empty'
    thingPublicName_kind_stateInViewMode = 'read-only/hidden-if-empty'

    def __init__(self, **attributes):
        things.BaseThing.__init__(self)
        for name, value in attributes.items():
            # Ensure that each argument has been declared as an attribute.
            if not hasattr(self.__class__, name):
                raise Exception('Undeclared attribute %s for %s' % (
                    name, self.__class__.__name__))
            if getattr(self, name) == value:
                continue
            setattr(self, name, value)

    def buildOptions(self, options):
        for name, value in options.items():
            # Ensure that each option has been declared as an attribute.
            nameBase = name.split('_')[0]
            if not hasattr(self.__class__, nameBase):
                raise Exception('Undeclared attribute %s for %s' % (
                    name, self.__class__.__name__))
            if hasattr(self, name) and getattr(self, name) == value:
                continue
            setattr(self, name, value)

##     def getHelpAlias(self, slot, fields):
##         if self.helpAlias:
##             return self.helpAlias
##         return slot.getKind().getHelpAlias(slot, fields)

    def getModelLabel(self, slot):
        if self.fieldLabel:
            return self.fieldLabel
        return slot.getKind().getModelLabel(slot)

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = things.BaseThing.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['apply']
        return slotNames

    def isInForm(self):
        # Quick & dirty hack.
        return context.getVar('inForm', default = 0)

    def isReadOnly(self, slot):
        # Quick & dirty hack.
        if context.getVar('readOnly', default = 0):
            return 1
        return slot.getKind().isReadOnly(slot)
register(BaseWidget)


class ExclusiveChoiceAbstract(BaseWidget):
    allLabel = N_('All')
    allLabel_kind_importExport = 'private'
    allLabel_kind_label = N_('All Label')
    allLabel_kind_stateInEditMode = 'hidden'
    allLabel_kind_stateInViewMode = 'hidden'
    allLabel_kindName = 'String'

    labels = None
    labels_kind_importExport = 'private'
    labels_kind_keyKind_valueName = 'String'
    labels_kind_stateInEditMode = 'hidden'
    labels_kind_stateInViewMode = 'hidden'
    labels_kind_valueKind_valueName = 'String'
    labels_kindName = 'Mapping'

    noneLabel = N_('None')
    noneLabel_kind_importExport = 'private'
    noneLabel_kind_label = N_('None Label')
    noneLabel_kind_stateInEditMode = 'hidden'
    noneLabel_kind_stateInViewMode = 'hidden'
    noneLabel_kindName = 'String'

    titles = None
    titles_kind_importExport = 'private'
    titles_kind_keyKind_valueName = 'String'
    titles_kind_stateInEditMode = 'hidden'
    titles_kind_stateInViewMode = 'hidden'
    titles_kind_valueKind_valueName = 'String'
    titles_kindName = 'Mapping'

    def getLabels(self, slot):
        if self.labels:
            return self.labels
        return slot.getKind().getLabels(slot)


class InputText(BaseWidget):
    size = None
    size_kind_label = N_('Width')
    size_kind_stateInViewMode = 'read-only/hidden-if-empty'
    size_kindName = 'Integer'

    thingPublicName = N_('Single-Line Text Entry Field')
register(InputText)


class Time(InputText):
    thingPublicName = N_('Time Entry Field')
register(Time)


class Duration(BaseWidget):
    thingPublicName = N_('Duration Entry Field')
register(Duration)


class Email(BaseWidget):
    size = 40
    size_kind_importExport = 'private'
    size_kind_label = N_('Width')
    size_kind_stateInEditMode = 'hidden'
    size_kind_stateInViewMode = 'hidden'
    size_kindName = 'Integer'

    thingPublicName = N_('E-mail Address Entry Field')
register(Email)


class InputCheckBox(BaseWidget):
    labels = None
    labels_kind_importExport = 'private'
    labels_kind_keyKind_valueName = 'String'
    labels_kind_stateInEditMode = 'hidden'
    labels_kind_stateInViewMode = 'hidden'
    labels_kind_valueKind_valueName = 'String'
    labels_kindName = 'Mapping'

    thingPublicName = N_('Check Box')

    titles = None
    titles_kind_importExport = 'private'
    titles_kind_keyKind_valueName = 'String'
    titles_kind_stateInEditMode = 'hidden'
    titles_kind_stateInViewMode = 'hidden'
    titles_kind_valueKind_valueName = 'String'
    titles_kindName = 'Mapping'

    def getLabels(self, slot):
        if self.labels:
            return self.labels
        return slot.getKind().getLabels(slot)
register(InputCheckBox)


class InputPassword(BaseWidget):
    size = None
    size_kind_importExport = 'private'
    size_kind_label = N_('Width')
    size_kind_stateInEditMode = 'hidden'
    size_kind_stateInViewMode = 'hidden'
    size_kindName = 'Integer'

    thingPublicName = N_('Password Entry Field')
register(InputPassword)


class Link(BaseWidget):
    thingPublicName = N_('Link Entry Field')
register(Link)


class Mapping(BaseWidget):
    fieldLabelPlural = None
    fieldLabelPlural_kindName = 'String'
register(Mapping)


class Multi(BaseWidget):
    addButtonLabel = None
    addButtonLabel_kind_importExport = 'private'
    addButtonLabel_kind_stateInEditMode = 'hidden'
    addButtonLabel_kind_stateInViewMode = 'hidden'
    addButtonLabel_kindName = 'String'

    fieldLabelPlural = None
    fieldLabelPlural_kind_importExport = 'private'
    fieldLabelPlural_kind_stateInEditMode = 'hidden'
    fieldLabelPlural_kind_stateInViewMode = 'hidden'
    fieldLabelPlural_kindName = 'String'

    inline = 0
    inline_kind_importExport = 'private'
    inline_kind_stateInEditMode = 'hidden'
    inline_kind_stateInViewMode = 'hidden'
    inline_kindName = 'Boolean'

    reorderingButtons = 1
    reorderingButtons_kind_importExport = 'private'
    reorderingButtons_stateInEditMode = 'hidden'
    reorderingButtons_stateInViewMode = 'hidden'
    reorderingButtons_kindName = 'Boolean'

    thingPublicName = N_('Multi-Line Field')

    uniqueValues = 0
    uniqueValues_kind_importExport = 'private'
    uniqueValues_kind_stateInEditMode = 'hidden'
    uniqueValues_kind_stateInViewMode = 'hidden'
    uniqueValues_kindName = 'Boolean'
register(Multi)


class MultiCheck(BaseWidget):
    pass
register(MultiCheck)


class Path(BaseWidget):
    thingPublicName = N_('File Path Entry Field')
register(Path)


class PushButton(BaseWidget):
    apply_kind_importExport = 'private'
    apply_kind_stateInEditMode = 'hidden'
    apply_kind_stateInViewMode = 'hidden'

    location = 'default'
    location_kind_label = N_('Location')
    location_kind_labels = {
        'action': N_('Action Buttons Bar'),
        'default': N_('Default'),
        'navigation': N_('Navigation Buttons Bar'),
        'other-action': N_('Custom Buttons Bar'),
        }
    location_kind_values = [
        'default',
        'navigation',
        'action',
        'other-action',
        ]
    location_kindName = 'Choice'

    thingPublicName = N_('Button')

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = BaseWidget.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['location']
        return slotNames
register(PushButton)


class RadioButtons(ExclusiveChoiceAbstract):
    thingPublicName = N_('Radio Buttons')
register(RadioButtons)


class Select(ExclusiveChoiceAbstract):
    thingPublicName = N_('Option Menu')
register(Select)


class SelectId(BaseWidget):
    noneLabel = N_('None')
    noneLabel_kind_label = N_('None Label')
    noneLabel_kindName = 'String'

    provideHypertextLink = 1
    provideHypertextLink_kind_label = N_('Provide hypertext link')
    provideHypertextLink_kindName = 'Boolean'

    showOthersButton = 1
    showOthersButton_kind_label = N_('Others Button')
    showOthersButton_kindName = 'Boolean'

    thingPublicName = N_('Option Menu')

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = BaseWidget.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['showOthersButton', 'noneLabel']
        return slotNames
register(SelectId)


class TextArea(BaseWidget):
    cols = 80
    cols_kind_label = N_('Width')
    cols_kindName = 'Integer'

    preview = 0
    preview_kind_label = N_('Show Preview')
    preview_kindName = 'Boolean'

    rows = 25
    rows_kind_label = N_('Height')
    rows_kindName = 'Integer'

    thingPublicName = N_('Multi-Line Text Entry Field')

    viewInTextArea = 0
    viewInTextArea_kind_importExport = 'private'
    viewInTextArea_kind_label = N_('View In Text Area')
    viewInTextArea_kind_stateInEditMode = 'hidden'
    viewInTextArea_kind_stateInViewMode = 'hidden'
    viewInTextArea_kindName = 'Boolean'

##     def getHelpAlias(self, slot, fields):
##         if self.format == 'spip':
##             return 'help-spip-syntax'
##         else:
##             return BaseWidget.getHelpAlias(self, slot, fields)

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = BaseWidget.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['cols', 'rows', 'preview', 'viewInTextArea']
        return slotNames
register(TextArea)


class Thing(BaseWidget):
    thingPublicName = N_('Standard')
register(Thing)


class Token(BaseWidget):
    pass
register(Token)

        
class UploadFile(BaseWidget):
    thingPublicName = N_('Upload Field')
register(UploadFile)


class Url(BaseWidget):
    size = None
    size_kind_label = N_('Width')
    size_kind_stateInViewMode = 'read-only/hidden-if-empty'
    size_kindName = 'Integer'

    thingPublicName = N_('URL Entry Field')
register(Url)


class XSelect(Select):
    otherFieldLabel = None
    otherFieldLabel_kindName = 'String'

    size = None
    size_kind_label = N_('Width')
    size_kind_stateInViewMode = 'read-only/hidden-if-empty'
    size_kindName = 'Integer'

    thingPublicName = N_('Combo Box')
register(XSelect)

