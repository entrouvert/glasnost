# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@entrouvert.com>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost XMLRPC Faults"""

__version__ = '$Revision$'[11:-2]


import types
import xmlrpclib


faultCodeUnknownServerException = 0
faultCodeUnknownApplicationToken = 1
faultCodeUnknownServerId = 2
faultCodeConnectionRefused = 3
faultCodeUnknownUserToken = 4
faultCodeUserAccessDenied = 5
faultCodeMissingItem = 6
faultCodeUnknownSessionToken = 7
faultCodeApplicationAccessDenied = 8
faultCodeReadOnlyObject = 9
faultCodeMissingMainRubric = 10
faultCodeBadEmailAddress = 11
faultCodeSmtpError = 12
faultCodeNonExistentFilePath = 13
faultCodeNotAFile = 14
faultCodeFileAccessDenied = 15
faultCodeWrongLogin = 16
faultCodeWrongPassword = 17
faultCodeUnknownStringDigest = 18
faultCodeWrongVersion = 19
faultCodeIncompatibleKinds = 20
faultCodeServerRegistrationDenied = 21
faultCodeNoneValue = 22
faultCodeMissingId = 23
faultCodeDuplicateValue = 24
faultCodeInconsistentPrototypeHierarchy = 25
faultCodeNonExistentSlotPath = 26
faultCodeBadValue = 27
faultCodeInvalidSessionToken = 28
faultCodeMissingSlotValue = 29
faultCodeBadSlotValue = 30
faultCodeKindsIncompatibility = 31
faultCodeInvalidSearchRequest = 32
faultCodeUnknownDispatcherInId = 33
faultCodeUnregisteredServer = 34
faultCodeUnknownAuthenticationMethod = 35
faultCodeUncountableGroup = 36
faultCodeNotAGroupUnion = 37
faultCodeIllegalRecursiveGroup = 38
faultCodeStringNotAvailableThroughGettext = 39
faultCodeUnknownCommandAction = 40
faultCodeValueTooBig = 41
faultCodeValueTooSmall = 42
faultCodeRoleNotInProfiles = 43
faultCodeUnknownObjectVersion = 44

faultCodeUnknownVoteToken = 1000
faultCodeUnknownVoterToken = 1001
faultCodeUnknownElectionToken = 1002
faultCodeWinnersGroupNotEmpty = 1003

faultCodeDuplicateLogin = 2000
faultCodeDuplicateFullName = 2001
faultCodeDuplicateEmail = 2002
faultCodeDuplicateFingerprint = 2003

faultCodeDuplicateHostName = 3000

# Accounting
faultCodeMissingParent = 4000
faultCodeDuplicateNumber = 4001

faultCodeWrongArtifact = 5000
faultCodeUnableToChangePasswordStorage = 5001
faultCodeWrongNameIdentifier = 5002
faultCodeWrongX509Serial = 5003
faultCodeDuplicateSerial = 5004
faultCodeWrongToken = 5005
faultCodeDisabledAccount = 5006

faultCodeUnresizableImage = 6000


Fault = xmlrpclib.Fault


class BaseFault(Fault):
    """Base class for errors in Glasnost XML-RPC.
    
    This class extend the xmlrpclib.Fault constructor with Glasnost faultCode.
    
    Attribute:
    ==========

    *faultCode*:
        The error fault code. It must be **set in subclasses**.
    """
    uiFaultString = N_('Unknown error')
    
    def __init__(self, *arguments):
        Fault.__init__(self, self.faultCode,
                       self.makeFaultString(*arguments))

    def makeFaultString(self, *arguments):
        return 'Unknown fault (arguments = %s)' % str(arguments)


class UnknownServerException(BaseFault):
    faultCode = faultCodeUnknownServerException

    def makeFaultString(self):
        return 'Unknown server exception'


class UnknownApplicationToken(BaseFault):
    faultCode = faultCodeUnknownApplicationToken

    def makeFaultString(self, applicationToken):
        return 'Unknown application token = %s' % applicationToken


class UnknownServerId(BaseFault):
    faultCode = faultCodeUnknownServerId

    def makeFaultString(self, serverId):
        return 'Unknown server id = %s' % serverId
 

class ConnectionRefused(BaseFault):
    faultCode = faultCodeConnectionRefused

    def makeFaultString(self, serverHostName, serverPort, functionName,
                        arguments):
        return 'Connection refused by server %s:%s while calling' \
               ' %s(*%s)' % (serverHostName, serverPort, functionName,
                             arguments)


class UnknownUserToken(BaseFault):
    faultCode = faultCodeUnknownUserToken

    def makeFaultString(self, userToken):
        return 'Unknown user token = %s' % userToken


class UserAccessDenied(BaseFault):
    faultCode = faultCodeUserAccessDenied

    def makeFaultString(self):
        return 'Access denied to user'


class MissingItem(BaseFault):
    faultCode = faultCodeMissingItem

    def makeFaultString(self, itemId):
        return 'Missing item = %s' % itemId


class UnknownSessionToken(BaseFault):
    faultCode = faultCodeUnknownSessionToken

    def makeFaultString(self, sessionToken):
        return 'Unknown session token = %s' % sessionToken


class ApplicationAccessDenied(BaseFault):
    faultCode = faultCodeApplicationAccessDenied

    def makeFaultString(self, virtualServerId):
        return 'Access denied to application = %s' % virtualServerId


class ReadOnlyObject(BaseFault):
    faultCode = faultCodeReadOnlyObject

    def makeFaultString(self):
        return 'Object is read only'


class MissingMainRubric(BaseFault):
    faultCode = faultCodeMissingMainRubric

    def makeFaultString(self):
        return 'Main rubric is missing'


class BadEmailAddress(BaseFault):
    faultCode = faultCodeBadEmailAddress
    uiFaultString = N_('Invalid email address')

    def makeFaultString(self, email):
        return 'Bad email address = %s' % email


class SmtpError(BaseFault):
    faultCode = faultCodeSmtpError

    def makeFaultString(self):
        return 'A SMTP error occurred'


class NonExistentFilePath(BaseFault):
    faultCode = faultCodeNonExistentFilePath
    uiFaultString = N_('Specified file path does not exist!')

    def makeFaultString(self, filePath):
        return 'Non existent file path = %s' % filePath


class NotAFile(BaseFault):
    faultCode = faultCodeNotAFile
    uiFaultString = N_('Specified path is not a file!')

    def makeFaultString(self, filePath):
        return 'Not a file = %s' % filePath


class FileAccessDenied(BaseFault):
    faultCode = faultCodeFileAccessDenied
    uiFaultString = N_('Access to file is denied!')

    def makeFaultString(self, filePath):
        return 'File access denied = %s' % filePath


class WrongLogin(BaseFault):
    faultCode = faultCodeWrongLogin
    uiFaultString = N_('Wrong value!')

    def makeFaultString(self, login):
        return 'Unknown login = "%s"' % login


class WrongPassword(BaseFault):
    faultCode = faultCodeWrongPassword
    uiFaultString = N_('Wrong value!')

    def makeFaultString(self, password):
        return 'Unknown password = "%s"' % password


class UnknownStringDigest(BaseFault):
    faultCode = faultCodeUnknownStringDigest

    def makeFaultString(self, digest):
        return 'Unknown string digest = "%s"' % digest


class WrongVersion(BaseFault):
    faultCode = faultCodeWrongVersion

    def makeFaultString(self):
        return 'Wrong version'


class IncompatibleKinds(BaseFault):
    faultCode = faultCodeIncompatibleKinds

    def makeFaultString(self, kind1, kind2):
        return 'Incompatible kinds : %s(%s) and %s(%s)' % (
                kind1, kind1.__dict__, kind2, kind2.__dict__)


class ServerRegistrationDenied(BaseFault):
    faultCode = faultCodeServerRegistrationDenied

    def makeFaultString(self, hostName, port):
        return 'Registration denied to host name = %s, port = %s' % (
            hostName, port)


class NoneValue(BaseFault):
    faultCode = faultCodeNoneValue

    def makeFaultString(self):
        return 'Wrong value: None'


class MissingId(BaseFault):
    faultCode = faultCodeMissingId

    def makeFaultString(self):
        return 'Missing ID'


class DuplicateValue(BaseFault):
    faultCode = faultCodeDuplicateValue
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, name, value):
        return 'Duplicate value for %s = %s' % (name, value)


class InconsistentPrototypeHierarchy(BaseFault):
    faultCode = faultCodeInconsistentPrototypeHierarchy

    def makeFaultString(self, implementation):
        return 'Inconsistent prototype hierarchy for implementation %s' \
               % implementation


class NonExistentSlotPath(BaseFault):
    faultCode = faultCodeNonExistentSlotPath

    def makeFaultString(self, slotPath):
        return 'Non existent slot path = %s' % slotPath


class BadValue(BaseFault):
    faultCode = faultCodeBadValue
    uiFaultString = N_('Wrong value!')

    def makeFaultString(self):
        return 'Bad value for object'


class InvalidSessionToken(BaseFault):
    faultCode = faultCodeInvalidSessionToken

    def makeFaultString(self, sessionToken):
        return 'Invalid session token = %s' % sessionToken


class MissingSlotValue(BaseFault):
    faultCode = faultCodeMissingSlotValue
    uiFaultString = N_('Missing value!')

    def makeFaultString(self, slot):
        return 'Missing slot = %s' % (slot)


class BadSlotValue(BaseFault):
    faultCode = faultCodeBadSlotValue

    def makeFaultString(self, slot, value):
        return 'Bad value (= %s) for slot "%s"' % (value, slot)


class KindsIncompatibility(BaseFault):
    faultCode = faultCodeKindsIncompatibility

    def makeFaultString(self, theoricalKind, realKind):
        return 'Kind %s doesn\'t accept kind %s' % (theoricalKind, realKind)


class InvalidSearchRequest(BaseFault):
    faultCode = faultCodeInvalidSearchRequest

    def makeFaultString(self, searchKeyword, searchValue):
        return 'Invalid search request (%s = %s)' % (
                searchKeyword, searchValue)


class UnknownDispatcherInId(BaseFault):
    faultCode = faultCodeUnknownDispatcherInId

    def makeFaultString(self, id):
        import tools_new as commonTools
        return 'Unknown dispatcher (= %s) in id (= %s)' % (
                commonTools.extractDispatcherId(id), id)


class UnregisteredServer(BaseFault):
    faultCode = faultCodeUnregisteredServer

    def makeFaultString(self, hostName, port):
        return 'Unregistered server: host name = %s, port = %s' % (
                hostName, port)


class UnknownAuthenticationMethod(BaseFault):
    faultCode = faultCodeUnknownAuthenticationMethod

    def makeFaultString(self, authenticationMethod):
        return 'Unknown authentication method: %s' % authenticationMethod


class UncountableGroup(BaseFault):
    faultCode = faultCodeUncountableGroup

    def makeFaultString(self, group):
        return 'Uncountable group: %s' % group


class NotAGroupUnion(BaseFault):
    faultCode = faultCodeNotAGroupUnion

    def makeFaultString(self, group):
        return 'Group %s is not an union' % group


class IllegalRecursiveGroup(BaseFault):
    faultCode = faultCodeIllegalRecursiveGroup

    def makeFaultString(self, groupId):
        return 'Illegal recursive group %s' % groupId


class StringNotAvailableThroughGettext(BaseFault):
    faultCode = faultCodeStringNotAvailableThroughGettext

    def makeFaultString(self, string):
        return 'String not available throught gettext: \'%s\'' % string


class UnknownCommandAction(BaseFault):
    faultCode = faultCodeUnknownCommandAction

    def makeFaultString(self, action):
        return 'Unknown command action: "%s"' % action


class WrongArtifact(BaseFault):
    faultCode = faultCodeWrongArtifact

    def makeFaultString(self, artifact):
        return 'Unknown artifact = %s' % artifact


class UnableToChangePasswordStorage(BaseFault):
    faultCode = faultCodeUnableToChangePasswordStorage

    def makeFaultString(self):
        return 'Unable to change password storage'


class WrongNameIdentifier(BaseFault):
    faultCode = faultCodeWrongNameIdentifier

    def makeFaultString(self, nameIdentifier):
        return 'Unknown name identifier = %s' % nameIdentifier


class WrongX509Serial(BaseFault):
    faultCode = faultCodeWrongX509Serial
    uiFaultString = N_('Wrong value!')

    def makeFaultString(self, serial):
        return 'Unknown serial = "%s"' % serial

class DuplicateSerial(BaseFault):
    faultCode = faultCodeDuplicateSerial
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, serial):
        return 'Duplicate serial = "%s"' % serial

class WrongToken(BaseFault):
    faultCode = faultCodeWrongToken
    uiFaultString = N_('Wrong value!')

    def makeFaultString(self, password):
        return 'Unknown password = "%s"' % password

class DisabledAccount(BaseFault):
    faultCode = faultCodeDisabledAccount

    def makeFaultString(self):
        return 'Disabled account'


class UnresizableImage(BaseFault):
    faultCode = faultCodeUnresizableImage

    def makeFaultString(self):
        return 'unresizable image'


class UnknownStringDigest(BaseFault):
    faultCode = faultCodeUnknownStringDigest

# Vote


class UnknownVoteToken(BaseFault):
    faultCode = faultCodeUnknownVoteToken

    def makeFaultString(self, voteToken):
        return 'Unknown vote token = %s' % voteToken


class UnknownVoterToken(BaseFault):
    faultCode = faultCodeUnknownVoterToken

    def makeFaultString(self, voterToken):
        return 'Unknown voter token = %s' % voterToken


class UnknownElectionToken(BaseFault):
    faultCode = faultCodeUnknownElectionToken

    def makeFaultString(self, electionToken):
        return 'Unknown election token = %s' % electionToken


class WinnersGroupNotEmpty(BaseFault):
    faultCode = faultCodeWinnersGroupNotEmpty
    uiFaultString = N_('Winners group is not empty!')

    def makeFaultString(self):
        return 'Winner group not empty'


#


class DuplicateLogin(BaseFault):
    faultCode = faultCodeDuplicateLogin
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, login):
        return 'Duplicate login = %s' % login


class DuplicateFullName(BaseFault):
    faultCode = faultCodeDuplicateFullName
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, fullName):
        return 'Duplicate fullName = %s' % fullName


class DuplicateEmail(BaseFault):
    faultCode = faultCodeDuplicateEmail
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, email):
        return 'Duplicate email address = %s' % email


class DuplicateFingerprint(BaseFault):
    faultCode = faultCodeDuplicateFingerprint
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, fingerprint):
        return 'Duplicate fingerprint = %s' % fingerprint


class DuplicateHostName(BaseFault):
    faultCode = faultCodeDuplicateHostName
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, hostName):
        return 'Duplicate host name = %s' % hostName


class DuplicateNumber(BaseFault):
    faultCode = faultCodeDuplicateNumber
    uiFaultString = N_('Value already used!')

    def makeFaultString(self, number):
        return 'Duplicate number = %s' % number


class MissingParent(BaseFault):
    faultCode = faultCodeMissingParent

    def makeFaultString(self, objectId):
        return 'Missing parent for %s' % objectId


class ValueTooSmall(BaseFault):
    faultCode = faultCodeValueTooSmall
    uiFaultString = N_('Value too small!')

    def makeFaultString(self, slot, value):
        return 'Value (= %s) too small for slot "%s"' % (value, slot)


class ValueTooBig(BaseFault):
    faultCode = faultCodeValueTooBig
    uiFaultString = N_('Value too big!')

    def makeFaultString(self, slot, value):
        return 'Value (= %s) too big for slot "%s"' % (value, slot)


class RoleNotInProfiles(BaseFault):
    faultCode = faultCodeRoleNotInProfiles
    uiFaultString = N_('Role not in profiles')

    def makeFaultString(self, role):
        return 'Role (%s) not in profiles' % role


class UnknownObjectVersion(BaseFault):
    faultCode = faultCodeUnknownObjectVersion
    uiFaultString = N_('Unknown object revision')

    def makeFaultString(self, objectId, versionNumber):
        return 'Unknown version (%d) for object %s' % (versionNumber, objectId)



faults = {}
for value in locals().values():
    if type(value) == types.ClassType and not value in (Fault, BaseFault):
        faults[value.faultCode] = value

