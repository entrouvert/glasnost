# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Comments Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminCommentsCommon(AdminCommon):
    serverRole = 'comments'


class CommentCommon(ObjectCommon):
    authorId = None
    authorId_kind_importExport = 'from-server-only'
    authorId_kind_serverRoles = ['people']
    authorId_kindName = 'Id'
    
    body = None
    body_kind_balloonHelp = N_('Enter the text of your comment.')
    body_kind_isTranslatable = 0
    body_kind_isRequired = 1
    body_kind_textFormat = 'spip'
    body_kindName = 'String'

    creationTime = None
    creationTime_kind_importExport = 'from-server-only'
    creationTime_kindName = 'CreationTime'

    isEditorial = 0
    class isEditorial_kindClass:
        _kindName = 'Boolean'
        label = N_('Editorial Comment ?')

    parentId = None
    parentId_kindName = 'Id'

    serverRole = 'comments'

    def canCache(self):
        return 1

    def getLabel(self):
        return _('some comment')

    def getLabelLanguage(self):
        '''Return an empty string so that label is not translated'''
        return ''

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += ['authorId', 'body' ]
        return slotNames


class CommentsCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminComments'
    newObjectNameCapitalized = N_('New Comment')
    objectClassName = 'Comment'
    objectName = N_('comment')
    objectNameCapitalized = N_('Comment')
    objectsName = N_('comments')
    objectsNameCapitalized = N_('Comments')
    serverRole = 'comments'

