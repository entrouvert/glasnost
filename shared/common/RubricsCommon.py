# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Rubrics Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminRubricsCommon(AdminCommon):
    mainRubricId = None
    mainRubricId_kind_balloonHelp = N_(
        "The [main->main-rubric] rubric is the top of the site's tree.")
    mainRubricId_kind_label = N_('Main Rubric')
    mainRubricId_kind_serverRoles = ['rubrics']
    mainRubricId_kindName = 'Id'

    serverRole = 'rubrics'

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = AdminCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += ['mainRubricId']
        return slotNames


class RubricCommon(ObjectCommon):
    contentId = None
    contentId_kind_balloonHelp = N_(
        'Select a [content->rubrics-help] article (or "None") for this rubric. '\
        'Click on "[Others->buttons-others]" if it is not proposed in the list.')
    contentId_kind_label = N_('Content')
    contentId_kind_serverRoles = ['articles']
    contentId_kindName = 'Id'

    membersSet = None
    membersSet_kind_balloonHelp = N_(
        'Select a [member->rubrics-help] [object->objects] of this rubric. '\
        'Click on "[Others->buttons-others]" if it is not proposed in the list. '\
        'Click on "Add" to add another one. '\
        'Select "None" to remove one.')
    membersSet_kind_itemKind_rememberedIds = 1
    membersSet_kind_itemKind_valueName = 'Id'
    membersSet_kind_label = N_('Members')
    membersSet_kindName = 'Sequence'

    name = None
    name_kind_balloonHelp = N_('A meaningful name or title.')
    name_kind_isRequired = 1
    name_kind_label = N_('Name')
    name_kindName = 'String'

    readersSet = None
    readersSet_kindName = 'ReadersSet'

    serverRole = 'rubrics'

    writersSet = None
    writersSet_kindName = 'WritersSet'

    def getLabel(self):
        label = self.name
        if label is None:
            return ''
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += [
            'name', 'contentId', 'membersSet', 'writersSet', 'readersSet']
        return slotNames


class RubricsCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminRubrics'
    newObjectNameCapitalized = N_('New Rubric')
    objectClassName = 'Rubric'
    objectName = N_('rubric')
    objectNameCapitalized = N_('Rubric')
    objectsName = N_('rubrics')
    objectsNameCapitalized = N_('Rubrics')
    serverRole = 'rubrics'
