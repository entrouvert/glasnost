# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Common Modes"""

__version__ = '$Revision$'[11:-2]


import system
import things


register = things.register


class Aspect(things.BaseThing):
    importExport = 'public'
    class importExport_kindClass:
        _kindName = 'Choice'
        defaultValue = 'public'
        isRequired = 1
        label = N_('Protection')
        labels = {
                'from-server-only': N_('Read Only'),
                'private': N_('Private'),
                'public': N_('Read/Write'),
                'to-server-only': N_('Write Only'),
                }
        sortLabels = 0
        values = [
                'public',
                'from-server-only',
                'to-server-only',
                'private',
                ]

    name = None
    class name_kindClass:
        _kindName = 'Choice'
        balloonHelp = N_('Enter an internal (non user-friendly) field name')
        isRequired = 0
        label = N_('Name')
        widget_apply = 1

        def getValues(self, slot):
            card = slot.getObject()
            return card.getPropertyNames()

    state = 'read-only'
    class state_kindClass:
        _kindName = 'Choice'
        defaultValue = 'read-only'
        isRequired = 1
        label = N_('Display')
        labels = {
                'hidden': N_('Hidden'),
                'read-only': N_('Read Only'),
                'read-only/hidden-if-empty': N_('Read Only / Hidden If Empty'),
                'read-write': N_('Read/Write'),
                }
        sortLabels = 0
        values = [
                'hidden',
                'read-only/hidden-if-empty',
                'read-only',
                'read-write',
                ]

    htmlTag = 'div-with-label'
    class htmlTag_kindClass:
        _kindName = 'Choice'
        defaultValue = 'div-with-label'
        isRequired = 1
        label = N_('HTML Tag')
        labels = {
                'div': N_('DIV Without Label'),
                'div-with-label': N_('DIV With Label'),
                'h1': N_('H1'),
                'h2': N_('H2'),
                'h3': N_('H3'),
                'h4': N_('H4'),
                'h5': N_('H5'),
                'h6': N_('H6'),
                }
        sortLabels = 0
        values = [
                'div-with-label',
                'div',
                'h1',
                'h2',
                'h3',
                'h4',
                'h5',
                'h6',
                ]
    
    widget = None
    class widget_kindClass:
        _kindName = 'Widget'
        balloonHelp = N_('Select a field widget')
        isRequired = 1
        label = N_('Widget')

        def getGroupedValues(self, slot):
            return (None, None) # groupNames, groupedValues

        def getValues(self, slot):
            nameSlot = slot.getContainer().getSlot(
                    'name', parentSlot = slot.parent)
            name = nameSlot.getValue()
            if name:
                card = slot.getObject()
                if card.hasSlotName(name):
                    kind = card.getSlot(name).getKind()
                    return kind.getPossibleWidgetNames()
            # When the property name is missing, return an empty list of
            # widgets.
            # return self.getRealKindClass().getValues(self, slot)
            return []

    def getOrderedFieldSlotNames(self, fields, parentSlot = None):
        slotNames = things.ThingMixin.getOrderedFieldSlotNames(
                self, fields, parentSlot = parentSlot)
        # The field name must be set before the field widget.
        slotNames += ['name', 'state', 'importExport', 'htmlTag', 'widget']
        return slotNames

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = things.BaseThing.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['name', 'state', 'importExport', 'htmlTag', 'widget']
        return slotNames

    def isEmpty(self, parentSlot = None):
        return not self.name
register(Aspect)


class ModeAbstract(things.BaseThing):
    name = None
    class name_kindClass:
        _kindName = 'PythonIdentifier'
        balloonHelp = N_('Enter an internal (non user-friendly) mode name')
        isRequired = 1
        label = N_('Name')

    thingCategory = 'mode'

    thingPublicCategory = N_('Mode')

    def getMenuLabel(self):
        return self.getName()

    def getName(self):
        if self.name:
            return self.name
        return _('Untitled')

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = things.BaseThing.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['name', 'usersSet']
        return slotNames
register(ModeAbstract)


class Custom(ModeAbstract):
    aspects = None
    class aspects_kindClass:
        _kindName = 'Sequence'
        label = N_('Fields')
        class itemKind_valueClass:
            _kindName = 'Thing'
            valueThingCategory = 'other'
            valueThingName = 'Aspect'

    class name_kindClass(ModeAbstract.name_kindClass):
        invalidValues = ['edit', 'new', 'view', 'viewAll']

    thingPublicName = N_('Custom Mode')

    usersSet = [system.generalPublicId]
    class usersSet_kindClass:
        _kindName = 'UsersSet'

    def getMenuLabel(self):
        return '%s (%s)' % (self.getName(), self.getThingPublicName())

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ModeAbstract.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['aspects']
        return slotNames
register(Custom)


class Edit(ModeAbstract):
    name = 'edit'
    class name_kindClass(ModeAbstract.name_kindClass):
        importExport = 'private'

    thingPublicName = N_('Edit Mode')
register(Edit)


class View(ModeAbstract):
    name = 'view'
    class name_kindClass(ModeAbstract.name_kindClass):
        importExport = 'private'

    thingPublicName = N_('View Mode')
register(View)


class ViewAll(ModeAbstract):
    name = 'viewAll'
    class name_kindClass(ModeAbstract.name_kindClass):
        importExport = 'private'

    objectModeName = 'view'
    class objectModeName_kindClass:
        _kindName = 'PythonIdentifier' # FIXME: Replace by ModeName.
        label = N_('Items Mode')

    thingPublicName = N_('View All Mode')
register(ViewAll)

