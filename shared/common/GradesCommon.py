# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Grades Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminGradesCommon(AdminCommon):
    serverRole = 'grades'


class GradeCommon(ObjectCommon):
    marks = None
    marks_kind_isRequired = 1
    marks_kindName = 'Marks'

    membersSet = None
    membersSet_kind_itemKind_value_serverRoles = ['identities']
    membersSet_kind_itemKind_valueName = 'Id'
    membersSet_kind_minCount = 1
    membersSet_kindName = 'Sequence'

    name = None
    name_kind_isRequired = 1
    name_kindName = 'String'

    serverRole = 'grades'

    def canCache(self):
        return 1

    def getLabel(self):
        label = self.name
        if label is None:
            return ''
        return label

    def getMarks(self, objectIds):
        return self.repairMarks(self.marks, objectIds)

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += ['name', 'membersSet', 'marks']
        return slotNames

    def repairMarks(self, marks, objectIds):
        if not marks:
            return {}
        distribution = {}
        marksSum = 0
        for objectId, mark in marks.items():
            if not objectId in objectIds:
                continue
            marksSum += mark
        if marksSum == 0:
            score = 1 / float(len(objectIds))
            for objectId in objectIds:
                distribution[objectId] = score
        else:
            coef = 1 / float(marksSum)
            for objectId in objectIds:
                if marks.has_key(objectId):
                    value = marks[objectId] * coef
                else:
                    value = 0
                distribution[objectId] = value
        return distribution


class GradesCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminGrades'
    newObjectNameCapitalized = N_('New Grade')
    objectClassName = 'Grade'
    objectName = N_('grading')
    objectNameCapitalized = N_('Grading')
    objectsName = N_('gradings')
    objectsNameCapitalized = N_('Gradings')
    serverRole = 'grades'
