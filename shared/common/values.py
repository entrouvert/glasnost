# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Common ValueHolders"""

__version__ = '$Revision$'[11:-2]


import things


class ValueHolder(things.BaseThing):
    kind = None
    kind_kind_label = N_('Kind')
    kind_kindName = 'Kind'

    value = None
    value_kind_label = N_('Value')
    value_kindName = '__getter__'

    def __eq__(self, other):
        return isinstance(other, ValueHolder) and self.kind == other.kind \
               and self.value == other.value

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return '<%s: %s, %s>' % (
                self.__class__.__name__, self.kind, self.value)

    def __repr__(self):
        return '<%s: %s, %s>' % (
                self.__class__.__name__, self.kind, self.value)

    def getImportSlotNames(self, parentSlot = None):
        names = things.BaseThing.getImportSlotNames(
                self, parentSlot = parentSlot)
        names = names[:]
        if 'kind' in names:
            # The slot 'kind' is already imported by the method
            # importFromXmlRpc.
            names.remove('kind')
        return names

    def importFromXmlRpc(self, dataImport, parentSlot = None):
        # The slot 'kind' must be imported first, because it is needed to
        # import value.
        if dataImport.has_key('__noneSlotNames__'):
            noneSlotNames = dataImport['__noneSlotNames__']
        else:
            noneSlotNames = []
        for slotName in ['kind']:
            if dataImport.has_key(slotName):
                exportedValue = dataImport[slotName]
            elif slotName in noneSlotNames:
                exportedValue = None
            else:
                continue
            slot = self.getSlot(slotName, parentSlot = parentSlot)
            kind = slot.getKind()
            if not kind.isImportable():
                continue
            slot.setValue(kind.importValueFromXmlRpc(slot, exportedValue))
        return things.BaseThing.importFromXmlRpc(
                self, dataImport, parentSlot = parentSlot)

    def value_kindGetter(self, slot):
        return self.kind

    def init(self, kind, value):
        assert kind.isPractical
        self.kind = kind
        self.value = value
        return self
things.register(ValueHolder)

