# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost PageName Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminPageNamesCommon(AdminCommon):
    serverRole = 'pagenames'


class PageNameCommon(ObjectCommon):
    language_kindName = None

    mappedId = None
    mappedId_kind_balloonHelp = N_(
        'Select the [object->objects] whose URL is named by this [alias->aliases-help].'\
        ' Click on "[Others->buttons-others]" if it is not proposed in the list.')
    mappedId_kind_isRequired = 1
    mappedId_kindName = 'Id'

    name = None
    name_kind_balloonHelp = N_(
        'Choose a meaningful name for this [alias->aliases-help].')
    name_kind_isRequired = 1
    name_kindName = 'Alias'

    serverRole = 'pagenames'

    def canCache(self):
        return 1

    def getLabel(self):
        label = self.name
        if label is None:
            return ''
        return label

    def getLabelLanguage(self):
        return ''

    def getLanguage(self):
        return ''

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += ['name', 'mappedId']
        return slotNames


class PageNamesCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminPageNames'
    newObjectNameCapitalized = N_('New Alias')
    objectClassName = 'PageName'
    objectName = N_('alias')
    objectNameCapitalized = N_('Alias')
    objectsName = N_('aliases')
    objectsNameCapitalized = N_('Aliases')
    serverRole = 'pagenames'
