# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost System Files Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


class AdminSystemFilesCommon(AdminCommon):
    serverRole = 'systemfiles'


class SystemFileCommon(ObjectCommon):
    creationTime = None
    creationTime_kindName = 'CreationTime'

    data = None
    data_kind_importExport = 'from-server-only'
    data_kind_useCustomStorage = 1
    data_kindName = 'Data'

    filePath = None
    filePath_kind_isRequired = 1
    filePath_kindName = 'FilePath'

    modificationTime = None
    modificationTime_kindName = 'ModificationTime'

    readersSet = None
    readersSet_kindName = 'ReadersSet'

    serverRole = 'systemfiles'

    size = None
    size_kind_importExport = 'from-server-only'
    size_kind_useCustomStorage = 1
    size_kindName = 'Integer'

    title = None
    title_kind_isRequired = 1
    title_kindName = 'String'

    writersSet = None
    writersSet_kindName = 'WritersSet'

    def getLabel(self):
        label = self.getTitle()
        if label is None:
            return ''
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += [
            'title', 'filePath', 'data', 'size', 'creationTime',
            'modificationTime', 'writersSet', 'readersSet']
        return slotNames

    def getTitle(self):
        return self.title


class SystemFilesCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminSystemFiles'
    newObjectNameCapitalized = N_('New System File')
    objectClassName = 'SystemFile'
    objectName = N_('system file')
    objectNameCapitalized = N_('System File')
    objectsName = N_('system files')
    objectsNameCapitalized = N_('System Files')
    serverRole = 'systemfiles'
