# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@entrouvert.com>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost X509 Accounts Common Models"""

__version__ = '$Revision$'[11:-2]


import ObjectsCommon as objects


class AdminX509Accounts(objects.AdminCommon):
    serverRole = 'x509accounts'


class X509Account(objects.ObjectCommon):
    authenticationMethod = None
    class authenticationMethod_kindClass:
        _kindName = 'Choice'
        isRequired = 1
        label = N_('Authentication Method')
        labels = {
                'smartcardPki': N_('Smartcard Certificate'),
                'softwarePki': N_('Software Certificate'),
                }
        values = [
                'smartcardPki',
                'softwarePki',
                ]
    
    identityId = None
    class identityId_kindClass:
        _kindName = 'Id'
        isRequired = 1
        label = N_('Identity')
        serverRoles = ['identities']

    language_kindName = None

    serial = None
    class serial_kindClass:
        _kindName = 'String'
        isRequired = 1
        isTranslatable = 0
        label = N_('Serial')
        textMaxLength = 40
        widget_size = 15
        
    serverRole = 'x509accounts'

    def getLabel(self):
        label = self.serial
        if label is None:
            return ''
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = objects.ObjectCommon.getOrderedLayoutSlotNames(
                self, parentSlot = parentSlot)
        slotNames += ['authenticationMethod', 'serial', 'identityId']
        return slotNames


class X509AccountsCommonMixin(objects.ObjectsCommonMixin):
    adminClassName = 'AdminX509Accounts'
    newObjectNameCapitalized = N_('New X509v3 Account')
    objectClassName = 'X509Account'
    objectName = N_('X509v3 account')
    objectNameCapitalized = N_('X509v3 Account')
    objectsName = N_('X509v3 accounts')
    objectsNameCapitalized = N_('X509v3 Accounts')
    serverRole = 'x509accounts'

