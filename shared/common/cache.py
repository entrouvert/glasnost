# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Cache Common"""

__version__ = '$Revision$'[11:-2]


import time

import faults
from glasnost.proxy.CacheProxy import isValueStillValid
from glasnost.proxy.DispatcherProxy import MultiCall, callServer, getApplicationToken


class Cache:
    cachedValues = None
    disabled = 0
            
    def __getitem__(self, key):
        return self.cachedValues[key][2]

    def has_key(self, key):
        return self.cachedValues.has_key(key)

    def __init__(self):
        self.cachedValues = {}

    def __setitem__(self, key, value):
        if key is None:
            return
        self.cachedValues[key] = (time.time(), time.time(), value)

    def checkCachedValues(self):
        if self.disabled:
            self.cachedValues = {}
            return

        try:
            multiCall = MultiCall()
            for k, v in self.cachedValues.items():
                isValueStillValid(k, v[0], multiCall = multiCall)

            for r in multiCall.call():
                k = r()
                if k:
                    del self.cachedValues[k]
        except faults.UnknownServerId:
            self.disabled = 1
            self.cachedValues = {}


class TemporaryCache(Cache):
    def checkCachedValues(self):
        self.cachedValues = {}

cache = Cache()
temporaryCache = TemporaryCache()

