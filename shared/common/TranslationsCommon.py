# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Translations Common Models"""

__version__ = '$Revision$'[11:-2]


from ObjectsCommon import AdminWithoutWritersCommon, ObjectCommon, ObjectsCommonMixin
import translation


class AdminTranslationsCommon(AdminWithoutWritersCommon):
    serverRole = 'translations'

    translatorsSets = None
    translatorsSets_kindName = 'TranslatorsSets'


class LocalizationCommon(ObjectCommon):
    destinationLanguage = None
    destinationLanguage_kind_isRequired = 1
    destinationLanguage_kind_values = translation.languageKeys
    destinationLanguage_kindName = 'Choice'

    destinationString = None
    #destinationString_kind_isRequired = 1
    destinationString_kindName = 'String'
    
    id_kindName = None

    isFuzzy = 0
    isFuzzy_kind_isRequired = 1
    isFuzzy_kindName = 'Boolean'

    isTranslatable = 0
    isTranslatable_kind_isRequired = 1
    isTranslatable_kindName = 'Boolean'

    language_kindName = None

    serverRole = 'translations'

    similarString = None
    similarString_kindName = 'String'

    sourceIds = None
    sourceIds_kind_itemKind_valueName = 'Id'
    sourceIds_kindName = 'Sequence'

    sourceLanguage = None
    sourceLanguage_kind_isRequired = 1
    sourceLanguage_kind_values = translation.languageKeys
    sourceLanguage_kindName = 'Choice'

    sourceString = None
    sourceString_kind_isRequired = 1
    sourceString_kindName = 'String'

    sourceStringDigest = None
    sourceStringDigest_kindName = 'String'

    translatorsSet = None
    translatorsSet_kind_minCount = 0
    translatorsSet_kindName = 'UsersSet'


    def getKey(self):
        return self.sourceLanguage + self.destinationLanguage

    def getLabel(self):
        label = self.getTitle()
        if label is None:
            return ''
        return label

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += [
            'sourceIds', 'sourceLanguage', 'sourceString', 'isTranslatable',
            'destinationLanguage', 'similarString', 'destinationString',
            'isFuzzy', 'translatorsSet']
        return slotNames

    def getTitle(self):
        title = self.sourceString
        if len(title) > 80:
            title = title[:60] + '...'
        return title


class TranslationsCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminTranslations'
    newObjectNameCapitalized = N_('New Translation')
    objectName = N_('translation')
    objectNameCapitalized = N_('Translation')
    objectsName = N_('translations')
    objectsNameCapitalized = N_('Translations')
    serverRole = 'translations'
    useObjectIds = 0

