# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Appointments Common Models"""

__version__ = '$Revision$'[11:-2]


import calendar
import time

from ObjectsCommon import AdminCommon, ObjectCommon, ObjectsCommonMixin


ONE_MINUTE = 60
ONE_HOUR = ONE_MINUTE * 60
ONE_DAY = ONE_HOUR * 24
ONE_WEEK = ONE_DAY * 7
ONE_MONTH = ONE_DAY * 28
ONE_YEAR = ONE_DAY * 365

class AdminAppointmentsCommon(AdminCommon):
    bodyFormat = 'spip'
    bodyFormat_kind_balloonHelp = N_(
            "The text format to use for the appointments. "\
            "[SPIP->syntax-spip] or [reStructredText->syntax-rst] syntax reference.")
    bodyFormat_kind_isRequired = 1
    bodyFormat_kind_label = N_('Text Format')
    bodyFormat_kind_labels = {
        'html': N_('HTML'),
        'spip': N_('SPIP'),
        'rst': N_('reStructured Text'),
        }
    bodyFormat_kind_values = [
        'html',
        'spip',
        'rst',
        ]
    bodyFormat_kindName = 'Choice'
    
    categoriesGroupId = None
    categoriesGroupId_kind_balloonHelp = N_(
        'Select the group that holds categories for the appointments.')
    categoriesGroupId_kind_serverRoles = ['groups']
    categoriesGroupId_kindName = 'Id'

    serverRole = 'appointments'


class AppointmentCommon(ObjectCommon):
    body = None
    class body_kindClass:
        _kindName = 'String'
        balloonHelp = N_('Enter the text of this appointment.') 
        isTranslatable = 1
        label = N_('Text')
        widget_colSpan = 2
        widget_preview = 1
        widgetName = 'TextArea'

        def getTextFormat(self, slot):
            from glasnost.proxy.tools import getProxyForServerRole
            proxy = getProxyForServerRole('appointments')
            return proxy.getAdmin().bodyFormat

    creationTime = None
    creationTime_kindName = 'CreationTime'

    end = None
    end_kind_stateInViewMode = 'read-only/hidden-if-empty'
    end_kindName = 'Time'

    participantsSet = None
    participantsSet_kindName = 'UsersSet'

    readersSet = None
    readersSet_kindName = 'ReadersSet'

    serverRole = 'appointments'

    start = None
    start_kind_balloonHelp = N_('Enter the day of the appointment.')
    start_kind_isRequired = 1
    start_kindName = 'Time'

    title = None
    title_kind_balloonHelp = N_('Enter a title for this appointment.')
    title_kind_isRequired = 1
    title_kindName = 'String'

    writersSet = None
    writersSet_kindName = 'WritersSet'

    def canCache(self):
        return 1

    def getCategory(self):
        result = self.category
        if result is None:
            result = ''
        return result

    def getLabel(self, withDate = 0, withHour = 0):
        title = self.getTitle()
        if title is None:
            return ''
        dateTime = self.getDateTime(withDate, withHour)
        if dateTime:
            return '%s - %s' % (dateTime, title)
        return title

    def getDateTime(self, withDate = 0, withHour = 0):
        hourTime = ''
        if withHour:
            hourTime = self.getBeginningHourAndMinute()

        date = ''
        if withDate:
            date = time.strftime('%d/%m/%Y', time.localtime(self.start))

        if not date and not hourTime:
            return ''
        return ' '.join([date, hourTime])

    def getTitle(self):
        return self.title

    def getBeginningHourAndMinute(self):
        """Returns a string with the start time of the appointment (or an empty
           string if no time set)"""
        hour = '%02d:%02d' % time.localtime(self.start)[3:5]
        if hour == '00:00' and not self.end:
            return ''
        if self.end:
            hourEnd = '%02d:%02d' % time.localtime(self.end)[3:5]
            if hour == '00:00' and self.end - self.start < 86400 and \
                    hourEnd == '23:59':
                return ''
        return hour

    def getEndHourAndMinute(self):
        if not self.end:
            return None
        hour = '%02d:%02d' % time.localtime(self.end)[3:5]
        return hour

    def getOrderedLayoutSlotNames(self, parentSlot = None):
        slotNames = ObjectCommon.getOrderedLayoutSlotNames(
            self, parentSlot = parentSlot)
        slotNames += [
            'title',
            'start',
            'end',
            'body',
            'participantsSet',
            'creationTime',
            'writersSet',
            'readersSet',
            ]
        return slotNames


class AppointmentsCommonMixin(ObjectsCommonMixin):
    adminClassName = 'AdminAppointments'
    objectClassName = 'Appointment'
    newObjectNameCapitalized = N_('New Appointment')
    objectName = N_('appointment')
    objectNameCapitalized = N_('Appointment')
    objectsName = N_('appointments')
    objectsNameCapitalized = N_('Appointments')
    serverRole = 'appointments'

