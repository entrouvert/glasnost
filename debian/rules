#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This is the debhelper compatability version to use.
export DH_COMPAT=3

configure: configure-stamp
configure-stamp:
	dh_testdir
	# Add here commands to configure the package.

	touch configure-stamp

build: build-stamp

build-stamp: configure-stamp 
	dh_testdir

	# Add here commands to compile the package.
	
	./make-system
	$(MAKE) mo

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp

	# Add here commands to clean up after the build process.
	-$(MAKE) distclean
	-rm config

	dh_clean

BASETEMPLATES=default april cuisine entrouvert2 glasnost2 lightbulb plane september
TEMPLATES=`ls $(CURDIR)/templates/ | grep -v CVS`

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	# Add here commands to install the package into debian/glasnost.
	rm -f config
	$(MAKE) install DESTDIR=$(CURDIR)/debian/glasnost PREFIX=/usr GLASNOST=glasnost TEMPLATES='$(TEMPLATES)' PYTHON_VERSION=2.3 SERVER_USER=glasnost-daemon PORT=8000
		# note: python version is only specified to get to pygettext.py
	#$(MAKE) install-templates DESTDIR=$(CURDIR)/debian/glasnost-web-extra-templates PREFIX=/usr GLASNOST=glasnost 'TEMPLATES=$(TEMPLATES)'
	#for F in $(BASETEMPLATES); \
	do \
		echo $$F; \
		rm -rf $(CURDIR)/debian/glasnost-web-extra-templates/var/lib/glasnost/templates/$$F; \
	done
	cp config $(CURDIR)/debian/glasnost/etc/glasnost/
	rm $(CURDIR)/debian/glasnost/usr/share/glasnost/config
	find $(CURDIR)/debian/glasnost/ -name .htaccess -exec rm {} \;
	# we'll recreate them debian compliant later
	find $(CURDIR)/debian/glasnost/ -type l -exec rm -f {} \;
	find $(CURDIR)/debian/glasnost/ -name '*.pyc' -exec rm -f {} \;
	find $(CURDIR)/debian/glasnost* -name .cvsignore -exec rm -f {} \;
	find $(CURDIR)/debian/glasnost* -name '.*' -type f -exec rm -r {} \;
	-find $(CURDIR)/debian/glasnost* -name CVS -type d -exec rm -rf {} \; 2> /dev/null
	# this is ugly but python 2.1 didn't have xmlrpclib.py
	-mkdir -p $(CURDIR)/debian/glasnost/usr/lib/python2.1/site-packages/
	cp /usr/lib/python2.3/xmlrpclib.py $(CURDIR)/debian/glasnost/usr/lib/python2.1/site-packages/

	cp -R $(CURDIR)/debian/glasnost/* $(CURDIR)/debian/glasnost-gtk/
	rm -rf $(CURDIR)/debian/glasnost/usr/share/glasnost/glasnost/gtk/ \
		$(CURDIR)/debian/glasnost/usr/bin/glasnost-gtk \
		$(CURDIR)/debian/glasnost/usr/lib/glasnost/web/apache-errors/
	
	rm -rf $(CURDIR)/debian/glasnost-gtk/usr/lib/glasnost/glasnost/web/ \
		$(CURDIR)/debian/glasnost-gtk/usr/lib/glasnost/glasnost/server/ \
		$(CURDIR)/debian/glasnost-gtk/usr/lib/glasnost/glasnost/gcurses/ \
		$(CURDIR)/debian/glasnost-gtk/usr/lib/glasnost/servers \
		$(CURDIR)/debian/glasnost-gtk/usr/lib/glasnost/web \
		$(CURDIR)/debian/glasnost-gtk/usr/share/glasnost/glasnost-ctl.py \
		$(CURDIR)/debian/glasnost-gtk/usr/sbin \
		$(CURDIR)/debian/glasnost-gtk/var
	-mkdir -p $(CURDIR)/debian/glasnost-gtk/usr/lib/site-python/
	cp glasnost-gtk/gtkexcepthook.py $(CURDIR)/debian/glasnost-gtk/usr/lib/site-python/

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_movefiles --sourcedir=./debian/glasnost/
	-find ./debian/glasnost/ -type d -exec rmdir -p {} \; 2> /dev/null
#	dh_installdebconf	
	dh_installdocs
	dh_installexamples
	dh_installlogrotate
	dh_installinit --init-script=glasnost
	dh_installcron
	dh_installman
	dh_installinfo
	dh_installchangelogs 

	dh_strip
	dh_compress
	dh_fixperms
#	dh_makeshlibs
	dh_installdeb
#	dh_perl
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary-arch: build install
# We have nothing to do by default.
binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
