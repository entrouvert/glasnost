# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Glasnost System Upload Files Builder"""


__version__ = '$Revision$'[11:-2]


import os
import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-system'
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.faults as faults

import glasnost.proxy.UploadFilesProxy as UploadFilesProxy
import glasnost.proxy.PageNamesProxy as PageNamesProxy
from glasnost.proxy.tools import getProxyForServerRole


uploadfilesProxy = getProxyForServerRole('uploadfiles')
pageNamesProxy = getProxyForServerRole('pagenames')


class BuildCase01_uploadfilesFromFiles(unittest.TestCase):
    def build01_uploadfilesFromFiles(self):
        """Create uploadfiles from files under system/uploadfiles/"""
        files = [x for x in os.listdir('uploadfiles/') if \
                    x != 'CVS' and x[0] != '.' and \
                    x[-1] != '~' and not x.endswith('.meta')]
        pageNames = {}
        for f in files:
            try:
                fd = open('uploadfiles/%s.meta' % f)
            except IOError:
                continue
            uploadfile = UploadFilesProxy.UploadFile()
            slotNames = uploadfile.getSlotNames()
            while 1:
                line = fd.readline()
                if line[0] != '#':
                    break
                key, value = [x.strip() for x in line[1:].split(':', 1)]
                if not key in slotNames:
                    raise 'Unknown slot: %s (in uploadfiles/%s)' % (key, f)
                setattr(uploadfile, key, value)
            uploadfile.dataFileName = f
            fd = open('uploadfiles/%s' % f)
            uploadfile.data = fd.read()
            uploadfile.readersSet = ['glasnost://localhost/groups/1']
            uploadfileId = uploadfilesProxy.addObject(uploadfile)
            pageNames[f] = uploadfileId

        #for f, uploadfileId in pageNames.items():
        #    pageName = PageNamesProxy.PageName()
        #    pageName.name = f
        #    pageName.mappedId = uploadfileId
        #    pageNamesProxy.addObject(pageName)


buildLoader = unittest.TestLoader()
buildLoader.testMethodPrefix = 'build'
buildSuite = buildLoader.loadTestsFromModule(sys.modules[__name__])

