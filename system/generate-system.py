#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Glasnost System Generation Script."""


__version__ = '$Revision$'[11:-2]

import sys
import time
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-system'
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.applications as applications
import glasnost.common.context as context

from glasnost.proxy.tools import getProxyForServerRole


class Application(applications.Application):
    applicationName = 'SystemGenerator'
    applicationRole = 'systemgenerator'

    def createUser(self):
        import glasnost.proxy.IdentitiesProxy as proxyIdentities
        import glasnost.proxy.PasswordAccountsProxy as proxyPasswordAccounts
        identitiesProxy = getProxyForServerRole('identities')
        passwordAccountsProxy = getProxyForServerRole('passwordaccounts')

        admin = passwordAccountsProxy.getAdmin()
        admin.userCanChoosePassword = 1
        passwordAccountsProxy.modifyAdmin(admin)

        user = proxyIdentities.Identity()
        userId = identitiesProxy.addObject(user)
        context.setVar('userId', userId)

        account = proxyPasswordAccounts.PasswordAccount()
        account.login = context.getVar('userLogin')
        account.password = account.login
        account.identityId = userId
        accountId = passwordAccountsProxy.addObject(account)

    def generateSystem(self):
        testsRunner = unittest.TextTestRunner()

        print 'Generating groups...'
        import groups
        testsRunner.run(groups.buildSuite)

##         print 'Generating dataflows...'
##         import dataflows
##         testsRunner.run(dataflows.buildSuite)

        print 'Generating articles...'
        import articles
        testsRunner.run(articles.buildSuite)

        print 'Generating uploadfiles...'
        import uploadfiles
        testsRunner.run(uploadfiles.buildSuite)


    def getUserPassword(self):
        return context.getVar('userLogin')

    def login(self):
        passwordAccountsProxy = getProxyForServerRole('passwordaccounts')
        userToken, authenticationMethod \
                = passwordAccountsProxy.checkObjectAuthentication(
                        context.getVar('userLogin'),
                        context.getVar('userPassword'))
        context.setVar('userToken', userToken)

    def logout(self):
        identitiesProxy = getProxyForServerRole('identities')
        identitiesProxy.deleteUserToken()
        context.delVar('userToken')

    def main(self):
        self.launch()

        context.push(
                _level = 'main',
                startTime = time.localtime(),
                userId = None,
                userLogin = 'root',
                userToken = None,
                )

        # Create the virtual host, because none exists.
        virtualHostsProxy = getProxyForServerRole('virtualhosts')
        virtualHost = virtualHostsProxy.getObjectByHostName('localhost')
        virtualHost.profiles.append('translations')
        virtualHostsProxy.modifyObject(virtualHost)

        if '--no-user' in sys.argv:
            context.setVar('userId', 'glasnost://localhost/identities/1')
            context.setVar('startTime', 0)
        else:
            self.createUser()
        context.setVar('userPassword', self.getUserPassword())
        self.login()
        self.generateSystem()
        self.logout()
        context.pull(_level = 'main')


if __name__ == '__main__':
    application = Application()
    application.main()

