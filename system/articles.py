# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Glasnost System Articles Builder"""


__version__ = '$Revision$'[11:-2]


import md5
import os
import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-system'
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.faults as faults

import glasnost.proxy.ArticlesProxy as ArticlesProxy
import glasnost.proxy.PageNamesProxy as PageNamesProxy
from glasnost.proxy.TranslationsProxy import Localization
from glasnost.proxy.tools import getProxyForServerRole


articlesProxy = getProxyForServerRole('articles')
pageNamesProxy = getProxyForServerRole('pagenames')
translationsProxy = getProxyForServerRole('translations')


class BuildCase01_articlesFromFiles(unittest.TestCase):
    def build01_articlesFromFiles(self):
        """Create articles from files under system/articles/"""
        files = [x for x in os.listdir('articles/') if \
                    x != 'CVS' and \
                    x[0] != '.' and \
                    x[-1] != '~' and \
                    not '.' in x ]
        pageNames = {}
        for f in files:
            fd = open('articles/%s' % f)
            article = ArticlesProxy.Article()
            slotNames = article.getSlotNames()
            while 1:
                line = fd.readline()
                if line[0] != '#':
                    break
                key, value = [x.strip() for x in line[1:].split(':', 1)]
                if not key in slotNames:
                    raise 'Unknown slot: %s (in articles/%s)' % (key, f)
                setattr(article, key, value)
            article.body = fd.read()
            article.readersSet = ['glasnost://localhost/groups/1']
            articleId = articlesProxy.addObject(article)
            pageNames[f] = articleId

        for f, articleId in pageNames.items():
            pageName = PageNamesProxy.PageName()
            pageName.name = f
            pageName.mappedId = articleId
            pageNamesProxy.addObject(pageName)
        
    def build02_translations(self):
        files = [x for x in os.listdir('articles/') if \
                    x != 'CVS' and \
                    x[0] != '.' and \
                    x[-1] != '~' and \
                    not '.' in x ]
        pageNames = {}
        for f in files:
            translationFiles = [x for x in os.listdir('articles/') if \
                    x.startswith('%s.' % f) and
                    len(x) == len(f)+3 ]
            if not translationFiles:
                continue
            articleId = pageNamesProxy.getIdByName(f)
            article = articlesProxy.getObject(articleId)

            titleStringDigest = md5.new(
                    article.title.replace('\r\n', '\n')).hexdigest()
            bodyStringDigest = md5.new(
                    article.body.replace('\r\n', '\n')).hexdigest()
            sourceLanguage = article.language

            translationsProxy.getTranslation(article.title, article.id,
                    'self.title', sourceLanguage, ['--'])
            translationsProxy.getTranslation(article.body, article.id,
                    'self.body', sourceLanguage, ['--'])

            for tFile in translationFiles:
                fd = open('articles/' + tFile)
                title = fd.readline()[2:].strip()
                fd.readline()
                body = fd.read()
                fd.close()

                localization = Localization()
                localization.sourceStringDigest = titleStringDigest
                localization.destinationLanguage = tFile[-2:]
                localization.destinationString = title
                localization.isFuzzy = 0
                localization.sourceString = article.title
                localization.sourceLanguage = sourceLanguage
                localization.isTranslatable = 1

                translationsProxy.modifyLocalization(localization)

                localization = Localization()
                localization.sourceStringDigest = bodyStringDigest
                localization.destinationLanguage = tFile[-2:]
                localization.destinationString = body
                localization.isFuzzy = 0
                localization.sourceString = article.body
                localization.sourceLanguage = sourceLanguage
                localization.isTranslatable = 1

                translationsProxy.modifyLocalization(localization)


buildLoader = unittest.TestLoader()
buildLoader.testMethodPrefix = 'build'
buildSuite = buildLoader.loadTestsFromModule(sys.modules[__name__])

