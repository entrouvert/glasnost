Gestion de contenu
==================

.. contents::
   :depth: 1
   :local:

Le contenu, les permissions, et les traductions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le contenu : Rubriques, Articles et Fichiers
--------------------------------------------

Les Articles sont les �l�ments de contenu. Ils sont rang�s dans des
Rubriques. Chaque Article peut figurer dans une ou plusieurs Rubriques.

Les Rubriques ont :

* Z�ro ou plusieurs � �l�ments � : une Rubrique peut contenir
  plusieurs Articles, plusieurs sous-rubriques, des �lections, etc.

* Z�ro ou un Article de � Contenu �. L'Article qui se trouve dans le
  � Contenu � de la Rubrique est la "page d'accueil" de cette
  Rubrique.

Les � R�glages � des Rubriques permettent de d�finir une Rubrique
principale. Cette Rubrique principale est la Rubrique racine, celle
qui se trouve au sommet de l'arborescence du site.

Les Rubriques affich�es par Glasnost dans le (premier niveau du) menu de
navigation qu'il g�n�re sont les Rubriques membres de la Rubrique principale.

Cr�er un Alias pour un **objet**, c'est lui associer un nouvel
**identifiant** o� le num�ro de l'**objet** est remplac� par un nom.
Cela permet d'avoir des **identifiants** (et des URIs) plus
explicites.

Avant d'inclure une image dans un Article, il faut la charger dans
Glasnost. Ceci consiste � cr�er un **objet** Fichier.

La gestion des droits
---------------------

Exemple : 

* Les �diteurs des Articles (d�finis dans les � R�glages �)
  sont les membres d'un Groupe "�diteurs".

* L'article "Un titre d'Article" a pour �diteurs les membres du Groupe
  "D�veloppeurs Glasnost".

Les membres de "D�veloppeurs Glasnost" peuvent supprimer "Un titre
d'Article", mais les membres de "�diteurs" ne le peuvent pas.


La traduction du contenu
------------------------

Lorsque le Traduction d'une langue � une autre est activ�e, le groupe
de traducteurs correspondant peut utiliser l'interface de Glasnost
pour :

* Traduire les Articles.

* Traduire toutes les cha�nes de caract�res non traduites. Une cha�ne
  de caract�res est un mot ou un groupe de mots : nom de Rubrique,
  titre d'Article, etc...


.. raw:: latex

   \newpage

.. include:: content-management/recettes.txt

.. raw:: latex

   \newpage


Les syntaxes SPIP et reStructuredText
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: content-management/syntaxe-spip.txt

.. include:: content-management/syntaxe-reStructuredText



