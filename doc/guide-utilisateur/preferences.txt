Authentification, session et pr�f�rences
========================================

.. contents::
   :depth: 1
   :local:

Le contenu public d'un site Glasnost est �videmment consultable par
n'importe quel internaute.

Par contre, pour lire le contenu priv� du site, ou pour y ajouter
ou modifier du contenu, il faut s'authentifier.

Authentification et d�connexion : session
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Avec tous les mod�les de pr�sentaton de Glasnost, un bouton
� S'identifier � se trouve sur chaque page.

Une session d'un utilisateur commence au moment o� il s'identifie, et elle
s'arr�te quand il se d�logue (en cliquant sur le bouton � Sortir �). [#]_

.. [#] Pour les utilisateurs non identifi�s, les sessions commencent
   au moment o� ils �ditent leurs Pr�f�rences.

Par d�faut, les sessions se terminent automatiquement apr�s 2 heures
d'inactivit� d'un utilisateur identifi�.

Modifier ses informations personnelles
--------------------------------------

Pour modifier ses information personnelles (son adresse �lectronique),
il faut �diter son **objet** Personne. 


Les pr�f�rences
~~~~~~~~~~~~~~~

.. raw:: latex

   \begin{figure}[H]
      \begin{center}
         \includegraphics[totalheight=0.27\textheight]{guide-utilisateur/preferences/preferences.png}
      \end{center}
      \caption{\footnotesize Les Pr�f�rences.}
   \end{figure}

Contrairement aux autres **objets** Glasnost, les Pr�f�rences ne sont
pas accessibles dans le menu des  **objets** mais dans le bouton
� Pr�f�rences �.

* La langue choisie est celle qui sera attribu�e par d�faut aux
  Articles au moment de leur cr�ation par un utilisateur.
  C'est surtout la langue d'affichage du site (si le site est traduit
  dans cette langue).
  Par d�faut, c'est la langue des pr�f�rences de votre navigateur.
  
* Le cryptage des courriels est recommand� pour �viter l'envoi des
  mots de passe en clair dans les courriels envoy�s par le **syst�me**. 

* La correction orthographique n'est pas activ�e par d�faut.

