Le vote dans Glasnost
=====================

Pour en savoir plus sur les m�thodes �lectorales, 

Du sondage aux �lections en passant par la prise de d�cision
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un **objet** �lection peut �tre une �lection, un vote ou un sondage.
Voici le formulaire d'�dition d'une �lection :

.. raw:: latex

   \begin{figure}[H]
      \begin{center}
         \includegraphics[totalheight=0.7\textheight]{guide-utilisateur/vote/vote.png}
      \end{center}
      \caption{\footnotesize Le calendrier de Glasnost.}
   \end{figure}


M�thodes �lectorales et types de votes
--------------------------------------

Glasnost propose deux m�thodes �lectorale :

- Condorcet
- Meilleure moyenne

et quatre types de votes :

- Assentiment : Voter consiste � choisir un ou plusieurs candidats.

- Rang : Voter consiste � classer les candidats par ordre de pr�f�rence. 1 pour le
  premier, 2 pour le(s) second(s), etc.
  
  Il est possible de ne voter que pour son candidat pr�f�r�. Ce candidat aura
  alors le rang 1, tous les autres auront le rang 2.
  
- Pourcentage : Voter consiste � attribuer un pourcentage � chaque candidat.
  La somme des pourcentages de tous les candidats doit �tre �gale �
  100.
  
- Note : Voter consiste � attribuer une note entre 0 et 100 � chaque
  candidat. 

Il est aussi possible de voter "Vote blanc ou nul".

Les votants peuvent attacher un commentaire � leur vote.

**Attention !** La m�thode �lectorale Condorcet donne des r�sultats
faux avec un type de vote diff�rent de Rang ; et la m�thode "meilleure moyenne"
avec un vote "Rang" est �quivalente � la m�thode Condorcet.

Il y a donc quatre types de votes :

* Vote Condorcet [#]_ : Condorcet + Rang
* Vote par assentiment [#]_ : Meilleure moyenne + Assentiment
* Meilleur moyenne + Pourcentage
* Meilleur moyenne + Note

.. [#] : http://electionmethods.org/fr/Condorcet.htm

.. [#] : http://electionmethods.org/fr/approved.htm

Les deux derniers sont �quivalents : attribuer une note revient �
attribuer un pourcentage.


Les types de bulletins
----------------------

- Bulletin public
- Bulletin priv�
- Bulletin public ou priv� au choix du votant


La dur�e du vote
----------------

Les r�sultats d'un vote permanent sont visibles d�s le d�but de
l'�lection.

Le r�sultat des �lections non permanentes est connu lorsque l'�lection
est �dit�e, et pass�e � "termin�e" au lieu de "en cours".

La Date de fin de l'�lection n'est pas la date � partir de laquelle
les r�sultats d'une �lection seront visible, mais la date � partir de
laquelle il n'est plus possible de voter.


Les candidats
-------------

Les candidats peuvent �tre n'importe quel **objet**. Les **objets**
Atomes servent � d�finir le nom des candidats qui ne sont pas un
**objet** existant d�j� dans Glasnost.

La pond�ration
--------------

Un vote pond�r� est un vote dans lequel les voix des votants ont un
poids diff�rent : le r�sultat d'une �lection peut servir �
pond�rer les voix des �lecteurs. Cette fonctionnalit� est encore en
cours de d�veloppement. Elle utilise l'**objet** Classement.

Nombre de vainqueurs, Groupe des vainqueurs
-------------------------------------------

Le nombre des vainqueurs n'influence que l'affichage des r�sultats :
le vainqueur s'il est �gal � 1, les deux premiers s'il est �gal � 2,
etc.

Les membres d'un Groupe peuvent �tre d�finis comme �tant les
vainqueurs d'une �lection.


Exemple d'un vote Condorcet
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les candidats :

* Bulletins publics pour toutes les �lections
* Bulletins priv�s pour toutes les �lections
* Bulletins priv�s pour les �lections de personnes, publics sinon
* Bulletins publics ou priv�s, au choix du votant, pour toutes les �lections
* Nature des bulletins choisie au cas par cas, � chaque �lection
* Refus de cette �lection

.. raw:: latex

   \begin{figure}[H]
      \begin{center}
         \includegraphics[totalheight=0.3\textheight]{guide-utilisateur/vote/bulletin1.png}
      \end{center}
      \caption{\footnotesize Un vote Condorcet.}
   \end{figure}

.. raw:: latex

   \begin{figure}[H]
      \begin{center}
         \includegraphics[totalheight=0.3\textheight]{guide-utilisateur/vote/bulletin2.png}
      \end{center}
      \caption{\footnotesize Le d�pouillement d'un vote Condorcet :
	  comparaison des candidats deux � deux.}
   \end{figure}

.. raw:: latex

   \begin{figure}[H]
      \begin{center}
         \includegraphics[totalheight=0.4\textheight]{guide-utilisateur/vote/bulletin3.png}
      \end{center}
      \caption{\footnotesize Le r�sultat d'un vote Condorcet.}
   \end{figure}


