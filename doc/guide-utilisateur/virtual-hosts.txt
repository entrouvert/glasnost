Les h�tes virtuels
==================

Pour cr�er un h�te virtuel, cliquez sur H�tes Virtuels puis sur le
bouton Nouveau.

- choisissez la Langue de cet H�te

- donnez lui un Titre, le titre est un champ libre qui l'identifie
  dans la liste (aucun fonctionnalit� technique)

- Identifiant de contr�leur Glasnost, ceci est l'information
  essentielle si vous avez par exemple deux h�tes virtuels, le
  premier aura comme valeur glasnost://localhost/, admettons que
  vous cr�ez un nouvel h�te qui sera accessible par l'url
  https://glas.bar.org/, indiquez alors comme identifiant
  glasnost://glas.bar.org/

- Nom d'h�te du site web, comme indiquez dans le paragraphe
  suivant le nom d'h�te sera glas.bar.org

- Le Mod�le sera choisi suivant vos go�ts, le mod�le (template)
  change l'aspect du site, mais ne modifie en rien les
  fonctionnalit�s.

- Le champ �diteurs sera positionn� suivant vos choix de s�curit�

- Attention au champ Lecteurs il doit absolument �tre positionn�
  sur Tout le monde si votre site est public.

Il ne reste plus qu'� ajouter le nom de votre nouvel h�te dans
votre fichier ``/etc/hosts`` ou � votre DNS.


.. raw:: latex

   \begin{figure}[H]
      \begin{center}
         \includegraphics[totalheight=0.33\textheight]{guide-utilisateur/virtual-hosts/virtual-host1.png}
      \end{center}
      \caption{\footnotesize L'h�te virtuel glasnost.entrouvert.org.}
   \end{figure}

Les r�glages des H�tes virtuels sont ceux du premier h�te cr��. Les
autres **objets** H�tes virtuels ne sont pas �ditables par les Personnes des
autres H�tes virtuels.

Par exemple, sur le syst�me Glasnost qui h�berge le site de Glasnost,
le premier H�te virtuel cr�� �tait celui de www.entrouvert.org. C'est
pour cela que le syst�me a rajout� ``(chez <www.entrouvert.org>)`` au
nom du Groupe des �diteurs.


