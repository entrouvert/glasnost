# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Orphaned Articles Web Page"""

__version__ = '$Revision$'[11:-2]


from glasnost.web.tools import *


def index():
    dispatcherId = context.getVar('dispatcherId')

    articlesProxy = getProxyForServerRole('articles')
    articles = articlesProxy.getObjects()

    d = {}
    for id in articles.keys():
        d[id] = 0

    # Getting rubrics
    rubricsProxy = getProxyForServerRole('rubrics')
    rubrics = rubricsProxy.getObjects()

    # Getting pagenames
    pagenamesProxy = getProxyForServerRole('pagenames')
    pagenames = pagenamesProxy.getObjects()

    pagenamesD = {}
    for p in pagenames.values():
        pagenamesD[p.name] = p.mappedId
    pagenames = pagenamesD

    # Marking articles that are part of rubrics
    for r in rubrics.values():
        if r.contentId:
            d[r.contentId] = 1
        if not r.membersSet:
            continue
        for id in r.membersSet:
            d[id] = 1

    # Parsing every articles for links
    import re
    for article in articles.values():
        if article.format == 'spip':
            links = [x[10:].strip() for x in re.findall(r'->article +\d+',
                     article.body)]
            for l in links:
                id = '%s/articles/%s' % (dispatcherId, l)
                d[id] = 1
            links = [x[8:].strip() for x in re.findall(r'->alias +[\w\-_]+',
                    article.body)]
            for l in links:
                if not pagenames.has_key(l):
                    print 'Wrong pagename: %s' % l
                    continue
                d[pagenames[l]] = 1
        elif article.format == 'rst':
            links = [x[10:].strip() for x in re.findall(r'<article +\d+',
                    article.body)]
            for l in links:
                id = '%s/articles/%s' % (dispatcherId, l)
                d[id] = 1
            links = [x[8:].strip() for x in re.findall(r'<alias +[\w\-_]+',
                    article.body)]
            for l in links:
                if not pagenames.has_key(l):
                    # print 'Wrong pagename: %s' % l
                    continue
                d[pagenames[l]] = 1
            

    # Results
    layout = X.ul()
    for id in d.keys():
        if d[id]:
            continue
        layout += X.li(X.objectHypertextLabel(id))

    return writePageLayout(layout, _('Orphaned Articles'))

