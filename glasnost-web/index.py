# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Index Web Page"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.xhtmlgenerator as X

from glasnost.web.tools import *


def index():
    # If Glasnost has not been inited yet and there is an init script, then
    # redirect to this script.
##     virtualHostsWeb = getWebForServerRole('virtualhosts')
##     try:
##         virtualHostsCount = virtualHostsWeb.getObjectsCount()
##     except faults.UserAccessDenied:
##         pass
##     else:
##         if virtualHostsCount <= 1:
##             identitiesWeb = getWebForServerRole('identities')
##             try:
##                 identitiesCount = identitiesWeb.getObjectsCount()
##             except faults.UserAccessDenied:
##                 pass
##             else:
##                 if identitiesCount == 0:
##                     pageNamesWeb = getWebForServerRole('pagenames')
##                     newVirtualHostWizardId = pageNamesWeb.getIdByName(
##                             'newVirtualHost')
##                     if newVirtualHostWizardId:
##                         newVirtualHostWizardWeb = getWeb(
##                                 newVirtualHostWizardId)
##                         if newVirtualHostWizardWeb.hasObject(
##                                 newVirtualHostWizardId):
##                             return redirect(X.aliasUrl('newVirtualHost'))
                
    knownRoles = context.getVar('knownRoles')
    articlesWeb = getWebForServerRole('articles')
    rubricsWeb = getWebForServerRole('rubrics')
    brevesWeb = getWebForServerRole('breves')
    electionsWeb = getWebForServerRole('elections')

    userId = context.getVar('userId', default = '')
    if userId:
        userSet = [userId]
    else:
        userSet = None

    layout = X.array()

    if rubricsWeb:
        try:
            mainRubric = rubricsWeb.getMainObject()
        except (faults.MissingMainRubric, faults.UserAccessDenied):
            mainRubric = None
        else:
            if mainRubric.contentId:
                web = getWeb(mainRubric.contentId)
                if web.canGetObject(mainRubric.contentId):
                    object = web.getObject(mainRubric.contentId)
                    if object.getLabel() == mainRubric.getLabel():
                        context.setVar('pageTitle', mainRubric.getLabel())
                        object.title = ''
                    context.push(sectionLevel = context.getVar('sectionLevel')+1)
                    layout += object.getEmbeddedViewLayout()
                    context.pull()
    else:
        mainRubric = None

    requiredSlotNames = ['editionTime', 'title']
    displayedSlotNames = ['editionTime']
    if articlesWeb and userSet:
        lastArticles = articlesWeb.getLastObjects(
            10, None, None, userSet, requiredSlotNames)
        layout += articlesWeb.getObjectsSectionLayout(
            lastArticles,
            _("""Your last articles"""),
            displayedSlotNames)

    if articlesWeb:
        lastArticles = articlesWeb.getLastObjects(
            10, None, userSet, None, requiredSlotNames)
        layout += articlesWeb.getObjectsSectionLayout(
            lastArticles,
            _("""The last articles"""),
            displayedSlotNames)

    requiredSlotNames = ['electionTitle', 'modificationTime', 'title']
    displayedSlotNames = ['modificationTime']

    if brevesWeb and brevesWeb.canGetObjects():
        lastBreves = brevesWeb.getLastObjects(
            10, None, userSet, None, requiredSlotNames)
        layout += brevesWeb.getObjectsSectionLayout(
            lastBreves,
            _("""The last short news"""),
            displayedSlotNames)

    requiredSlotNames = ['endTime', 'isAlwaysRunning', 'state', 'title']
    displayedSlotNames = ['state', 'isAlwaysRunning', 'endTime']

    if userSet and electionsWeb and electionsWeb.canGetObjects():
        lastElections = electionsWeb.getLastObjects(
            10, None, None, userSet, ['draft'], requiredSlotNames)
        layout += electionsWeb.getObjectsSectionLayout(
            lastElections,
            _("""Your elections being written"""),
            displayedSlotNames)

    if electionsWeb and electionsWeb.isAdmin():
        lastElections = electionsWeb.getLastObjects(
            10, None, None, None, ['proposed'], requiredSlotNames)
        layout += electionsWeb.getObjectsSectionLayout(
            lastElections,
            _("""The elections being submitted for evaluation"""),
            displayedSlotNames)

    if electionsWeb and electionsWeb.canGetObjects():
        lastElections = electionsWeb.getLastObjects(
            10, None, None, None, ['running'], requiredSlotNames)
        layout += electionsWeb.getObjectsSectionLayout(
            lastElections,
            _("""The elections in progress"""),
            displayedSlotNames)

        lastElections = electionsWeb.getLastObjects(
            10, None, None, None, ['closed'], requiredSlotNames)
        layout += electionsWeb.getObjectsSectionLayout(
            lastElections,
            _("""The last closed elections"""),
            displayedSlotNames)

    if mainRubric:
        title = rubricsWeb.getObjectLabelTranslated(mainRubric.id,
                    context.getVar('readLanguages'))
    else:
        title = _('Home')
    return writePageLayout(layout, title)

