/*
 * Glasnost
 * By: Odile B�nassy <obenassy@entrouvert.com>
 *     Romain Chantereau <rchantereau@entrouvert.com>
 *     Nicolas Clapi�s <nclapies@easter-eggs.org>
 *     Pierre-Antoine Dejace <padejace@entrouvert.be>
 *     Thierry Dulieu <tdulieu@easter-eggs.com>
 *     Florent Monnier <monnier@codelutin.com>
 *     C�dric Musso <cmusso@easter-eggs.org>
 *     Fr�d�ric P�ters <fpeters@entrouvert.be>
 *     Benjamin Poussin <poussin@codelutin.com>
 *     Emmanuel Raviart <eraviart@entrouvert.com>
 *     S�bastien R�gnier <regnier@codelutin.com>
 *     Emmanuel Saracco <esaracco@easter-eggs.com>
 *
 * Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
 * Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
 *     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
 *     Emmanuel Saracco & Th�ridion
 * Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
 *     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
 *     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
 *     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
 *     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


function prop(elem){
  var result = "";
  for (var p in elem)
    result += p+":"+ elem[p] + "\n";
  alert(result);
}


var pageHistory = new Array();
var pageHistoryIndex = 0;

var pages = new Array();
var pagei = 0;
var pageview;
var indexPage;

function lookupAllPage(){
    var tmppages = document.getElementsByTagName("div");
    var result = new Array();
    for (var i=0; i<tmppages.length; i++){
        if (tmppages.item(i).className == "page"){
            result.push(tmppages.item(i));
        }
    }
    return result;
}

function createIndexPage(listPages){
    var result = "<span class='indexTitle'>"+document.title+"</span><ol>\n";
    for (var i=0; i<listPages.length; i++){
        var title = "page "+ (i+1);
        var page = listPages[i];
        var hindex = 0;
        var hs;
        do{
            hindex++;            
            hs = page.getElementsByTagName("h"+hindex);
        }while(hs.length == 0 && hindex < 7);

        if (hs.length > 0) {
            hs[0].setAttribute("pageNumber", i+1);
            title = hs.item(0).innerHTML;
        }
        result += "<li class='index' onclick='showPage("+i+")'>"+ title + "</li>\n";
    }
    result += "</ol>";
    return result;
}

/**
 * Catch click events
 */
function clickPage(e){
    nextPage();
}

/** 
 * Catch keys events
 */
function keyPage(e){
    var key = e.keyCode;
    if (key == 32 /* space       */ ||
        key == 34 /* page down   */ || 
        key == 40 /* down arrow  */ || 
        key == 39 /* right arrow */)
        nextPage();

    else if (key == 33 /* page up    */ || 
             key == 38 /* up arroaw  */ || 
             key == 37 /* left arrow */)
        precPage();
    else if (key == 73 /* i */ )
        showIndexPage();
    else if (key == 66 /* b */ )
        precPageHistory();
    else if (key == 78 /* n */)
        nextPageHistory();
    else if (key == 72 /* h */)
        showHelp();
}

function showPage(pageNumber){
    pagei = pageNumber;
    pageHistory[pageHistoryIndex++] = pagei;
    if (0<=pageNumber && pageNumber < pages.length ){
        pageview.innerHTML = pages[pageNumber].innerHTML;
    }
}

function showIndexPage(){
    pageview.innerHTML = indexPage;
}

function nextPage(){
    showPage((pagei+1) % pages.length);
}

function precPage(){
    var p = pagei - 1;
    if (p<0)
        p = pages.length;
    showPage(p);
}


function showPageHistory(pageNumber){
    if ( 0<=pageNumber && pageNumber < pages.length )
        pageview.innerHTML = pages[pageNumber].innerHTML;
}

function precPageHistory(){
    if (pageHistoryIndex > 0)
        showPageHistory(pageHistory[(--pageHistoryIndex)-1]);
}

function nextPageHistory(){
    if (pageHistoryIndex < pageHistory.length)
        showPageHistory(pageHistory[(pageHistoryIndex++)]);
}


function showHelp(){
    /* FIXME: should be i18nized */
    pageview.innerHTML = 
        "<li> page up, up arrow, left arrow: previous page"+
        "<li> space, page down, down arrow, right arrow: next page"+
        "<li> i: index"+
        "<li> b: previous page in history"+
        "<li> n: next page in history"+
        "<li> h: this message";
        
}

function main(){
    pageview = document.getElementById("pageview");
    document.addEventListener("click", clickPage, false);
    document.addEventListener("keyup", keyPage, false);
    
    // searching every pages
    pages = lookupAllPage();
    // creating index page
    indexPage = createIndexPage(pages);
    // starting with the index page
    showIndexPage();
}

window.addEventListener("load", main, false);

document.styleSheets[0].insertRule("@media screen { div.page { display: none }}", 1);
