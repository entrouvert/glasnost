# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Session Variable Definition Web Page"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.xhtmlgenerator as X

from glasnost.web.tools import *


def index(**keywords):
    nextUri = context.getVar('nextUri') or ''
    if keywords is None:
        keywords = {}
    session = context.getVar('session')
    sessionToken = context.getVar('sessionToken')
    oldSession = session
    req = context.getVar('req')
    if not session:
        session = getProxyForServerRole('sessions').newSession(
                    req.connection.remote_ip)
        sessionToken = session['sessionToken']
        context.setVar('session', session)
        context.setVar('sessionToken', sessionToken)
    if keywords:
        for key, value in keywords.items():
            session[key] = value
        session['isDirty'] = 1
    nextUri = cleanUpUri(nextUri, ['sessionToken'])
    canUseCookie = context.getVar('canUseCookie', default = 0)
    if not canUseCookie:
        nextUri = appendToUri(nextUri, 'sessionToken=' + sessionToken)
        return redirect(nextUri)
    if oldSession:
        return redirect(nextUri)
    uri = X.actionUrl('testCookie')
    uri.add('nextUri', nextUri)
    uri.add('sessionToken', sessionToken)
    context.setVar('canUseCookie', 0)
    uri = uri.getAsUrl()
    context.setVar('canUseCookie', 1)
    return redirect(uri)


def testCookie():
    nextUri = context.getVar('nextUri') or ''
    session = context.getVar('session')
    nextUri = cleanUpUri(nextUri, ['sessionToken'])
    canUseCookie = context.getVar('canUseCookie', default = 0)
    if not canUseCookie:
        nextUri = appendToUri(
            nextUri,
            'sessionToken=' + context.getVar('sessionToken'))
    return redirect(nextUri)

