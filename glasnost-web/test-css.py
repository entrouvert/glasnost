# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Web Page To test CSS"""

__version__ = '$Revision$'[11:-2]


import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.tools_new as commonTools
import glasnost.common.xhtmlgenerator as X

import glasnost.web.kinds as kinds
import glasnost.web.modes as modes
import glasnost.web.properties as properties
from glasnost.web.tools import writePageLayout
import glasnost.web.uploads as uploads
import glasnost.web.WebAPI as WebAPI
import glasnost.web.widgets as widgets


helloWorld = N_('Hello World')
loremIpsum = """\
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean suscipit. Nulla facilisi. Nulla eleifend. Aliquam sit amet nisl eu wisi laoreet tristique. Aliquam nibh ante, venenatis sit amet, sollicitudin sit amet, hendrerit in, nulla. Vestibulum eu est. Nulla leo massa, vulputate in, placerat nec, pretium quis, odio. Morbi sed leo. Cras molestie enim quis lacus. Praesent egestas, velit ac auctor posuere, nisl erat lacinia wisi, vitae lacinia lorem augue nec tortor. Mauris sollicitudin lorem vitae nulla. Vestibulum et tortor et nibh varius vehicula. Suspendisse molestie.\
"""


def index():
    from glasnost.web.CardsWeb import Card
    card = Card() #commonTools.newCard()

    card.modes = []
    mode = modes.Custom()
    card.modes.append(mode)
    mode.name = 'mode1'
    mode.aspects = []
    
    card.properties = []

    # Add field readOnlyInputText.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyInputText'
    property.kind = kind = kinds.String()
    card.getSlot('readOnlyInputText').setValue(helloWorld)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name

    # Add field readWriteInputText.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteInputText'
    property.kind = kind = kinds.String()
    card.getSlot('readWriteInputText').setValue(helloWorld)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Add field readWriteErrorInputText
    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteErrorInputText'
    property.kind = kind = kinds.String()
    card.getSlot('readWriteErrorInputText').setValue(helloWorld)
    card.setError(card.getSlot('readWriteErrorInputText').getPath(),
            faults.BadValue())

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Add field readOnlyInputText with each possible htmlTag .

    for htmlTag in ['div-with-label', 'div', 'h1', 'h2', 'h3', 'h4', 'h5',
                    'h6']:
        property = properties.Property()
        card.properties.append(property)
        property.name = 'readOnlyInputText'
        property.kind = kind = kinds.String()
        card.getSlot('readOnlyInputText').setValue(helloWorld)

        aspect = modes.Aspect()
        mode.aspects.append(aspect)
        aspect.htmlTag = htmlTag
        aspect.name = property.name

    # Add field readOnlyTextArea.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyTextArea'
    property.kind = kind = kinds.String()
    card.getSlot('readOnlyTextArea').setValue(loremIpsum)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.widget = widgets.TextArea()

    # Add field readOnlyTextAreaFormattedAsText.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyTextAreaFormattedAsText'
    property.kind = kind = kinds.String()
    kind.textFormat = 'text'
    card.getSlot('readOnlyTextAreaFormattedAsText').setValue(loremIpsum)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.widget = widgets.TextArea()

    # Add field readWriteTextArea.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteTextArea'
    property.kind = kind = kinds.String()
    card.getSlot('readWriteTextArea').setValue(loremIpsum)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'
    aspect.widget = widgets.TextArea()

    # Add field readOnlyStringsSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyStringsSequence'
    property.kind = kind = kinds.Sequence()
    kind.itemKind = kinds.String()
    card.getSlot('readOnlyStringsSequence').setValue([
            helloWorld,
            helloWorld,
            helloWorld,
            ])

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.widget = widgets.Multi()

    # Add field readWriteStringsSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteStringsSequence'
    property.kind = kind = kinds.Sequence()
    kind.itemKind = kinds.String()
    card.getSlot('readWriteStringsSequence').setValue([
            helloWorld,
            helloWorld,
            helloWorld,
            ])

    # Add field readWriteUpload.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteUpload'
    property.kind = kind = kinds.Upload()
    upload = uploads.Upload()
    upload.data = loremIpsum
    upload.dataFileName = 'toto.txt'
    upload.dataType = 'octet/stream'
    card.getSlot('readWriteUpload').setValue(upload)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Add field readWriteUploadsSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteUploadsSequence'
    property.kind = kind = kinds.Sequence(
            itemKind = kinds.Upload())
    upload = uploads.Upload()
    upload.data = loremIpsum
    upload.dataFileName = 'toto.txt'
    upload.dataType = 'octet/stream'
    card.getSlot('readWriteUploadsSequence').setValue([
            upload,
            upload,
            upload])

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Add field readOnlyAspect.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyAspect'
    property.kind = kind = kinds.Thing(
            defaultValue = modes.Aspect(),
            valueThingCategory = 'other',
            valueThingName = 'Aspect')
    anAspect = modes.Aspect()
    anAspect.name = 'anAspect'
    card.getSlot('readOnlyAspect').setValue(anAspect)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name

    # Add field readWriteAspect.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteAspect'
    property.kind = kind = kinds.Thing(
            defaultValue = modes.Aspect(),
            valueThingCategory = 'other',
            valueThingName = 'Aspect')
    anAspect = modes.Aspect()
    anAspect.name = 'anAspect'
    card.getSlot('readWriteAspect').setValue(anAspect)

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Add field readOnlyAspectsSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyAspectsSequence'
    property.kind = kind = kinds.Sequence(
            itemKind = kinds.Thing(
                    defaultValue = modes.Aspect(),
                    valueThingCategory = 'other',
                    valueThingName = 'Aspect'))
    anAspect = modes.Aspect()
    anAspect.name = 'anAspect'
    card.getSlot('readOnlyAspectsSequence').setValue([
            anAspect,
            anAspect,
            anAspect])

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name

    # Add field readWriteAspectsSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWriteAspectsSequence'
    property.kind = kind = kinds.Sequence(
            itemKind = kinds.Thing(
                    defaultValue = modes.Aspect(),
                    valueThingCategory = 'other',
                    valueThingName = 'Aspect'))
    anAspect = modes.Aspect()
    anAspect.name = 'anAspect'
    card.getSlot('readWriteAspectsSequence').setValue([
            anAspect,
            anAspect,
            anAspect])

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Add field readOnlyPropertiesSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readOnlyPropertiesSequence'
    property.kind = kind = kinds.Sequence(
            itemKind = kinds.Thing(
                    defaultValue = properties.Property(),
                    valueThingCategory = 'other',
                    valueThingName = 'Property'))
    card.getSlot('readOnlyPropertiesSequence').setValue(card.properties[-5:])

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name

    # Add field readWritePropertiesSequence.

    property = properties.Property()
    card.properties.append(property)
    property.name = 'readWritePropertiesSequence'
    property.kind = kind = kinds.Sequence(
            itemKind = kinds.Thing(
                    defaultValue = properties.Property(),
                    valueThingCategory = 'other',
                    valueThingName = 'Property'))
    card.getSlot('readWritePropertiesSequence').setValue(card.properties[-5:])

    aspect = modes.Aspect()
    mode.aspects.append(aspect)
    aspect.name = property.name
    aspect.state = 'read-write'

    # Display card.

    again = 0
    error = 0
    create = 0
    keywords = {}
    context.push(_level = 'index',
                 layoutMode = 'use',
                 pageTitle = 'Test CSS - Index')
    try:
        headerTitle, pageBodyLayout, buttonsBarLayout, inForm \
                = mode.getModelLayoutInfos(card, keywords, create)
        layout = X.array()
        if inForm:
            if error:
                layout += card.getErrorLayout(keywords)
            form = X.form(action = X.actionUrl('submit'),
                          enctype = 'multipart/form-data', method = 'post')
            layout += form
            form += pageBodyLayout
            if buttonsBarLayout:
                form += buttonsBarLayout
        else:
            layout += pageBodyLayout
            if buttonsBarLayout:
                layout += buttonsBarLayout

        context.push(
                currentObject = WebAPI.GlasnostObject(object = card))
        layout = writePageLayout(layout, headerTitle)
        context.pull()
        return layout
    finally:
        context.pull(_level = 'index')

