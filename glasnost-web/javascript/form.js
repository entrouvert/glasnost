function moveItemUp(elem) {
	ul = elem.parentNode;
	var n = 0;
	for (var i=0; i<ul.childNodes.length; i++) {
		tag = ul.childNodes[i];
		if (tag.nodeName != 'LI') continue;
		if (elem != tag) {
			n = i;
			continue;
		}
		if (n == 0) break;

		otherNode = ul.childNodes[n];
		for (var j=0; j<elem.childNodes.length; j++) {
			name = elem.childNodes[j].name;
			if (! name) continue;
			t = elem.childNodes[j].name;
			elem.childNodes[j].name = otherNode.childNodes[j].name;
			otherNode.childNodes[j].name = t;
		}
		ul.insertBefore(elem, otherNode);
		break;
	}
}

function moveItemDown(elem) {
	ul = elem.parentNode;
	var n = -1;
	for (var i=0; i<ul.childNodes.length; i++) {
		tag = ul.childNodes[i];
		if (tag.nodeName != 'LI') continue;
		if (n == -1) {
			if (elem != tag) continue;
			n = 0;
			continue;
		}
		n = i;

		otherNode = ul.childNodes[n];
		for (var j=0; j<elem.childNodes.length; j++) {
			name = elem.childNodes[j].name;
			if (! name) continue;
			t = elem.childNodes[j].name;
			elem.childNodes[j].name = otherNode.childNodes[j].name;
			otherNode.childNodes[j].name = t;
		}
		ul.insertBefore(otherNode, elem);
		break;
	}
}

function selectOthers(tdHere, url)
{
	var selectBox = tdHere.getElementsByTagName('select')[0];
	selectBox.id = 'editedSelect';
	var w = window.open(url, 'test', 'scrollbars=yes,width=500,height=120')
	// FIXME: should include sessionToken
}

