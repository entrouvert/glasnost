addEvent(window, "load", makeBalloonHelp);

document.getElementsByClassName = function ( class_name ) {
    var all_obj, ret_obj = new Array(), j = 0, strict = 0;
    if ( document.getElementsByClassName.arguments.length > 1 )
        strict = ( document.getElementsByClassName.arguments[1] ? 1 : 0 );
    if ( document.all )
        all_obj = document.all;
    else if ( document.getElementsByTagName && !document.all )
        all_obj = document.getElementsByTagName ( "*" );
    for ( i = 0; i < all_obj.length; i++ ) {
        if ( ( ' ' + all_obj[i].getAttribute("class") + ' ').toLowerCase().match(
            new RegExp ( ( strict ? '^ ' + class_name + ' $' : 
                '^.* ' + class_name + ' .*$' ).toLowerCase(),'g' ) ) ) {
            ret_obj[j++] = all_obj[i];
        }
    }
    return ret_obj;
}

var previousBalloon;
var currentBalloon;
var timeoutId;
var timeoutIdPrevious;

function makeBalloonHelp() {
	if (!document.createElement) return;
	var tags = document.getElementsByClassName('tooltip');
	for (var i=0; i<tags.length; i++) {
		tag = tags[i];
		if (tag.id.substring(0, 3) != 'for' ) continue;
		tag.style.display = 'none';
		tag.style.visibility = 'visible';
		parentTag = document.getElementById(tag.id.substring(4, tag.id.length));
		if (! parentTag) continue;
		if (parentTag.childNodes.length == 1 &&
				parentTag.childNodes[0].nodeType != 3)
			parentTag = parentTag.childNodes[0]
		parentTag.balloonHelp = tag;
		addEvent(parentTag, "mouseover", showBalloonHelp);
		addEvent(parentTag, "focus", showBalloonHelp);
		addEvent(parentTag, "mouseout", hideBalloonHelp);
		addEvent(parentTag, "blur", hideBalloonHelp);
		addEvent(parentTag, "keydown", hideBalloonHelp);
	}
}

function addEvent(obj, evType, fn) {
	/* adds an eventListener for browsers which support it.
		 Derived from snippet by Scott Andrew */
	if (obj.addEventListener) {
		obj.addEventListener(evType, fn, true);
		return true;
	}
	if (obj.attachEvent)
		return obj.attachEvent("on"+evType, fn);
	return false;
}

function showBalloonHelp(e) {
	if (window.event && window.event.srcElement) {
		el = window.event.srcElement
	} else if (e && e.target) {
		el = e.target
	}
	if (!el) return;

	while (! el.balloonHelp ) {
		el = el.parentNode;
	}
	tag = el.balloonHelp;

	if (currentBalloon && tag != currentBalloon)
		hideBalloonHelp();
		
	if (tag == previousBalloon)
		return;

	tag.style.display = 'block';

	currentBalloon = tag;
	clearTimeout(timeoutId);
	timeoutId = setTimeout('hideBalloonHelp()', 3000);
}

function hideBalloonHelp(e) {
	if (!document.getElementsByTagName) return;
	if (currentBalloon) {
		currentBalloon.style.display = 'none';
		previousBalloon = currentBalloon;
		currentBalloon = null;
		clearTimeout(timeoutIdPrevious);
		timeoutIdPrevious = setTimeout('clearPrevious()', 5000);
	}
}

function clearPrevious(e) {
	previousBalloon = null;
}

