/* Tooltip handling for Glasnost
   See http://glasnost.entrouvert.org/about for licence and copyright */

var tipdiv = null;
var popDownDelay = 300;
var timeoutId;

// you can define an other class for tooltips or more than one class
if (typeof tipClass == 'undefined')
	var tipClass= new Array('tooltip');

addEvent(window, "load", initTooltips);

function initTooltips() {
	createDivTooltip();

	tags = getTipElements(document);

	for(var i=0; i<tags.length; i++) {
		var parent = getParentForTooltip(tags[i]);
		addEvent(parent, 'mouseover', popUp);
		addEvent(parent, 'mouseout', popDown);
		addEvent(parent, "focus", popUp);
		addEvent(parent, "blur", popDown);
		addEvent(parent, "keydown", popDown);

		hide(tags[i]);
	}
}

/**
 * return element those activate the tooltip
 */
function getParentForTooltip(tip) {
	var parent = null;
	if (tip.hasAttribute('id')) {
		var id = tip.getAttribute('id');
		if (id.substring(0,4) == 'for-') {
			id = id.substring(4, id.length);
			parent = document.getElementById(id);
		}
	}
	if (!parent)
		parent = tip.parentNode;
	return parent;
}

function createDivTooltip() {
	div = document.createElement("DIV");
	div.style.position = "absolute";
	div.style.zIndex = "100";

	hide(div);
	document.body.appendChild(div);
	tipdiv = div;
}

function getTipElements(tag, result) {
	if (result == undefined) {
		var result = new Array();
	}
	var childs = tag.childNodes;
	for (var i=0; i<childs.length; i++) {
		child = childs[i];
		for(var c=0; c<tipClass.length; c++) {
			if (child.className && child.className.toString().indexOf(tipClass[c]) != -1) {
				result[result.length] = child;
				break;
			}
		}
		getTipElements(child, result);
	}
	return result;
}

function addEvent(obj, evType, fn) {
	/* adds an eventListener for browsers which support it
		 Written by Scott Andrew: nice one, Scott */
	if (obj.addEventListener) {
		obj.addEventListener(evType, fn, true);
		return true;
	} else if (obj.attachEvent) {
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	} else {
		return false;
	}
}

function getTipFromEvent(evt) {
	var parent = evt.currentTarget;
	var tip = null;
	if (parent.hasAttribute('id')) {
		var id = parent.getAttribute('id');
		var tip = document.getElementById('for-' + id);
	}
	if (!tip) {
		tip = getTipElements(evt.currentTarget)[0];
	}
	return tip;
}

function getStyleOfTag(tag) {
	result = tag.style;
	return result;
}

function clone(tag) {
	var result = tag.cloneNode(true);
	return result;
}

function hide(tag) {
	style = getStyleOfTag(tag);
	style.visibility = 'hidden';
 	style.display = 'none';
}

function show(tag) {
	style = getStyleOfTag(tag);
	style.visibility = 'visible';
 	style.display = 'block';
}

function moveTipdiv(top, left) {
	var style = getStyleOfTag(tipdiv);
	style.left = left;
	style.top = top;
}

function resizeTipdiv() {
	var style = getStyleOfTag(tipdiv);
	tip = tipdiv.lastChild;

	width = tip.offsetWidth;
	posX = findPosX(tipdiv);
	winWidth = window.innerWidth;
	if (posX + width >= winWidth - 25) {
		width = winWidth - posX - 25;
	}
	
	style.height = tip.offsetHeight + 'px';
	style.width = width + 'px';
	
	tip.style.height = style.height;
	tip.style.width = style.width;
}

function findPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj) {
	var curtop = obj.offsetHeight;
	// try to pass through Gecko bug (number ?)  (25 is an arbitrary value)
	if (curtop < 25)
	  curtop = 25;

	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

function prepareTooltip(tip) {
	var message = clone(tip);
	show(message);
	
	var lastChild = tipdiv.lastChild;
	if (lastChild)
		tipdiv.removeChild(tipdiv.lastChild);
	tipdiv.appendChild(message);
}

function popDown(evt) {
	timeoutId = setTimeout('hide(tipdiv)', popDownDelay);
	return true;
}

function popUp(evt) {
	clearTimeout(timeoutId);
	var tip = getTipFromEvent(evt);
	var parent = evt.currentTarget;

	if (!tip) return true;

	prepareTooltip(tip);

	var top = findPosY(parent) +'px';
	var left = findPosX(parent) +'px';

	moveTipdiv(top, left);
	show(tipdiv);
	
	resizeTipdiv();
	
	hide(tipdiv);
	show(tipdiv);

	return true;
}

