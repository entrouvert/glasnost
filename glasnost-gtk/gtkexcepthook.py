# from pygtk faq, 20.10
# (http://www.async.com.br/faq/pygtk/index.py?req=show&file=faq20.010.htp)
#
# The second approach is to have exceptions displayed in a dialog. Gustavo
# Carneiro has written "a sys.excepthook function that displays a nice dialog
# for uncaught exceptions. To use, just import the module, that's all. The
# license is whatever you want.". You might want to extend on his module to add
# something like a talkback function "Oops. Something bad happened. Should I
# send a report back to the developer?".
# 
# http://www.daa.com.au/pipermail/pygtk/attachments/20030828/2d304204/gtkexcepthook.py
#
# I thought this might be useful to someone else.  I made this sys.excepthook
# function that displays a nice dialog for uncaught exceptions.  To use, just
# import the module, that's all.  The license is whatever you want.
#
#  -- Gustavo J A M Carneiro
#     http://www.daa.com.au/pipermail/pygtk/2003-August/005775.html

import sys
import gtk, pango
from gettext import gettext as _
from cStringIO import *
import traceback

_exception_in_progress = 0

def _info(type, value, tb):
    global _exception_in_progress
    if _exception_in_progress:
        # Exceptions have piled up, so we use the default exception
        # handler for such exceptions
        _excepthook_save(type, value, tb)
        return
    _exception_in_progress = 1
    dialog = gtk.MessageDialog(parent=None,
                               flags=0,
                               type=gtk.MESSAGE_WARNING,
                               buttons=gtk.BUTTONS_NONE,
                               message_format=_(
        "<big><b>A programming error has been detected during the execution of this program.</b></big>"
        "\n\nIt probably isn't fatal, but should be reported to the developers nonetheless."))
    dialog.set_title(_("Bug Detected"))
    dialog.set_property("has-separator", False)
    dialog.vbox.get_children()[0].get_children()[1].set_property("use-markup", True)

    dialog.add_button(_("Show Details"), 1)
    dialog.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

    # Details
    textview = gtk.TextView(); textview.show()
    textview.set_editable(False)
    textview.modify_font(pango.FontDescription("Monospace"))
    sw = gtk.ScrolledWindow(); sw.show()
    sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    sw.add(textview)
    frame = gtk.Frame();
    frame.set_shadow_type(gtk.SHADOW_IN)
    frame.add(sw)
    frame.set_border_width(6)
    dialog.vbox.add(frame)
    textbuffer = textview.get_buffer()
    trace = StringIO()
    traceback.print_exception(type, value, tb, None, trace)
    textbuffer.set_text(trace.getvalue())
    textview.set_size_request(gtk.gdk.screen_width()/2, gtk.gdk.screen_height()/3)

    dialog.details = frame
    dialog.set_position(gtk.WIN_POS_CENTER)
    dialog.set_gravity(gtk.gdk.GRAVITY_CENTER)
    
    while 1:
        resp = dialog.run()
        if resp == 1:
            dialog.details.show()
            dialog.action_area.get_children()[1].set_sensitive(0)
        else: break
    dialog.destroy()
    _exception_in_progress = 0
    

_excepthook_save = sys.excepthook
sys.excepthook = _info

