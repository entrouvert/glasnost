#! /usr/bin/python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

__doc__ = """Main file for the Glasnost - GUI"""

__version__ = '$Revision$'[11:-2]


import __builtin__

import pygtk
pygtk.require('2.0')
import gtk
import gettext
import locale
import os
import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.applications as applications
import glasnost.common.context as context
import glasnost.common.tools_new as commonTools
from glasnost.common.tools import utf8
from glasnost.common.cache import cache

import glasnost.gtk.MainWindow
import glasnost.gtk.LoginBox

if not '-d' in sys.argv:
    try:
        import gtkexcepthook
    except ImportError:
        pass


class Application(applications.Application):
    applicationName = 'GlasnostGTK'
    applicationRole = 'gtk'

    def initContextCommandLineOptions(self):
        applications.Application.initContextCommandLineOptions(self)

        commandLineContext = context.get(_level = 'commandLine')

        language = os.environ.get('LANG')
        if language:
            language = language.split('_')[0].split('@')[0]
        else:
            language = 'C'
        commandLineContext.setVar('readLanguages', [language])

    def initContextOriginalOptions(self):
        applications.Application.initContextOriginalOptions(self)

        originalContext = context.get(_level = 'original')

        originalContext.setVar('cache', cache)

    def loadConfigOptions(self):
        applications.Application.loadConfigOptions(self)

        configContext = context.get(_level = 'config')

        gettextDomains = commonTools.getConfig(
                'Misc', 'GettextDomains',
                default = '%s-web' % glasnost.applicationName).replace(
                ',', ' ').split()
        gettextDomains = [domain.strip()
                          for domain in gettextDomains]
        configContext.setVar('gettextDomains', gettextDomains)

    def main(self):
        self.launch()

        #locale.setlocale(locale.LC_ALL, '')
        domains = context.getVar('gettextDomains')
        translation = commonTools.translation(domains + ['gnome-libs'],
                context.getVar('readLanguages'))
        __builtin__.__dict__['_'] = lambda x: utf8(translation.gettext(x))

        imagesDirectory = glasnost.gtk.MainWindow.__file__
        imagesDirectory = imagesDirectory[:imagesDirectory.rindex('/')]

        gladeFileName = glasnost.gtk.MainWindow.__file__
        gladeFileName = gladeFileName[:gladeFileName.rindex('/')] + \
                '/glade/glasnost-gtk.glade'
        if not os.path.exists(gladeFileName):
            gladeFileName = None

        context.push(
            #Must be chosen by user (default follows...)
            userId = '',
            userToken = '',
            #Instances of Active Gtk widgets
            gtkMainWindow_instance = None,
            gtkMiniBrowser_instance = None,
            gtkDirectMenu_instance = None,
            imagesDirectory = imagesDirectory,
            gladeFileName = gladeFileName,
            gtkGlobalEvent_instance = None
            )

        #DaWindow = glasnost.gtk.MainWindow.MainWindow()
        loginBox = glasnost.gtk.LoginBox.LoginBox()
        gtk.main()


if __name__ == '__main__':
    application = Application()
    application.main()

