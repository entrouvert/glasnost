#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Add People"""

__version__ = '0.0.1'

import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import ldap

import glasnost.common.context as context

context.push(
    applicationId = 'glasnost://scripts/add-people',
    dispatcherId = 'glasnost://localhost',
    userToken = None, # Set later.
    )

from glasnost.proxy.AuthenticationProxy import authenticationProxy
from glasnost.proxy.PeopleProxy import Person, peopleProxy


ldap_host = "localhost"
ldap_port = 389
ldap_login = "cn=admin,o=codelutin,c=fr"
ldap_password = "xxxxxxxx"
ldap_suffix = "o=codelutin,c=fr"
ldap_searchString = "cn=*"

glasnost_login = "poussin"
glasnost_password = "QGcT8E3n"

options = ['ldap_host',
           'ldap_port',
           'ldap_login',
           'ldap_password',
           'ldap_suffix',
           'ldap_searchString',
           'glasnost_login',
           'glasnost_password',
           ]
i = 0
while i<len(sys.argv):
    optionName = sys.argv[i]
    i = i + 1
    if optionName in options:
        optionValue = sys.argv[i]
        i = i + 1
        locals()[optionName] = optionValue
        print optionName, optionValue

con = ldap.open(ldap_host, ldap_port)
con.simple_bind_s(ldap_login, ldap_password)
ldap_result = con.search_s(ldap_suffix, ldap.SCOPE_SUBTREE,
                                ldap_searchString, None)
con.unbind_s()

userToken = authenticationProxy.getUserToken('login/password',
        glasnost_login,    # Enter your login here.
        glasnost_password) # Enter your password here.
context.setVar('userToken', userToken)

for personInfo in ldap_result:
    person = Person()
    if personInfo[1].has_key('cn'):
        person.lastName =  personInfo[1]['cn'][0]
    if personInfo[1].has_key('sn'):
        person.firstName =  personInfo[1]['sn'][0]
    if personInfo[1].has_key('cn'):
        person.login =  personInfo[1]['cn'][0]
    if personInfo[1].has_key('userPassword'):
        person.password =  personInfo[1]['userPassword'][0]
    
    try:
        person.id = peopleProxy.addObject(person)
        if personInfo[1].has_key('mail'):
            person.email = personInfo[1]['mail'][0]
        peopleProxy.modifyObject(person)
        print 'adding %s: done' % person.login
    except:
        print 'adding %s: failed' % person.login

