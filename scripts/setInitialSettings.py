#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

from scriptingTools import *
from glasnost.proxy.tools import getProxyForServerRole

adminId = sys.argv[1]
editorsId = sys.argv[2]

dispatcherId = commonTools.extractDispatcherId(adminId)

init(dispatcherId)

for role in context.getVar('knownRoles'):
    if role == 'virtualhosts':
        continue
    try:
        proxy = getProxyForServerRole(role)
        admin = proxy.getAdmin()
    except:
        print 'skipping', role
        continue
    print 'doing', role
    admin.adminsSet = [ sys.argv[1] ]
    admin.writersSet = [ sys.argv[2] ]
    try:
        proxy.modifyAdmin(admin)
    except:
        pass

