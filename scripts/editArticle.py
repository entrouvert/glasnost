#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

from scriptingTools import *
from glasnost.proxy.tools import getProxyForServerRole

articleId = sys.argv[1]
dispatcherId = commonTools.extractDispatcherId(articleId)

init(dispatcherId)

articlesProxy = getProxyForServerRole('articles')

object = getObject(articleId)

import tempfile
fileName = tempfile.mktemp()
open(fileName, 'w').write(object.body)
os.system('$EDITOR %s' % fileName)

newBody = open(fileName).read()
if newBody == object.body:
    raise 'no change'

object.body = newBody
articlesProxy.modifyObject(object)

os.unlink(fileName)

