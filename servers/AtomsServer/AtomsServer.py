#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Atoms Server"""

__version__ = '$Revision$'[11:-2]


import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

from glasnost.common.AtomsCommon import *
import glasnost.common.faults as faults
import glasnost.common.context as context
import glasnost.common.tools_new as commonTools
import glasnost.common.xhtmlgenerator as X

from glasnost.server.ObjectsServer import register, ObjectServerMixin, \
        AdminServerMixin, ObjectsServer
from glasnost.server.tools import *


applicationName = 'AtomsServer'
applicationRole = 'atoms'
dispatcher = None


class AdminAtoms(AdminServerMixin, AdminAtomsCommon):
    pass
register(AdminAtoms)


class Atom(ObjectServerMixin, AtomCommon):
    def canBeCreatedByClient(self):
        clientToken = context.getVar('clientToken')
        clientId = getApplicationId(clientToken)
        clientNameAndPort, clientRole, mu = commonTools.splitId(clientId)
        return ObjectServerMixin.canBeCreatedByClient(self) \
               or clientRole == 'elections'

    def checkAddIsPossible(self):
        ObjectServerMixin.checkAddIsPossible(self)
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        if self.name is not None:
            for atom in virtualServer.objects.values():
                if atom.name == self.name:
                    raise faults.DuplicateValue('name', self.name)

    def checkModifyIsPossible(self, changes, givenSlotNames = None):
        ObjectServerMixin.checkModifyIsPossible(
            self, changes, givenSlotNames = givenSlotNames)
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        if (not givenSlotNames or 'name' in givenSlotNames) \
               and changes.name != self.name and changes.name is not None:
            for atom in virtualServer.objects.values():
                if atom.name == changes.name:
                    raise faults.DuplicateValue('name', changes.name)
register(Atom)


class AtomsServer(AtomsCommonMixin, ObjectsServer):
    useAdminWritersSet = 0

    def repairVirtualServer(self, virtualServer, version):
        changed = 0
        if version < 4000:
            changed = virtualServer.admin.repair(4000) or changed
            for id, object in virtualServer.objects.items():
                newId = repairId(id)
                if newId:
                    changed = 1
                    del virtualServer.objects[id]
                    virtualServer.objects[newId] = object
                changed = object.repair(4000) or changed
                if not object.__dict__.has_key('language'):
                    changed = 1
                    object.language = 'fr'
        if version < 5004:
            changed = virtualServer.admin.repair(5004) or changed
            for id, object in virtualServer.objects.items():
                changed = object.repair(5004) or changed
        if version <= 1008000:
            admin = virtualServer.admin
            if admin.id is None:
                changed = 1
                admin.id = '%s/__admin__' % virtualServer.virtualServerId
        if changed:
            virtualServer.markAllAsDirtyFIXME()


atomsServer = AtomsServer()


if __name__ == "__main__":
    atomsServer.launch(applicationName, applicationRole)
