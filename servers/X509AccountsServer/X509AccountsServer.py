#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@entrouvert.com>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost X509account Accounts Server"""

__version__ = '$Revision$'[11:-2]


import copy
import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.context as context
import glasnost.common.faults as faults
from glasnost.common.tools import iso8859_15
import glasnost.common.tools_new as commonTool
import glasnost.common.X509AccountsCommon as commonX509Accounts

import glasnost.server.ObjectsServer as objects

from glasnost.proxy.tools import getProxy


applicationName = 'X509AccountsServer'
applicationRole = 'x509accounts'
dispatcher = None


class AdminX509Accounts(objects.AdminServerMixin,
                        commonX509Accounts.AdminX509Accounts):
    pass
objects.register(AdminX509Accounts)


class X509Account(objects.ObjectServerMixin,
                      commonX509Accounts.X509Account):
    def checkModifyIsPossible(self, changes, givenSlotNames = None):
        objects.ObjectServerMixin.checkModifyIsPossible(
                self, changes, givenSlotNames = givenSlotNames)
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        if (not givenSlotNames or 'serial' in givenSlotNames) \
               and changes.serial != self.serial \
               and changes.serial is not None:
            if virtualServer.objectsBySerial.has_key(changes.serial) \
                    and changes.id != virtualServer.objectsBySerial[
                            changes.serial].id:
                raise faults.DuplicateSerial(changes.serial)

    def clear(self):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        if virtualServer.objectsBySerial.has_key(self.serial):
            del virtualServer.objectsBySerial[self.serial]

    def modify(self, changes, givenSlotNames = None):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        serial = self.serial
        objects.ObjectServerMixin.modify(
                self, changes, givenSlotNames = givenSlotNames)
        if self.serial != serial:
            if serial is not None:
                del virtualServer.objectsBySerial[serial]
            if self.serial is not None:
                virtualServer.objectsBySerial[self.serial] = self
objects.register(X509Account)


class X509AccountsVirtualServer(objects.ObjectsVirtualServer):
    objectsBySerial = None

    def init(self):
        objects.ObjectsVirtualServer.init(self)
        self.objectsBySerial = {}


class X509AccountsServer(
        commonX509Accounts.X509AccountsCommonMixin,
        objects.ObjectsServer):
    VirtualServer = X509AccountsVirtualServer

    def addObjectXmlRpc(self, objectImport):
        objectId = objects.ObjectsServer.addObjectXmlRpc(self, objectImport)
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        object = virtualServer.loadObjectCore(objectId)
        if virtualServer.objectsBySerial.has_key(object.serial):
            # Serial already used.
            del virtualServer.objects[objectId]
            virtualServer.markObjectAsDeleted(objectId)
            virtualServer.markCoreAsDirty()
            raise faults.DuplicateSerial(object.serial)

        virtualServer.objectsBySerial[object.serial] = object
        virtualServer.markCoreAsDirty()
        return objectId

    def checkObjectAuthenticationXmlRpc(self, serial):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        if not virtualServer.objectsBySerial.has_key(serial):
            serial = serial.lstrip('0')
            if not virtualServer.objectsBySerial.has_key(serial):
                raise faults.WrongX509Serial(serial)
        object = virtualServer.objectsBySerial[serial]
        identitiesProxy = getProxy(object.identityId)
        return [identitiesProxy.getUserToken(object.identityId),
                object.authenticationMethod]

    def registerPublicMethods(self):
        objects.ObjectsServer.registerPublicMethods(self)
        self.registerPublicMethod('checkObjectAuthentication',
                                  self.checkObjectAuthenticationXmlRpc)


x509AccountsServer = X509AccountsServer()


if __name__ == "__main__":
    x509AccountsServer.launch(applicationName, applicationRole)
