#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Assertions Server"""

__version__ = '$Revision$'[11:-2]


import md5
import sys
import time
import whrandom

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.context as context
import glasnost.common.faults as faults

import glasnost.server.ObjectsServer as objects

import lasso.Tools as lassoTools

applicationName = 'AssertionsServer'
applicationRole = 'assertions'
dispatcher = None

expirationTime = 1 * 60


class AssertionsVirtualServer(objects.VirtualServer):
    assertions = None
    assertionExpirationTimes = None

    def init(self):
        objects.VirtualServer.init(self)
        self.assertions = {}
        self.assertionExpirationTimes = {}


class AssertionsServer(objects.Server):
    VirtualServer = AssertionsVirtualServer

    def addAssertion(self, assertion):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        virtualServer.lock.acquire()
        while 1:
            artifact = lassoTools.buildAssertionArtifact()
            if not virtualServer.assertions.has_key(artifact):
                break
        virtualServer.assertions[artifact] = assertion
        virtualServer.assertionExpirationTimes[artifact] \
                = time.time() + expirationTime
        virtualServer.lock.release()
        virtualServer.markCoreAsDirty()
        return artifact

    def convertVirtualServersIds(self, sourceDispatcherId,
                                 destinationDispatcherId):
        # Erase the assertions when converting ids.
        return 0

    def getAssertion(self, artifact):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)

        # Remove expired assertions.
        currentTime = time.time()
        for artifact2 in virtualServer.assertions.keys():
            if currentTime > virtualServer.assertionExpirationTimes[artifact2]:
                del virtualServer.assertions[artifact2]
                del virtualServer.assertionExpirationTimes[artifact2]

        if not virtualServer.assertions.has_key(artifact):
            raise faults.WrongArtifact(artifact)
        assertion = virtualServer.assertions[artifact]
        del virtualServer.assertions[artifact]
        del virtualServer.assertionExpirationTimes[artifact]
        virtualServer.markCoreAsDirty()
        return assertion

    def init(self):
        self.randomGenerator = whrandom.whrandom()
        objects.Server.init(self)

    def registerPublicMethods(self):
        objects.Server.registerPublicMethods(self)
        self.registerPublicMethod('addAssertion')
        self.registerPublicMethod('getAssertion')


assertionsServer = AssertionsServer()


if __name__ == "__main__":
    assertionsServer.launch(applicationName, applicationRole)

