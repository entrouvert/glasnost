#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Comments Server"""

__version__ = '$Revision$'[11:-2]


import sys
import time

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

from glasnost.common.CommentsCommon import *
import glasnost.common.faults as faults
import glasnost.common.tools_new as commonTools

from glasnost.server.ObjectsServer import ObjectServerMixin, \
        AdminServerMixin, ObjectsServer
from glasnost.server.things import register
from glasnost.server.tools import *


applicationName = 'CommentsServer'
applicationRole = 'comments'
dispatcher = None


class AdminComments(AdminServerMixin, AdminCommentsCommon):
    pass
register(AdminComments)


class Comment(ObjectServerMixin, CommentCommon):
    def checkAddIsPossible(self):
        ObjectServerMixin.checkAddIsPossible(self)
        virtualServerId = context.getVar('applicationId')
        ultimateParentId = self.getServer().getParentId(self)
        parent = getObject(ultimateParentId)
            # if it booms out; adding was not possible, we get what we want
        # TODO: parent object should tell if it accepts comments

    def modify(self, changes, givenSlotNames = None):
        return ObjectServerMixin.modify(self, changes,
                ['language', 'title', 'body'])

    def setAutomaticalSlots(self, parentSlot = None):
        ObjectServerMixin.setAutomaticalSlots(self, parentSlot = parentSlot)
        self.authorId = getProxyForServerRole('identities').getUserId()
        self.creationTime = time.time()
register(Comment)


class CommentsServer(CommentsCommonMixin, ObjectsServer):
    def canGetObject(self, objectId):
        result = ObjectsServer.canGetObject(self, objectId)
        if result == 0:
            return 0
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        object = virtualServer.loadObjectCore(objectId)
        parentId = self.getParentId(object)
        roleProxy = getProxyForServerRole(
                commonTools.extractRole(parentId))
        return roleProxy.canGetObject(parentId)

    def canGetObjects(self):
        return 0

    def canPostEditorialComment(self, parentId):
        roleProxy = getProxyForServerRole(
                commonTools.extractRole(parentId))
        return ObjectsServer.canAddObject(self) and \
                roleProxy.canModifyObject(parentId)

    def getObjectIdsWithParent(self, parentId, isEditorial):
        if isEditorial and not self.canPostEditorialComment(parentId):
            raise faults.UserAccessDenied()
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        tempResult = []
        for objectId, objectCore in virtualServer.objects.items():
            if self.getParentId(objectCore) == parentId and \
                    objectCore.isEditorial == isEditorial:
                tempResult.append(objectId)
        result = []
        for objectId in tempResult:
            if self.canGetObject(objectId):
                result.append(objectId)
        return result

    def getParentId(self, comment):
        currentComment = comment
        while 1:
            role = commonTools.extractRole(currentComment.parentId)
            if role != 'comments':
                return currentComment.parentId
            currentComment = self.getObjectCore(currentComment.parentId)

    def registerPublicMethods(self):
        ObjectsServer.registerPublicMethods(self)
        self.registerPublicMethod('canPostEditorialComment')
        self.registerPublicMethod('getObjectIdsWithParent')

commentsServer = CommentsServer()


if __name__ == '__main__':
    commentsServer.launch(applicationName, applicationRole)

