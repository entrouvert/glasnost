#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost PageNames Server"""

__version__ = '$Revision$'[11:-2]


import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.faults as faults
from glasnost.common.PageNamesCommon import *
import glasnost.common.context as context

from glasnost.server.ObjectsServer import register, ObjectServerMixin, \
        AdminServerMixin, ObjectsServer
from glasnost.server.tools import *


applicationName = 'PageNamesServer'
applicationRole = 'pagenames'
dispatcher = None


class AdminPageNames(AdminServerMixin, AdminPageNamesCommon):
    pass
register(AdminPageNames)


class PageName(ObjectServerMixin, PageNameCommon):
    def checkAddIsPossible(self):
        ObjectServerMixin.checkAddIsPossible(self)
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        if self.name is not None:
            for pageName in virtualServer.objects.values():
                if pageName.name == self.name:
                    raise faults.DuplicateValue('name', self.name)

    def checkModifyIsPossible(self, changes, givenSlotNames = None):
        ObjectServerMixin.checkModifyIsPossible(
            self, changes, givenSlotNames = givenSlotNames)
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getServer().getVirtualServer(virtualServerId)
        if (not givenSlotNames or 'name' in givenSlotNames) \
               and changes.name != self.name and changes.name is not None:
            for pageName in virtualServer.objects.values():
                if pageName.name == changes.name:
                    raise faults.DuplicateValue('name', changes.name)

    def modify(self, changes, givenSlotNames = None):
        return ObjectServerMixin.modify(self, changes,
                                        givenSlotNames = givenSlotNames)
        # TODO: (path support)
        #   if we change "doc" to "documentation" and we had "doc/userguide" it
        #   should be updated to "documentation/userguide"
register(PageName)


class PageNamesServer(PageNamesCommonMixin, ObjectsServer):
    def getIdByName(self, name):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name == name:
                return object.mappedId
        # Id not found. Look for it into help virtual server.
        virtualServerId = 'glasnost://help/%s' % self.applicationRole
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name == name:
                return object.mappedId
        # Id not found. Look for it into system virtual server.
        virtualServerId = 'glasnost://system/%s' % self.applicationRole
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name == name:
                return object.mappedId
        return '' # or should it raise sth ?

    def getObjectIdByName(self, name):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name == name:
                return object.id
        # Id not found. Look for it into help virtual server.
        virtualServerId = 'glasnost://help/%s' % self.applicationRole
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name == name:
                return object.id
        # Id not found. Look for it into system virtual server.
        virtualServerId = 'glasnost://system/%s' % self.applicationRole
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name == name:
                return object.id
        return '' # or should it raise sth ?

    def getNameByMappedId(self, mappedId):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.mappedId == mappedId:
                return object.name
        # Name not found. Look for it into help virtual server.
        virtualServerId = 'glasnost://help/%s' % self.applicationRole
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.mappedId == mappedId:
                return object.name
        # Name not found. Look for it into system virtual server.
        virtualServerId = 'glasnost://system/%s' % self.applicationRole
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.mappedId == mappedId:
                return object.name
        return '' # or should it raise sth ?

    def hasObjectStartingWithPath(self, path):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, object in virtualServer.objects.items():
            if object.name.startswith(path):
                return 1
        return 0

    def registerPublicMethods(self):
        ObjectsServer.registerPublicMethods(self)
        self.registerPublicMethod('getIdByName')
        self.registerPublicMethod('getObjectIdByName')
        self.registerPublicMethod('getNameByMappedId')
        self.registerPublicMethod('hasObjectStartingWithPath')

    def repairVirtualServer(self, virtualServer, version):
        changed = 0
        if version <= 1008000:
            admin = virtualServer.admin
            if admin.id is None:
                changed = 1
                admin.id = '%s/__admin__' % virtualServer.virtualServerId
        if changed:
            virtualServer.markAllAsDirtyFIXME()


pageNamesServer = PageNamesServer()


if __name__ == "__main__":
    pageNamesServer.launch(applicationName, applicationRole)

