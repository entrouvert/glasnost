#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost System Files Server"""

__version__ = '$Revision$'[11:-2]


import fcntl
import os
from stat import *
import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

from glasnost.common.SystemFilesCommon import *
import glasnost.common.faults as faults
import glasnost.common.xhtmlgenerator as X

from glasnost.server.ObjectsServer import register, ObjectServerMixin, \
        AdminServerMixin, ObjectsServer
from glasnost.server.tools import *

from glasnost.proxy.DispatcherProxy import getApplicationId


applicationName = 'SystemFilesServer'
applicationRole = 'systemfiles'
dispatcher = None


class AdminSystemFiles(AdminServerMixin, AdminSystemFilesCommon):
    pass
register(AdminSystemFiles)


class SystemFile(ObjectServerMixin, SystemFileCommon):
    def acquireNonCore(self, objectDirectoryPath = None,
                       dataDirectoryPath = None, parentSlot = None):
        ObjectServerMixin.acquireNonCore(
            self, objectDirectoryPath = objectDirectoryPath,
            dataDirectoryPath = dataDirectoryPath, parentSlot = parentSlot)
        self.loadData()

    def loadData(self):
        dataFilePath = self.filePath
        try:
            dataFile = open(dataFilePath, 'rb')
        except IOError:
            if self.__dict__.has_key('data'):
                del self.data
            if self.__dict__.has_key('size'):
                del self.size
        else:
            fcntl.lockf(dataFile, fcntl.LOCK_SH)
            self.data = dataFile.read()
            fcntl.lockf(dataFile, fcntl.LOCK_UN)
            dataFile.close()
            # XMLRPC doesn't implements the handling of long integers.
            self.size = int(os.stat(dataFilePath)[ST_SIZE])

    def releaseNonCore(self, parentSlot = None):
        if self.__dict__.has_key('data'):
            del self.data
        ObjectServerMixin.releaseNonCore(self, parentSlot = parentSlot)
register(SystemFile)


class SystemFilesServer(SystemFilesCommonMixin, ObjectsServer):
    def checkFilePath(self, filePath):
        if not os.path.exists(filePath):
            raise faults.NonExistentFilePath(filePath)
        elif not os.path.isfile(filePath):
            raise faults.NotAFile(filePath)
        elif not os.access(filePath, os.R_OK):
            raise faults.FileAccessDenied(filePath)

    def registerPublicMethods(self):
        ObjectsServer.registerPublicMethods(self)
        self.registerPublicMethod('checkFilePath')

    def repairVirtualServer(self, virtualServer, version):
        changed = 0
        if version < 4000:
            changed = virtualServer.admin.repair(4000) or changed
            for id, object in virtualServer.objects.items():
                newId = repairId(id)
                if newId:
                    changed = 1
                    del virtualServer.objects[id]
                    virtualServer.objects[newId] = object
                changed = object.repair(4000) or changed
                if not object.__dict__.has_key('language'):
                    changed = 1
                    object.language = 'fr'
        if version < 5004:
            changed = virtualServer.admin.repair(5004) or changed
            for id, object in virtualServer.objects.items():
                changed = object.repair(5004) or changed
        if version <= 1012000:
            admin = virtualServer.admin
            if admin.id is None:
                changed = 1
                admin.id = '%s/__admin__' % virtualServer.virtualServerId
        if changed:
            virtualServer.markAllAsDirtyFIXME()


systemFilesServer = SystemFilesServer()


if __name__ == "__main__":
    systemFilesServer.launch(applicationName, applicationRole)
