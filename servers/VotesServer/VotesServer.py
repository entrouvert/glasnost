#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Votes Server"""

__version__ = '$Revision$'[11:-2]


import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

from glasnost.common.VotesCommon import *
import glasnost.common.faults as faults
import glasnost.common.tools_new as commonTools
import glasnost.common.xhtmlgenerator as X

from glasnost.server.ObjectsServer import register, ObjectServerMixin, \
        AdminServerWithoutWritersMixin, ObjectsServer
from glasnost.server.tools import *

from glasnost.proxy.CacheProxy import invalidateValue
from glasnost.proxy.DispatcherProxy import getApplicationId


applicationName = 'VotesServer'
applicationRole = 'votes'
dispatcher = None


class AdminVotes(AdminServerWithoutWritersMixin, AdminVotesCommon):
    pass
register(AdminVotes)


class VoteMixin(ObjectServerMixin):
    # The slot voterId doesn't exist in the server votes.
    voterId_kindName = None

    def canBeCreatedByClient(self):
        clientToken = context.getVar('clientToken')
        clientId = getApplicationId(clientToken)
        clientNameAndPort, clientRole, mu = commonTools.splitId(clientId)
        return clientRole == 'ballots'

    def canBeDeletedByClient(self):
        clientToken = context.getVar('clientToken')
        clientId = getApplicationId(clientToken)
        clientNameAndPort, clientRole, mu = commonTools.splitId(clientId)
        return clientRole == 'ballots'

    def canBeGottenByClient(self):
        clientToken = context.getVar('clientToken')
        clientId = getApplicationId(clientToken)
        clientNameAndPort, clientRole, mu = commonTools.splitId(clientId)
        return clientRole == 'ballots'

    def canBeModifiedByClient(self):
        clientToken = context.getVar('clientToken')
        clientId = getApplicationId(clientToken)
        clientNameAndPort, clientRole, mu = commonTools.splitId(clientId)
        return clientRole == 'ballots'


class AbstractVote(VoteMixin, AbstractVoteCommon):
    pass
register(AbstractVote)


class VoteApproval(VoteMixin, VoteApprovalCommon):
    pass
register(VoteApproval)


class VoteDistribution(VoteMixin, VoteDistributionCommon):
    pass
register(VoteDistribution)


class VoteRanking(VoteMixin, VoteRankingCommon):
    pass
register(VoteRanking)


class VoteRating(VoteMixin, VoteRatingCommon):
    pass
register(VoteRating)


class VotesServer(VotesCommonMixin, ObjectsServer):
    useAdminReadersSet = 0
    useAdminWritersSet = 0

    def canAddObject(self):
        # Nobody can add a vote.
        return 0

    def canDeleteObject(self, objectId):
        # Nobody can delete a vote.
        return 0

    def canGetObject(self, objectId):
        # Nobody can get a vote.
        try:
            virtualServerId = context.getVar('applicationId')
            virtualServer = self.getVirtualServer(virtualServerId)
            object = virtualServer.loadObjectCore(objectId)
            if object.canBeGottenByClient():
                return 1
        except faults.UnknownServerId:
            pass
        return 0

    def canModifyObject(self, objectImport):
        # Nobody can modify a vote.
        return 0

    def repairVirtualServer(self, virtualServer, version):
        changed = 0
        if version < 4000:
            changed = virtualServer.admin.repair(4000) or changed
            for id, object in virtualServer.objects.items():
                newId = repairId(id)
                if newId:
                    changed = 1
                    del virtualServer.objects[id]
                    virtualServer.objects[newId] = object
                changed = object.repair(4000) or changed
                if not object.__dict__.has_key('language'):
                    changed = 1
                    object.language = 'fr'
        if version < 5004:
            changed = virtualServer.admin.repair(5004) or changed
            for id, object in virtualServer.objects.items():
                changed = object.repair(5004) or changed
        if version <= 1015000:
            admin = virtualServer.admin
            if admin.id is None:
                changed = 1
                admin.id = '%s/__admin__' % virtualServer.virtualServerId
        if version <= 1019000:
            for object in virtualServer.objects.values():
                if object.token is not None \
                       and not object.token.startswith('glasnost://'):
                    changed = 1
                    object.token = '%s/%s' % (
                        virtualServer.virtualServerId, object.token)
        if changed:
            virtualServer.markAllAsDirtyFIXME()


votesServer = VotesServer()


if __name__ == "__main__":
    votesServer.launch(applicationName, applicationRole)
