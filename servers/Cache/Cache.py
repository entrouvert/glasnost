#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Cache"""

__version__ = '$Revision$'[11:-2]


import sys
import time

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.faults as faults

from glasnost.server.ObjectsServer import Server, VirtualServer
from glasnost.server.tools import *


applicationName = 'Cache'
applicationRole = 'cache' # no!
dispatcher = None


class CacheVirtualServer(VirtualServer):
    pass


class Cache(Server):
    VirtualServer = CacheVirtualServer
    hasMultipleVirtualServers = 0
    invalidatedKeys = {}
    invalidatedKeyStarts = {}
    useDataFile = 0

    def invalidateKeyStart(self, keyStart):
        self.invalidatedKeyStarts[keyStart] = time.time()
        return keyStart

    def invalidateValue(self, key):
        self.invalidatedKeys[key] = time.time()
        return key

    def isValueStillValid(self, key, timeCached):
        if not self.invalidatedKeys.has_key(key):
            k = [x
                 for x in self.invalidatedKeyStarts.keys()
                 if key.startswith(x)]
            if len(k) and self.invalidatedKeyStarts[k[0]] > timeCached:
                return key
            return ''
        if self.invalidatedKeys[key] < timeCached:
            return ''
        return key

    def registerPublicMethods(self):
        Server.registerPublicMethods(self)
        self.registerPublicMethod('invalidateKeyStart')
        self.registerPublicMethod('invalidateValue')
        self.registerPublicMethod('isValueStillValid')


cache = Cache()


if __name__ == "__main__":
    cache.launch(applicationName, applicationRole)

