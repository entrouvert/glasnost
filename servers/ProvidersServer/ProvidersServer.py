#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Providers Server"""

__version__ = '$Revision$'[11:-2]


import sys

glasnostPythonDir = '/usr/local/lib/glasnost-devel' # changed on make install
sys.path.insert(0, glasnostPythonDir)

import glasnost

import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.ProvidersCommon as commonProviders

import glasnost.server.ObjectsServer as objects


applicationName = 'ProvidersServer'
applicationRole = 'providers'
dispatcher = None


class AdminProviders(objects.AdminServerMixin, commonProviders.AdminProviders):
    pass
objects.register(AdminProviders)


class IdentityProvider(objects.ObjectServerMixin,
                       commonProviders.IdentityProvider):
    pass
objects.register(IdentityProvider)


class ServiceProvider(objects.ObjectServerMixin,
                      commonProviders.ServiceProvider):
    pass
objects.register(ServiceProvider)


class ProvidersServer(commonProviders.ProvidersCommonMixin,
                      objects.ObjectsServer):
    def getRemoteIdentityProviderId(self):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        admin = virtualServer.admin
        if admin.remoteIdentityProviderId is None:
            raise faults.MissingItem('Remote Identity Provider')
        return admin.remoteIdentityProviderId

    def getServiceProviderId(self, providerId):
        virtualServerId = context.getVar('applicationId')
        virtualServer = self.getVirtualServer(virtualServerId)
        for objectId, objectCore in virtualServer.objects.items():
            if isinstance(objectCore, ServiceProvider) \
                    and objectCore.providerId == providerId \
                    and (self.canGetObject(objectId)
                         or objectCore.canBeGottenByClient()):
                return objectId
        raise faults.MissingItem('Service Provider "%s"' % providerId)

    def registerPublicMethods(self):
        objects.ObjectsServer.registerPublicMethods(self)
        self.registerPublicMethod('getRemoteIdentityProviderId')
        self.registerPublicMethod('getServiceProviderId')


providersServer = ProvidersServer()


if __name__ == "__main__":
    providersServer.launch(applicationName, applicationRole)
