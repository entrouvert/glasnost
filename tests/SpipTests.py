# -*- coding: iso-8859-15 -*-

import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-tests'
sys.path.insert(0, glasnostPythonDir)

import glasnost.common.context as context
from glasnost.common.parsers import makeHtmlFromSpip

class SpipParserTestCase(unittest.TestCase):
    def setUp(self):
        context.push(dispatcherId = 'glasnost://localhost')

    def test00_plainSentence(self):
        '''Render a plain sentence'''
        result = makeHtmlFromSpip('''this is not interesting''')
        self.failUnless(result == '''<p>this is not interesting</p>''')

    def test01_plainSentenceInlineMode(self):
        '''Render a plain sentence for inline use'''
        result = makeHtmlFromSpip('''this is not interesting''', inline = 1)
        self.failUnless(result == 'this is not interesting''')

    def test02_httpLink(self):
        '''Render an http link, no text'''
        result = makeHtmlFromSpip('''[->http://www.example.com]''', inline = 1)
        self.failUnless(result == '<a href="http://www.example.com" class="external">www.example.com</a>')

    def test03_httpLinkWithText(self):
        '''Render an http link, with text'''
        result = makeHtmlFromSpip('''[Check this->http://www.example.com]''', inline = 1)
        self.failUnless(result == '<a href="http://www.example.com" title="www.example.com" class="external">Check this</a>')

    def test04_httpLinkWithTextAndTooltip(self):
        '''Render an http link, with text and tooltip'''
        result = makeHtmlFromSpip('[Check this|Nice layout->http://www.example.com]''', inline = 1)
        self.failUnless(result == '<a href="http://www.example.com" title="Nice layout" class="external">Check this</a>')

    def test05_glasnostLink(self):
        '''Render a link to a Glasnost article'''
        result = makeHtmlFromSpip('''[->article 2]''', inline = 1)
        self.failUnless(result == '<a href="[{glasnost:partialid:localhost:articles:2}]">[{glasnost:label:localhost:articles:2}]</a>')

    def test06_glasnostPageNameLink(self):
        '''Render a link using an alias'''
        result = makeHtmlFromSpip('''[->alias test]''', inline = 1)
        self.failUnless(result == '<a href="[{glasnost:alias:localhost:test}]">[{glasnost:aliaslabel:localhost:test}]</a>')

    def test07_nothingSpecialLink(self):
        '''Render a plain link, nothing special'''
        result = makeHtmlFromSpip('''[->/test]''', inline = 1)
        self.failUnless(result == '<a href="/test">/test</a>')

    def test08_inlineEmphasis(self):
        '''Render an emphasis word'''
        result = makeHtmlFromSpip('''this {is} it.''', inline = 1)
        self.failUnless(result == 'this <em>is</em> it.')

    def test09_inlineStrong(self):
        '''Render a strong word'''
        result = makeHtmlFromSpip('''this {{is}} it.''', inline = 1)
        self.failUnless(result == 'this <strong>is</strong> it.')

    def test10_title(self):
        '''Render a title'''
        result = makeHtmlFromSpip('{{{big title}}}')
        self.failUnless(result == '<h2 id="big-title">big title</h2>')

    def test11_emphasisInsideTitle(self):
        '''Render emphasis inside a title'''
        result = makeHtmlFromSpip('{{{big {emphasis} title}}}')
        self.failUnless(result == '<h2 id="big-emphasis-title">big <em>emphasis</em> title</h2>')

    def test12_twoParagraphs(self):
        '''Render two paragraphs'''
        result = makeHtmlFromSpip('''first paragraph\n\nsecond paragraph''')
        self.failUnless(result == '<p>first paragraph</p>\n<p>second paragraph</p>')

    def test13_linkInsiteTitle(self):
        '''Render a link inside a title'''
        result = makeHtmlFromSpip('''{{{a [->test]}}}''', inline = 1)
        self.failUnless(result == '<h2 id="a-test">a <a href="test">test</a></h2>')

    def test14_strongInParagraph(self):
        '''Render emphasis in a paragraph'''
        result = makeHtmlFromSpip('''begin {{strong}} end''', inline = 0)
        self.failUnless(result == '<p>begin <strong>strong</strong> end</p>')

    def test15_strongOutOfParagraph(self):
        '''Render emphasis out of a paragraph'''
        result = makeHtmlFromSpip('''begin\n\n{{strong}}\n\nend''')
        self.failUnless(result == '<p>begin</p>\n<h3 id="strong">strong</h3>\n<p>end</p>')

    def test16_strongSecondLine(self):
        '''Render strong with and without empty lines'''
        result1 = makeHtmlFromSpip('''{{test}}\n''')
        result2 = makeHtmlFromSpip('''\n{{test}}\n''')
        self.failUnless(result1 == '<h3 id="test">test</h3>' and \
                result1 == result2)

    def test17_tableWithHeader(self):
        '''Render a table with header'''
        result = makeHtmlFromSpip("""
| {{0,0}} | {{0,1}} |
|   1,0   |   1,1   |
""")
        self.failUnless(result == """<table>
  <tr>
    <th scope="col">0,0</th>
    <th scope="col">0,1</th>
  </tr>
  <tr class="odd">
    <td>1,0</td>
    <td>1,1</td>
  </tr>
</table>""")

    def test18_tableWithoutHeader(self):
        '''Render a table without header'''
        result = makeHtmlFromSpip("""
|   0,0   |   0,1   |
|   1,0   |   1,1   |
""")
        self.failUnless(result == """<table>
  <tr class="odd">
    <td>0,0</td>
    <td>0,1</td>
  </tr>
  <tr class="even">
    <td>1,0</td>
    <td>1,1</td>
  </tr>
</table>""")

    def test19_tableWithStrongCell(self):
        '''Render a table with a strong cell'''
        result = makeHtmlFromSpip("""
|   0,0   |   0,1   |
|   1,0   | {{1,1}} |
""")
        self.failUnless(result == """<table>
  <tr class="odd">
    <td>0,0</td>
    <td>0,1</td>
  </tr>
  <tr class="even">
    <td>1,0</td>
    <td><strong>1,1</strong></td>
  </tr>
</table>""")

    def test20_tableWithStrongCellOnFirstLine(self):
        '''Render a table with a strong cell on first line'''
        result = makeHtmlFromSpip("""
| {{0,0}} |   0,1   |
|   1,0   |   1,1   |
""")
        self.failUnless(result == """<table>
  <tr class="odd">
    <td><strong>0,0</strong></td>
    <td>0,1</td>
  </tr>
  <tr class="even">
    <td>1,0</td>
    <td>1,1</td>
  </tr>
</table>""")

suite1 = unittest.makeSuite(SpipParserTestCase, 'test')

allTests = unittest.TestSuite((suite1, ))

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(allTests)


