# -*- coding: iso-8859-15 -*-

import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-tests'
sys.path.insert(0, glasnostPythonDir)

from GlasnostTestCase import MinimalTestCase, OneUserTestCase

import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.system as system
import glasnost.common.tools_new as commonTools

from glasnost.proxy.tools import getProxyForServerRole


articlesProxy = getProxyForServerRole('articles')

class AddingTestCase(OneUserTestCase):
    def test01_noneObject(self):
        '''Add a None article'''
        try:
            articlesProxy.addObject(None)
            self.fail('Server accepted None')
        except AttributeError:
            pass

    def test02_nonArticleObject(self):
        """Add a non article object"""
        
        badObject = 'This is it!'
        try:
            articlesProxy.addObject(badObject)
            self.fail('Server accepted a random string')
        except AttributeError:
            pass

    def test03_viciousObject(self):
        """Add a vicious article"""

        class ViciousObject:
            def exportToXmlRpc(self):
                return {
                        '__thingCategory__' : 'object',
                        '__thingName__' : 'articles.Article',
                        }
        
        viciousObject = ViciousObject()

        try:
            articlesProxy.addObject(viciousObject)
            self.fail('Server accepted the vicious object')
        except faults.MissingSlotValue:
            pass


suite1 = unittest.makeSuite(AddingTestCase, 'test')

allTests = unittest.TestSuite((suite1, ))

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(allTests)

