# -*- coding: iso-8859-15 -*-

import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-tests'
sys.path.insert(0, glasnostPythonDir)

from GlasnostTestCase import MinimalTestCase

from glasnost.web.AppointmentsWeb import weekNumberToDate, dateToWeekNumber

class WeekNumberKnownValuesTestCase(MinimalTestCase):
    knownValues = ( ( (1995,  1), (1995,  1,  2) ),
                    ( (1996,  1), (1996,  1,  1) ),
                    ( (1997,  1), (1996, 12, 30) ),
                    ( (1998,  1), (1997, 12, 29) ),
                    ( (1999,  1), (1999,  1,  4) ),
                    ( (2000,  1), (2000,  1,  3) ),
                    ( (2001,  1), (2001,  1,  1) ),
                    ( (2002,  1), (2001, 12, 31) ),
                    ( (2003,  1), (2002, 12, 30) ),
                    ( (2003,  2), (2003,  1,  6) ),
                    ( (2003, 25), (2003,  6, 16) ),
                    ( (2003, 52), (2003, 12, 22) ),
                    ( (2004,  1), (2003, 12, 29) ),
                    ( (1979, 24), (1979,  6, 11) ),
                  )
    knownFromDateValues = ( ( (2003,  3, 16), (2003, 11) ),
                            ( (2003,  3, 17), (2003, 12) ),
                            ( (2003,  3, 18), (2003, 12) ),
                            ( (2003,  3, 19), (2003, 12) ),
                            ( (2003,  3, 20), (2003, 12) ),
                            ( (2003,  3, 21), (2003, 12) ),
                            ( (2003,  3, 22), (2003, 12) ),
                            ( (2003,  3, 23), (2003, 12) ),
                            ( (2003,  3, 24), (2003, 13) ),
                            ( (1999, 12, 31), (1999, 52) ),
                            ( (2000,  1,  1), (1999, 52) ),
                            ( (2000,  1,  2), (1999, 52) ),
                            ( (2000,  1,  3), (2000,  1) ),
                          )

    def test01_ToDateKnownValues(self):
        """Convert week numbers to dates"""
        for ywTuple, ymdTuple in self.knownValues:
            date = weekNumberToDate(ywTuple[0], ywTuple[1])
            self.assertEqual(date[:3], ymdTuple)

    def test02_FromDateKnownValues(self):
        """Convert dates to week numbers"""
        for ywTuple, ymdTuple in self.knownValues:
            week = dateToWeekNumber(ymdTuple[0], ymdTuple[1], ymdTuple[2])
            self.assertEqual(week, ywTuple)

    def testFromDateMoreKnownValues(self):
        """Convert more dates to week numbers"""
        for ymdTuple, ywTuple in self.knownFromDateValues:
            week = dateToWeekNumber(ymdTuple[0], ymdTuple[1], ymdTuple[2])
            self.assertEqual(week, ywTuple)


suite1 = unittest.makeSuite(WeekNumberKnownValuesTestCase, 'test')

allTests = unittest.TestSuite((suite1,))

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(allTests)

