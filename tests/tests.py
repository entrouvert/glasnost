#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

import imp
import unittest
import sys

testSuites = (
        'AppointmentsTests',
        'CardsTests',
        'GroupsTests',
        'KindsTests',
        'VirtualHostsTests',
        'ArticlesTests',
        'SpipTests',
        'NCardsTests',
        'WebTests',
)

for testSuite in testSuites:
    fp, pathname, description = imp.find_module(testSuite)
    try:                                             
        module = imp.load_module(testSuite, fp, pathname, description)
    finally:                                                   
        if fp:
            fp.close()
    if not module:
        print 'Unable to load test suite:', testSuite

    if module.__doc__:
        doc = module.__doc__
    else:
        doc = testSuite
    
    print
    print '-' * len(doc)
    print doc
    print '-' * len(doc)

    unittest.TextTestRunner(verbosity=2).run(module.allTests)

