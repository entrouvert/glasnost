# -*- coding: iso-8859-15 -*-

import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-tests'
sys.path.insert(0, glasnostPythonDir)

from GlasnostTestCase import MinimalTestCase

import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.system as system
import glasnost.common.tools_new as commonTools

import glasnost.proxy.kinds as kinds
import glasnost.proxy.properties as properties
from glasnost.proxy.ObjectsProxy import AdminMixin, ObjectProxyMixin, ObjectsProxy
from glasnost.proxy.tools import getProxyForServerRole


ncardsProxy = getProxyForServerRole('ncards')

class AddingTestCase(MinimalTestCase):
    def tearDown(self):
        for objectId in ncardsProxy.getObjectIds():
            ncardsProxy.deleteObject(objectId)
        MinimalTestCase.tearDown(self)

    def test01_noneObject(self):
        '''Add a None article'''
        try:
            ncardsProxy.addObject(None)
            self.fail('Server accepted None')
        except AttributeError:
            pass

    def test02_nonNCardObject(self):
        """Add a non ncard object"""
        
        badObject = 'This is it!'
        try:
            ncardsProxy.addObject(badObject)
            self.fail('Server accepted a random string')
        except AttributeError:
            pass

    def test03_viciousObject(self):
        """Add a vicious ncard"""

        class ViciousObject:
            def exportToXmlRpc(self):
                return {
                        '__thingCategory__' : 'object',
                        '__thingName__' : 'ncards.NCard',
                        }
        
        viciousObject = ViciousObject()

        try:
            ncardsProxy.addObject(viciousObject)
            self.fail('Server accepted the vicious object')
        except faults.MissingSlotValue:
            pass

    def test04_NCardWithoutRole(self):
        """Add a ncard missing the role slot"""
        ncard = commonTools.newThing('object', 'ncards.NCard')
        ncard.objectName = 'test'
        ncard.objectsName = 'test'
        
        try:
            ncardsProxy.addObject(ncard)
            self.fail('Server accepted an object without role slot')
        except faults.MissingSlotValue:
            pass

class SimpleNCardTestCase(MinimalTestCase):
    def test01_creating(self):
        '''Create a simple NCard'''
        ncard = commonTools.newThing('object', 'ncards.NCard')
        ncard.language = 'en'
        ncard.objectName = 'My Atom'
        ncard.objectsName = 'My Atoms'
        ncard.role = 'myatoms'

        property = properties.Property()
        property.name = 'name'
        kind = kinds.String()
        kind.isRequired = 1
        property.kind = kind

        ncard.properties = [property]

        ncardsProxy.addObject(ncard)

    def test02_getProxy(self):
        '''Get the card proxy'''
        myatomsProxy = getProxyForServerRole('myatoms')
        self.failIf(myatomsProxy is None)
        self.failUnless(isinstance(myatomsProxy, ObjectsProxy))
        self.failUnless(myatomsProxy.objectNameCapitalized == 'My Atom')

    def test03_newObject(self):
        '''Test the newObject() method'''
        myatomsProxy = getProxyForServerRole('myatoms')
        myAtom = myatomsProxy.newObject()
        self.failUnless(isinstance(myAtom, ObjectProxyMixin))

    def test04_newAdmin(self):
        '''Test the newAdmin() method'''
        myatomsProxy = getProxyForServerRole('myatoms')
        myAtomAdmin = myatomsProxy.newAdmin()
        self.failUnless(isinstance(myAtomAdmin, AdminMixin))

    def test05_addObject(self):
        '''Test the addObject() method'''
        myatomsProxy = getProxyForServerRole('myatoms')
        myAtom = myatomsProxy.newObject()
        myAtom.name = 'foo'
        objectId = myatomsProxy.addObject(myAtom)
        self.failUnless(commonTools.extractRole(objectId) == 'myatoms')

    def test06_getObjectIds(self):
        '''Test the getObjectIds() method'''
        myatomsProxy = getProxyForServerRole('myatoms')
        objectIds = myatomsProxy.getObjectIds()
        self.failUnless(len(objectIds) == 1)
        self.failUnless(commonTools.extractRole(objectIds[0]) == 'myatoms')

    def test07_getObject(self):
        '''Test the getObject() method'''
        myatomsProxy = getProxyForServerRole('myatoms')
        objectIds = myatomsProxy.getObjectIds()
        myAtom = myatomsProxy.getObject(objectIds[0])
        self.failUnless(myAtom.name == 'foo')

    def test08_deleteObject(self):
        '''Test the deleteObject() method'''
        myatomsProxy = getProxyForServerRole('myatoms')
        objectIds = myatomsProxy.getObjectIds()
        myatomsProxy.deleteObject(objectIds[0])
        objectIds = myatomsProxy.getObjectIds()
        self.failUnless(len(objectIds) == 0)
        
    def test99_deleting(self):
        '''Deleting the simple NCard'''
        for objectId in ncardsProxy.getObjectIds():
            ncardsProxy.deleteObject(objectId)

suite1 = unittest.makeSuite(AddingTestCase, 'test')
suite2 = unittest.makeSuite(SimpleNCardTestCase, 'test')

allTests = unittest.TestSuite((suite1, suite2,))

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(allTests)

