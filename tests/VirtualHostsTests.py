# -*- coding: iso-8859-15 -*-

import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-tests'
sys.path.insert(0, glasnostPythonDir)

from GlasnostTestCase import MinimalTestCase

import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.system as system
import glasnost.common.tools_new as commonTools

from glasnost.proxy.tools import getProxyForServerRole


virtualHostsProxy = getProxyForServerRole('virtualhosts')

class VirtualHostsTestCase(MinimalTestCase):
    pass

class SaneAdminTestCase(VirtualHostsTestCase):
    def test01_admins(self):
        """Check if administrator is general public"""
        admin = virtualHostsProxy.getAdmin()
        self.failUnless(admin.adminsSet == [system.generalPublicId])

class AddingTestCase(VirtualHostsTestCase):
    def tearDown(self):
        for objectId in virtualHostsProxy.getObjectIds():
            virtualHostsProxy.deleteObject(objectId)
        VirtualHostsTestCase.tearDown(self)

    def test01_noneObject(self):
        """Add a None virtual host"""
        try:
            virtualHostsProxy.addObject(None)
        except AttributeError:
            return
        self.fail('Server accepted None')

    def test02_nonVirtualHostObject(self):
        """Add a non virtual host object"""
        
        badObject = 'This is it!'
        try:
            virtualHostsProxy.addObject(badObject)
        except AttributeError:
            return
        self.fail('Server accepted a random string')

    def test03_viciousObject(self):
        """Add a vicious virtual host"""

        class ViciousObject:
            def exportToXmlRpc(self):
                return {
                        '__thingCategory__' : 'object',
                        '__thingName__' : 'virtualhosts.VirtualHost',
                        }
        
        viciousObject = ViciousObject()

        try:
            id = virtualHostsProxy.addObject(viciousObject)
        except faults.MissingSlotValue:
            return
        self.fail('Server accepted the vicious object')

    def test04_goodObject(self):
        '''Add a virtual host'''

        virtualHost = commonTools.newThing(
                'object', 'virtualhosts.VirtualHost')
        virtualHost.language = 'en'
        virtualHost.title = 'test04_goodObject'
        virtualHost.defaultDispatcherId = 'glasnost://localhost'
        virtualHost.hostName = 'tests.example.com'
        virtualHostId = virtualHostsProxy.addObject(virtualHost)
        self.failUnless(virtualHostsProxy.hasObject(virtualHostId))

    def test05_sameObject(self):
        '''Add twice the same virtual host'''

        virtualHost = commonTools.newThing(
                'object', 'virtualhosts.VirtualHost')
        virtualHost.language = 'en'
        virtualHost.title = 'test05_sameObject'
        virtualHost.defaultDispatcherId = 'glasnost://localhost'
        virtualHost.hostName = 'tests.example.com'
        try:
            virtualHostId = virtualHostsProxy.addObject(virtualHost)
            virtualHostId = virtualHostsProxy.addObject(virtualHost)
        except faults.DuplicateHostName:
            return
        self.fail('Server accepted two objects with same host name')

class GetObjectByHostNameTestCase(VirtualHostsTestCase):
    def setUp(self):
        VirtualHostsTestCase.setUp(self)

        virtualHost = commonTools.newThing('object', 'virtualhosts.VirtualHost')
        virtualHost.language = 'en'
        virtualHost.title = 'GetObjectByHostNameTestCase'
        virtualHost.defaultDispatcherId = 'glasnost://localhost'
        self.hostNamesIds = {
                'tests.example.com': None,
                'example.com': None,
                'glasnost.example.com': None,
                'other.example.net': None
            }
        for hostName in self.hostNamesIds.keys():
            virtualHost.hostName = hostName
            self.hostNamesIds[hostName] = virtualHostsProxy.addObject(virtualHost)


    def tearDown(self):
        for objectId in virtualHostsProxy.getObjectIds():
            virtualHostsProxy.deleteObject(objectId)
        VirtualHostsTestCase.tearDown(self)

    def test01_getExisting(self):
        '''Get virtual host by existing hostname'''
        virtualHost = virtualHostsProxy.getObjectByHostName('tests.example.com')
        self.failUnless(virtualHost.id == self.hostNamesIds['tests.example.com'])

    def test02_getNotExisting(self):
        '''Get virtual host by unexisting hostname'''
        try:
            virtualHostsProxy.getObjectByHostName('tests.example.net')
        except faults.MissingItem:
            return
        self.fail('Server returned something for an unknown hostname')

    def test03_getClose(self):
        '''Get virtual host by unexisting (but close) hostname'''
        virtualHost = virtualHostsProxy.getObjectByHostName('www.example.com')
        self.failUnless(virtualHost.id == self.hostNamesIds['example.com'])

    def test04_getNotExistingWhenDefaultDefined(self):
        '''Get virtual host by unexisting hostname (but default defined)'''
        admin = virtualHostsProxy.getAdmin()
        admin.defaultVirtualHostId = self.hostNamesIds['tests.example.com']
        virtualHostsProxy.modifyAdmin(admin)
        virtualHost = virtualHostsProxy.getObjectByHostName('blah')
        self.failUnless(virtualHost.id == self.hostNamesIds['tests.example.com'])
        admin.defaultVirtualHostId = None
        virtualHostsProxy.modifyAdmin(admin)


suite1 = unittest.makeSuite(SaneAdminTestCase, 'test')
suite2 = unittest.makeSuite(AddingTestCase, 'test')
suite3 = unittest.makeSuite(GetObjectByHostNameTestCase, 'test')

allTests = unittest.TestSuite((suite1, suite2, suite3))

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(allTests)
    #from GtkTestRunner import GtkTestRunner
    #GtkTestRunner().run(allTests)


