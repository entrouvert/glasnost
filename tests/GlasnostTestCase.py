#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os
import sys
import unittest

glasnostPythonDir = '/usr/local/lib/glasnost-tests'
sys.path.insert(0, glasnostPythonDir)

import glasnost.common.applications as applications
import glasnost.common.context as context
import glasnost.common.faults as faults
import glasnost.common.system as system
import glasnost.common.tools_new as commonTools

from glasnost.proxy.tools import getProxyForServerRole

class MinimalTestCase(unittest.TestCase, applications.Application):
    applicationName = 'Tests'
    applicationRole = 'tests'

    def setUp(self):
        self.launch()
        virtualHostsProxy = getProxyForServerRole('virtualhosts')
        object = virtualHostsProxy.getObjectByHostName('localhost')
        if not 'cards' in object.profiles:
            object.profiles.append('cards')
            virtualHostsProxy.modifyObject(object)
        
    def tearDown(self):
        pass
        #context.pull()


class OneUserTestCase(MinimalTestCase):
    def login(self):
        passwordAccountsProxy = getProxyForServerRole('passwordaccounts')
        userToken, authenticationMethod = \
                passwordAccountsProxy.checkObjectAuthentication(
                    'test-user', 'test-user')
        context.setVar('userToken', userToken)

    def logout(self):
        identitiesProxy = getProxyForServerRole('identities')
        identitiesProxy.deleteUserToken()
        context.delVar('userToken')

    def setUp(self):
        MinimalTestCase.setUp(self)
        identitiesProxy = getProxyForServerRole('identities')
        identity = identitiesProxy.newObject()
        self.identityId = identitiesProxy.addObject(identity)

        passwordAccountsProxy = getProxyForServerRole('passwordaccounts')
        admin = passwordAccountsProxy.getAdmin()
        admin.userCanChoosePassword = 1
        passwordAccountsProxy.modifyAdmin(admin)

        passwordAccount = passwordAccountsProxy.newObject()
        passwordAccount.identityId = self.identityId
        passwordAccount.login = 'test-user'
        passwordAccount.password = 'test-user'
        self.passwordAccountId = passwordAccountsProxy.addObject(passwordAccount)
        


    def tearDown(self):
        identitiesProxy = getProxyForServerRole('identities')
        identitiesProxy.deleteObject(self.identityId)
        passwordAccountsProxy = getProxyForServerRole('passwordaccounts')
        passwordAccountsProxy.deleteObject(self.passwordAccountId)

        admin = passwordAccountsProxy.getAdmin()
        admin.userCanChoosePassword = 1
        passwordAccountsProxy.modifyAdmin(admin)

        MinimalTestCase.tearDown(self)

