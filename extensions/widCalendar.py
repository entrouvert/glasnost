# -*- coding: iso-8859-15 -*-


# Glasnost
# By: Odile B�nassy <obenassy@entrouvert.com>
#     Romain Chantereau <rchantereau@entrouvert.com>
#     Nicolas Clapi�s <nclapies@easter-eggs.org>
#     Pierre-Antoine Dejace <padejace@entrouvert.be>
#     Thierry Dulieu <tdulieu@easter-eggs.com>
#     Florent Monnier <monnier@codelutin.com>
#     C�dric Musso <cmusso@easter-eggs.org>
#     Fr�d�ric P�ters <fpeters@entrouvert.be>
#     Benjamin Poussin <poussin@codelutin.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     S�bastien R�gnier <regnier@codelutin.com>
#     Emmanuel Saracco <esaracco@easter-eggs.com>
#
# Copyright (C) 2000, 2001 Easter-eggs & Emmanuel Raviart
# Copyright (C) 2002 Odile B�nassy, Code Lutin, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Fr�d�ric P�ters, Benjamin Poussin, Emmanuel Raviart,
#     Emmanuel Saracco & Th�ridion
# Copyright (C) 2003 Odile B�nassy, Romain Chantereau, Nicolas Clapi�s,
#     Code Lutin, Pierre-Antoine Dejace, Thierry Dulieu, Easter-eggs,
#     Entr'ouvert, Florent Monnier, C�dric Musso, Ouvaton, Fr�d�ric P�ters,
#     Benjamin Poussin, Rodolphe Qui�deville, Emmanuel Raviart, S�bastien
#     R�gnier, Emmanuel Saracco, Th�ridion & Vecam
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


__doc__ = """Glasnost Calendar Widget Extension"""

__version__ = '$Revision$'[11:-2]

import time
import calendar

import glasnost.web.calendaring as calendaring

def monthTupleJS(year = 0, month = 0):
    if not year or not month:
        year, month = time.gmtime(time.time())[:2]
    year, month = int(year), int(month)
    today = time.gmtime(time.time())[:3]
    cal = calendar.monthcalendar(year, month)
    for i in range(len(cal)):
        w = cal[i]
        for j in range(len(w)):
            d = cal[i][j]
            if d == 0:
                cal[i][j] = ''
                continue
            more = ''
            if (year, month, d) == today:
                more = ' id="today"'
            date = '%s-%02d-%02d' % (year, month, cal[i][j])
            cal[i][j] = '<a href="#" onclick="selectDay(\'%s\')"%s>%s</a>' % \
                    (date, more, cal[i][j])
    return cal

def getDayLabels():
    return calendaring.dayLabels

def getMonthYear(year = 0, month = 0):
    if not year or not month:
        year, month = time.gmtime(time.time())[:2]
    year, month = int(year), int(month)
    monthNames = calendaring.montLabels
    return '%s %s' % (_(monthNames[month-1]), year)

def getPreviousMonth(year = 0, month = 0):
    if not year or not month:
        year, month = time.gmtime(time.time())[:2]
    year, month = int(year), int(month)
    if month == 1:
        return (year-1, 12)
    else:
        return (year, month-1)

def getNextMonth(year = 0, month = 0):
    if not year or not month:
        year, month = time.gmtime(time.time())[:2]
    year, month = int(year), int(month)
    if month == 12:
        return (year+1, 1)
    else:
        return (year, month+1)

