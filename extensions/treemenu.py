def getClass(object):
    if object.getThingName() == 'rubrics.Rubric':
        return 'menu-close'
    else:
        return 'menu-leaf'

def haveChild(currentObject, sub, objectPath):
    try:
        result = currentObject and sub and sub.getThingName() == 'rubrics.Rubric' and (
                currentObject.id == sub.id or (
                        objectPath and sub.id in [o.id for o in objectPath \
                                                  if hasattr(o, 'id')]))
    except:
        result = 0
    return result
